﻿var GridPager = React.createClass({
    render: function () {
        var li = [];
        var pageCount = this.props.Size;
        var currentPage = this.props.CurrentPage;
        var previous = currentPage - 1;
        var next = currentPage + 1;
        var rightPageLinks = currentPage + 4;
        var leftPageLinks = currentPage - 3;
        var firstPageKey = -1;
        var lastPageKey = -2;
        
        if (currentPage > 1) {
            li.push(
                <li key={firstPageKey} className="first">
                    <a href="#" onClick={this.props.onPageChanged.bind(null,1)}>
                        <i className="fa fa-angle-double-left" aria-hidden="true"></i>
                    </a>
                </li>
            );
            for (var i = leftPageLinks; i < currentPage; i++) {
                if (i > 0) {
                    li.push(
                        <li key={i}>
                            <a href="#" onClick={this.props.onPageChanged.bind(null,i)}>{i}</a>
                        </li>
                   );
                }
            }
        }
        li.push(
            <li key={currentPage} className="active"><a href="#/">{currentPage}</a></li>
        );
        for (var i = currentPage + 1; i < rightPageLinks; i++) {
            if (i <= pageCount) {
                li.push(
                    <li key={i}>
                        <a href="#" onClick={this.props.onPageChanged.bind(null,i)}>{i}</a>
                    </li>
                );
            }
        }
        if (currentPage < pageCount) {
            li.push(
                <li key={lastPageKey} className="last">
                    <a href="#" onClick={this.props.onPageChanged.bind(null,pageCount)}>
                        <i className="fa fa-angle-double-right" aria-hidden="true"></i>
                    </a>
                </li>
            );
        }
        return (
            <ul className="pagination" style={{float:'right'}}>{li}</ul>
        );
    }
});