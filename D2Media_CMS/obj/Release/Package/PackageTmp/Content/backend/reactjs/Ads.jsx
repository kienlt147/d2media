﻿var AdGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage,
            searchText: this.state.Data.SearchText
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    handleAdRemove: function (objectId) {
        var params = {
            Id: objectId
        }
        $.ajax({
            url: this.props.urlDelete,
            type: 'GET',
            data: params,
            success: function (data) {
                if (data.Code == 0) {
                    this.populateData();
                } else {
                    alert(data.Message);
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    searchChange: function (value) {
        var data = this.state.Data;
        data.SearchText = value;
        this.setState({
            Data: data
        });
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <AdGridRow key={item.Id} item={item} onAdRemove={that.handleAdRemove} urlAdAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={8}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <SearchBox onSearchChanged={this.searchChange} searchText={this.state.Data.SearchText} urlAdAddEdit={that.props.urlAddEdit}/>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>#</th>
                            <th>Tên</th>
                            <th>Nội dung</th>
                            <th>Link</th>
                            <th>Chuyên mục</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var AdGridRow = React.createClass({
    handleRemoveAd: function (objectId, e) {
        e.preventDefault();
        if (confirm("Bạn chắc chắn muốn xóa bản ghi?")) {
            this.props.onAdRemove(this.props.item.Id)
        }
    },
    render: function () {
        return (
            <tr>
                <td>{this.props.item.Id}</td>
                <td>{this.props.item.AdName}</td>
                <td>{this.props.item.AdContent}</td>
                <td>{this.props.item.Link}</td>
                <td>{this.props.item.CategoryName}</td>
                <td className="col-10">
                    <a href={this.props.urlAdAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
                <td className="col-10">
                    <button type="button" className="btn btn-danger" onClick={this.handleRemoveAd.bind(null,this)}>
                        <i className="fa fa-trash-o"></i>
                    </button>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    handleChange : function(e){
        this.props.onSearchChanged(e.target.value);
    },
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <div className="col-md-4">
                        <input type="text" value={this.props.searchText} className="form-control" placeholder="Tìm kiếm" onChange={this.handleChange} />
                    </div>
                </div>
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlAdAddEdit} className="btn btn-gray" title="Thêm mới quảng cáo">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

