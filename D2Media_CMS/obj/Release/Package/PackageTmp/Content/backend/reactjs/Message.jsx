﻿var MessageGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage,
            searchText: this.state.Data.SearchText
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    handleMessageRemove: function (objectId) {
        var params = {
            Id: objectId
        }
        $.ajax({
            url: this.props.urlDelete,
            type: 'GET',
            data: params,
            success: function (data) {
                if (data.Code == 0) {
                    this.populateData();
                } else {
                    alert(data.Message);
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    searchChange: function (value) {
        var data = this.state.Data;
        data.SearchText = value;
        this.setState({
            Data: data
        });
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <MessageGridRow key={item.Id} item={item} onMessageRemove={that.handleMessageRemove} urlAdAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={8}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <SearchBox onSearchChanged={this.searchChange} searchText={this.state.Data.SearchText} urlAdAddEdit={that.props.urlAddEdit}/>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>#</th>
                            <th>Tên</th>
                            <th>Nội dung</th>
                            <th>Mã người dùng</th>
                            <th>Loại thông báo</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var MessageGridRow = React.createClass({
    handleRemoveMessage: function (objectId, e) {
        e.preventDefault();
        if (confirm("Bạn chắc chắn muốn xóa bản ghi?")) {
            this.props.onMessageRemove(this.props.item.Id)
        }
    },
    render: function () {
        return (
            <tr>
                <td>{this.props.item.Id}</td>
                <td>{this.props.item.Title}</td>
                <td>{this.props.item.Content}</td>
                <td>{this.props.item.UserId}</td>
                <td>{this.props.item.MessageTypeName}</td>
                <td className="col-10">
                    <a href={this.props.urlAdAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
                <td className="col-10">
                    <button type="button" className="btn btn-danger" onClick={this.handleRemoveMessage.bind(null,this)}>
                        <i className="fa fa-trash-o"></i>
                    </button>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlAdAddEdit} className="btn btn-gray" title="Gửi thông báo">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Gửi thông báo</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

