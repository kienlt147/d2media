﻿var Mega645GridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                SortOrder: 'desc',
                SortColumnName: 'DrawId',
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            gameId: this.props.GameId,
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage
        }
        if (this.state.Data.SortColumnName) {
            params.SortColumnName = this.state.Data.SortColumnName;
        }
        if (this.state.Data.SortOrder) {
            params.SortOrder = this.state.Data.SortOrder;
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    sortChanged: function (sortColumnName, order, e) {
        e.preventDefault();
        this.state.Data.SortColumnName = sortColumnName;
        this.state.Data.CurrentPage = 1;
        this.state.Data.SortOrder = order.toString().toLowerCase() == 'asc' ? 'desc' : 'asc';
        this.populateData();
    },
    _sortClass: function (filterName) {
        return "fa fa-fw " + ((filterName == this.state.Data.SortColumnName) ? ("fa-sort-" + this.state.Data.SortOrder) : "fa-sort");
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <Mega645GridRow key={item.Id} item={item} urlAdAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={10}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <SearchBox urlMega645AddEdit={that.props.urlAddEdit} />
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th onClick={this.sortChanged.bind(this,'DrawId',this.state.Data.SortOrder)}>
                                <i className={this._sortClass('DrawId')}></i>Kỳ quay
                            </th>
                            <th>Cặp 1</th>
                            <th>Cặp 2</th>
                            <th>Cặp 3</th>
                            <th>Cặp 4</th>
                            <th>Cặp 5</th>
                            <th>Cặp 6</th>
                            <th>Jackpot</th>
                            <th>Ngày quay</th>
                            <th className="col-10"></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var Mega645GridRow = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.item.DrawId}</td>
                <td>{this.props.item.Number1}</td>
                <td>{this.props.item.Number2}</td>
                <td>{this.props.item.Number3}</td>
                <td>{this.props.item.Number4}</td>
                <td>{this.props.item.Number5}</td>
                <td>{this.props.item.Number6}</td>
                <td>{FormatNumberToMoney(this.props.item.JackPort)}</td>
                <td>{this.props.item.DrawDate}</td>
                <td>
                    <a href={this.props.urlAdAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlMega645AddEdit} className="btn btn-gray" title="Thêm mới">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});