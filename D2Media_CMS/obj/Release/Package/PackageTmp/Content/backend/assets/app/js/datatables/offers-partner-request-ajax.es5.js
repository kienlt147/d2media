﻿"use strict";

var DataOffer = {
    init: function init() {
        var EditLink = "/OfferAddEdit/";
        var DeleteLink = "/PartnerRequestDelete/";
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetPartnerRequestList",
                        map: function map(t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !0, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#UserName") },
            columns: [{ field: "Id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center" }, { field: "UserName", title: "User Name", sortable: !1, filterable: !1 }, { field: "Email", title: "Email", sortable: !1 }, { field: "Name", title: "Offer Name", sortable: !1 }, { field: "RequestDate", title: "Time Request", sortable: !1, width: 150, type: "date", format: "DD/MM/YYYY" }, {
                field: "Status",
                title: "Status",
                width: 100,
                locked: { right: "xl" },
                template: function template(t) {
                    var e = {
                        1: { title: "Approval", "class": " m-badge--success" },
                        0: { title: "Pending", "class": " m-badge--warning" }
                    };
                    return '<span class="btn m-badge ' + e[t.Status]["class"] + ' m-badge--wide">' + e[t.Status].title + "</span>";
                }
            }, {
                field: "Actions",
                width: 70,
                title: "Actions",
                sortable: !1,
                overflow: "visible",
                locked: { right: "xl" },
                template: function template(t, e, a) {
                    var e = {
                        0: { "class": "m-btn--hover-accent" },
                        1: { disabled: "disabled", "class": "m-btn--hover-metal" }
                    };
                    return '\t\t\t\t\t\t<button type="button" class="m-portlet__nav-link btn m-btn ' + e[t.Status]["class"] + ' m-btn--icon m-btn--icon-only m-btn--pill acceptPartnerRequest" title="Edit details" ' + 'data-id="' + t.Id + '" data-offerid="' + t.OfferId + '" data-userid="' + t.UserId + '" ' + e[t.Status].disabled + '>\t\t\t\t\t\t\t' + '<i class="fa fa-download"></i>\t\t\t\t\t\t</button>\t\t\t\t\t\t' + '<button href="#" data-action="' + DeleteLink + '" data-id="' + t.Id + '" data-action-delete-item="' + t.Id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill action-delete" title="Delete">\t\t\t\t\t\t\t' + '<i class="la la-trash"></i>\t\t\t\t\t\t</button>\t\t\t\t\t';
                }
            }]
        }), $("#Status").on("change", function () {
            t.search($(this).val(), "Status");
        }), $("#OfferName").on("change", function () {
            t.search($(this).val(), "OfferName");
        }), $("#Status").selectpicker();
    }
};
jQuery(document).ready(function () {
    DataOffer.init();
});

