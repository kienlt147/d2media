﻿var Dashboard = function () {
    var a = function (e, t, a, r) {
        if (0 != e.length) {
            var o = {
                type: "line",
                data: {
                    labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October"],
                    datasets: [{
                        label: "",
                        borderColor: a,
                        borderWidth: r,
                        pointHoverRadius: 4,
                        pointHoverBorderWidth: 12,
                        pointBackgroundColor: Chart.helpers.color("#000000").alpha(0).rgbString(),
                        pointBorderColor: Chart.helpers.color("#000000").alpha(0).rgbString(),
                        pointHoverBackgroundColor: mApp.getColor("danger"),
                        pointHoverBorderColor: Chart.helpers.color("#000000").alpha(.1).rgbString(),
                        fill: !1,
                        data: t
                    }]
                },
                options: {
                    title: {
                        display: !1
                    },
                    tooltips: {
                        enabled: !1,
                        intersect: !1,
                        mode: "nearest",
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: !1,
                        labels: {
                            usePointStyle: !1
                        }
                    },
                    responsive: !0,
                    maintainAspectRatio: !0,
                    hover: {
                        mode: "index"
                    },
                    scales: {
                        xAxes: [{
                            display: !1,
                            gridLines: !1,
                            scaleLabel: {
                                display: !0,
                                labelString: "Month"
                            }
                        }],
                        yAxes: [{
                            display: !1,
                            gridLines: !1,
                            scaleLabel: {
                                display: !0,
                                labelString: "Value"
                            },
                            ticks: {
                                beginAtZero: !0
                            }
                        }]
                    },
                    elements: {
                        point: {
                            radius: 4,
                            borderWidth: 12
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 10,
                            top: 5,
                            bottom: 0
                        }
                    }
                }
            };
            return new Chart(e, o)
        }
    }
    return {
        init: function (columnCategories, rowCoversion, rowClicks, rowRevenue, rowPayout) {
            !function () {
                if (0 != $("#m_dashboard_daterangepicker").length) {
                    var n = $("#m_dashboard_daterangepicker"),
                        e = moment(),
                        t = moment();
                    n.daterangepicker({
                        startDate: e,
                        endDate: t,
                        opens: "left",
                        ranges: {
                            Today: [moment(), moment()],
                            Yesterday: [moment().subtract(1, "days"), moment().subtract(1, "days")],
                            "Last 7 Days": [moment().subtract(6, "days"), moment()],
                            "Last 30 Days": [moment().subtract(29, "days"), moment()],
                            "This Month": [moment().startOf("month"), moment().endOf("month")],
                            "Last Month": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                        }
                    }, a), a(e, t, "")
                }

                function a(e, t, a) {
                    var r = "",
                        o = "";
                    t - e < 100 ? (r = "Today:", o = e.format("MMM D")) : "Yesterday" == a ? (r = "Yesterday:", o = e.format("MMM D")) : o = e.format("MMM D") + " - " + t.format("MMM D"), n.find(".m-subheader__daterange-date").html(o), n.find(".m-subheader__daterange-title").html(r)
                }
            }(),
            function() {
                if (0 != $("#m_chart_report_stats").length) {
                    var chartId = document.getElementById("m_chart_report_stats").getContext("2d");
                    var configOptions = {
                        type: "line",
                        data: {
                            labels: columnCategories,
                            datasets: [{
                                label: "Conversion",
                                backgroundColor: "#36a3f7",
                                borderColor: "#36a3f7",
                                fill: false,
                                data: rowCoversion
                            }, {
                                label: "Click",
                                backgroundColor: "#f4516c",
                                borderColor: "#f4516c",
                                fill: false,
                                data: rowClicks
                            },
                            {
                                label: "Revenue",
                                backgroundColor: "#716aca",
                                borderColor: "#716aca",
                                fill: false,
                                data: rowRevenue
                            },
                            {
                                label: "Payout",
                                backgroundColor: "#34bfa3",
                                borderColor: "#34bfa3",
                                fill: false,
                                data: rowPayout
                            }]
                        },
                        options: {
                            title: {
                                display: !1
                            },
                            tooltips: {
                                intersect: !1,
                                mode: 'index'
                            },
                            legend: {
                                display: !1
                            },
                            responsive: !0,
                            maintainAspectRatio: !1,
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: !0,
                                    gridLines: !1,
                                    scaleLabel: {
                                        display: !1,
                                        labelString: "Datetime"
                                    }
                                }],
                                yAxes: [{
                                    display: !0,
                                    gridLines: !0,
                                    scaleLabel: {
                                        display: !1,
                                        labelString: "Value"
                                    },
                                    ticks: {
                                        beginAtZero: !0
                                    }
                                }]
                            },
                            elements: {
                                line: {
                                    tension: .19
                                },
                                point: {
                                    radius: 2,
                                    borderWidth: 0
                                }
                            }
                        }
                    };
                    new Chart(chartId, configOptions);
                }    
            }(),
            a($("#m_chart_quick_stats_1"), [10, 14, 18, 11, 9, 12, 14, 17, 18, 14], mApp.getColor("brand"), 3), 
            a($("#m_chart_quick_stats_2"), [11, 12, 18, 13, 11, 12, 15, 13, 19, 15], mApp.getColor("danger"), 3),
            a($("#m_chart_quick_stats_3"), [12, 12, 18, 11, 15, 12, 13, 16, 11, 18], mApp.getColor("success"), 3),
            a($("#m_chart_quick_stats_4"), [11, 9, 13, 18, 13, 15, 14, 13, 18, 15], mApp.getColor("accent"), 3);
        }
    }
}();