﻿var ChartReport = function () {
    return {
        hour : function (columnCategories, rowCoversion, rowClicks, rowRevenue, rowPayout) {
            var chartId = document.getElementById("m_chart_report_stats").getContext("2d");
            var configOptions = {
                type: "line",
                data: {
                    labels: columnCategories,
                    datasets: [{
                        label: "Conversion",
                        backgroundColor: "#36a3f7",
                        borderColor: "#36a3f7",
                        fill: false,
                        data: rowCoversion
                    }, {
                        label: "Click",
                        backgroundColor: "#f4516c",
                        borderColor: "#f4516c",
                        fill: false,
                        data: rowClicks
                    },
                    {
                        label: "Revenue",
                        backgroundColor: "#716aca",
                        borderColor: "#716aca",
                        fill: false,
                        data: rowRevenue
                    },
                    {
                        label: "Payout",
                        backgroundColor: "#34bfa3",
                        borderColor: "#34bfa3",
                        fill: false,
                        data: rowPayout
                    }]
                },
                options: {
                    title: {
                        display: !1
                    },
                    tooltips: {
                        intersect: !1,
                        mode: 'index'
                    },
                    legend: {
                        display: !1
                    },
                    responsive: !0,
                    maintainAspectRatio: !1,
                    hover: {
                        mode: 'nearest',
                        intersect: true
                    },
                    scales: {
                        xAxes: [{
                            display: !0,
                            gridLines: !1,
                            scaleLabel: {
                                display: !1,
                                labelString: "Datetime"
                            }
                        }],
                        yAxes: [{
                            display: !0,
                            gridLines: !0,
                            scaleLabel: {
                                display: !1,
                                labelString: "Value"
                            },
                            ticks: {
                                beginAtZero: !0
                            }
                        }]
                    },
                    elements: {
                        line: {
                            tension: .19
                        },
                        point: {
                            radius: 2,
                            borderWidth: 0
                        }
                    }
                }
            };
            return new Chart(chartId, configOptions);
        }   
    }
}();