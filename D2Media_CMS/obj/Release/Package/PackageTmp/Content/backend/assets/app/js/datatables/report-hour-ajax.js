﻿var DataReportHour = {
    init: function () {
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetReporHourList",
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !1, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#TimePicker") },
            columns: [
                {
                    field: "DateFormat", title: "Time on date", filterable: !1,
                    template: function(t) {
                        return t.DateFormat + " " + t.Hour;
                    }
                },
                { field: "TotalClick", title: "Total Unique Click", sortable: !1 },
                { field: "TotalClick", title: "Total Click", sortable: !1 },
                { field: "TotalConversion", title: "Total Conversion", sortable: !1 },
                {
                    field: "CR", title: "CR", type: "number", filterable: !1,
                    template: function (t) {
                        if (t.TotalConversion === 0) return "0%";
                        return (t.TotalConversion / t.TotalClick).toFixed(2) + " %";
                    }
                },
                {
                    field: "TotalPayout", title: "Total Payout", sortable: !1,
                    template: function (t) {
                        return (t.TotalPayout).toFixed(2);
                    }
                },
                {
                    field: "TotalRevenue", title: "Total Revenue", sortable: !1,
                    template: function (t) {
                        return (t.TotalRevenue).toFixed(2);
                    }
                },
                {
                    field: "Profit", title: "Profit", filterable: !1,
                    template: function (t) {
                        return (t.TotalRevenue - t.TotalPayout).toFixed(2);
                    }
                }
            ]
        }), $("#TimePicker").on("change", function () {
            t.search($(this).val(), "TimePicker");
            $.ajax({
                url: Configs.Domain.Link + "GetChartReporHourList/",
                type: "GET",
                data: {
                    timePicker: $(this).val()
                },
                dataType: "json",
                success: function (data) {
                    var categories = Enumerable.From(data.data).Select("$.Hour").ToArray();
                    var totalConversion = Enumerable.From(data.data).Select("$.TotalConversion").ToArray();
                    var totalClick = Enumerable.From(data.data).Select("$.TotalClick").ToArray();
                    var totalRevenue = Enumerable.From(data.data).Select("$.TotalRevenue").ToArray();
                    var totalPayout = Enumerable.From(data.data).Select("$.TotalPayout").ToArray();
                    chartReportHour.data = {
                        labels: categories,
                        datasets: [
                            {
                                label: "Conversion",
                                backgroundColor: "#36a3f7",
                                borderColor: "#36a3f7",
                                fill: false,
                                data: totalConversion
                            }, {
                                label: "Click",
                                backgroundColor: "#f4516c",
                                borderColor: "#f4516c",
                                fill: false,
                                data: totalClick
                            },
                            {
                                label: "Revenue",
                                backgroundColor: "#716aca",
                                borderColor: "#716aca",
                                fill: false,
                                data: totalRevenue
                            },
                            {
                                label: "Payout",
                                backgroundColor: "#34bfa3",
                                borderColor: "#34bfa3",
                                fill: false,
                                data: totalPayout
                            }
                        ]
                    };
                    chartReportHour.update();
                }
            });
        });
    }
};
jQuery(document).ready(function () { DataReportHour.init() });