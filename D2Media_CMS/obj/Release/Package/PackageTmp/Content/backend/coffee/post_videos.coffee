$ ".add_new_category" 
    .click ->
        $ ".form_category_post"
            .animate
                left: "+=50"
                height: "toggle"
                1000, ->

$ "#IsYoutube"
    .click ->
        $ "#isYoutubeUrl"
            .toggle this.checked
        $ "#isServerUrl"
            .toggle !this.checked

$ ".add_new_category_news"
    .click ->
        category_id = $ "#CategoryParentId"
            .val()
        category_name = $ ".category_new_name" 
            .val()

        if !category_name? || category_name is ""
            $ ".message_result_categories"
                .addClass "alert alert-warning"
                .html "Tên chuyên mục không được để trống."
            return false
        if !category_id? || category_id is ""
            $ ".message_result_categories"
                .addClass "alert alert-warning"
                .html "Chuyên mục không được để trống."
            return false
        
        $.ajax
            url : "/AjaxCategoryAdd"
            type : "GET"
            data : { category_id: category_id, category_name: category_name }
            dataType : "json"
            success : (data) ->
                if data.Status is "0"
                    $ ".message_result_categories"
                        .addClass "alert alert-success"
                        .html data.Message
                    $ "#CategoryParentId"
                        .empty()
                            .append data.Data
                    $ "#CategoryParentId"
                        .selectpicker "refresh"
                    $ "#CategoryId"
                        .empty()
                            .append data.Data
                    $ "#CategoryId"
                        .selectpicker "refresh"
                else
                    $ ".message_result_categories"
                        .addClass "alert alert-danger"
                        .html data.Message

$ ".add_news_tags"
    .click ->
        tags_name = $("#tags_name").val()
        console.log tags_name
        tags_hidden = $("#tags_hidden").val()
        tags_value = ""
        milliseconds = new Date().getTime()
        return false if !tags_name? || tags_name is ""
        array_tag = tags_name.split(',')
        if array_tag.length > 1
            for i in array_tag
                if !i? || i is ""
                    continue
                $ "#view_all_tags"
                    .append '<span class="btn btn-default btn-xs margin-5">' +
                        '<a id="post_tag_' + _i + '" data-value="' + i + '" onclick="return RemoveTags(' + _i + ')">' +
                        '<i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;' + i + '</span>'
        else
            $ "#view_all_tags"
                .append '<span class="btn btn-default btn-xs margin-5">' +
                    '<a id="post_tag_' + milliseconds + '" data-value="' + tags_name + '" onclick="return RemoveTags(' + milliseconds + ')">' +
                    '<i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;' + tags_name + '</span>'
        $("#view_all_tags").find('a').each ->
            if tags_value is ""
                tags_value += $(this)
                    .data "value"
                    .toString()
                    .toLowerCase()
                    .trim()
            else
                tags_value += "," + $(this)
                    .data "value"
                    .toString()
                    .toLowerCase()
                    .trim()
        $("#tags_name").val ""
        $("#tags_hidden").val tags_value