$(".set_slug").click ->
    slugFieldValue = $("#Name")
        .val()
        .toLowerCase()
        .replace(/\ /g, "-")
    if $("#Slug").val().trim() isnt ""
        if confirm(segmentNotEmptyPressOkToOverwrite)
            $("#Slug").val(RemoveUTF8(slugFieldValue))
    else
        $("#Slug").val(RemoveUTF8(slugFieldValue))