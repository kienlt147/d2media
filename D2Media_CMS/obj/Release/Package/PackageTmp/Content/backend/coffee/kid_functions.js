(function() {
  var kids;

  kids = typeof exports !== "undefined" && exports !== null ? exports : this;

  kids.RemoveUTF8 = function(str) {
    return str.toLowerCase().replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e").replace(/ì|í|ị|ỉ|ĩ/g, "i").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u").replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y").replace(/đ/g, "d");
  };

  kids.TreeViewList = function(treeList) {
    return $("#tree").treeview({
      data: treeList,
      levels: 5,
      showIcon: false,
      showCheckbox: true,
      showTags: true
    });
  };

  kids.GetCurrentDate = function() {
    var dd, hh, minutes, mm, today, yyyy;
    today = new Date();
    dd = today.getDate();
    mm = today.getMonth() + 1;
    yyyy = today.getFullYear();
    hh = today.getHours();
    minutes = today.getMinutes();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    if (hh < 10) {
      hh = "0" + hh;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return dd + "/" + mm + "/" + yyyy + " " + hh + ":" + minutes;
  };

  kids.UseCurrentDate = function(fieldId, postId) {
    if (postId === 0) {
      return $("#" + fieldId).val(GetCurrentDate);
    }
  };

  kids.FormatTypeDatetime = function(datetime, type) {
    var dd, hh, minutes, mm, today, yyyy;
    today = new Date(datetime);
    dd = today.getDate();
    mm = today.getMonth() + 1;
    yyyy = today.getFullYear();
    hh = today.getHours();
    minutes = today.getMinutes();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    if (hh < 10) {
      hh = "0" + hh;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    if (type === "date") {
      return dd + "/" + mm + "/" + yyyy;
    } else if (type === "time") {
      return hh + ":" + minutes;
    }
  };

  kids.SetDate = function(fieldId, datetime) {
    return $("#" + fieldId).val(FormatTypeDatetime(datetime, 'date'));
  };

  kids.SetTime = function(fieldId, datetime) {
    return $("#" + fieldId).val(FormatTypeDatetime(datetime, 'time'));
  };

  kids.GetCurrentDateFullCalendar = function() {
    var dd, hh, minutes, mm, today, yyyy;
    today = new Date();
    dd = today.getDate();
    mm = today.getMonth() + 1;
    yyyy = today.getFullYear();
    hh = today.getHours();
    minutes = today.getMinutes();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    if (hh < 10) {
      hh = "0" + hh;
    }
    if (minutes < 10) {
      minutes = '0' + minutes;
    }
    return yyyy + "/" + mm + "/" + dd;
  };

  kids.RemoveTags = function(element) {
    var tags_value;
    tags_value = "";
    $("#post_tag_" + element).parent().remove();
    $("#view_all_tags").find("a").each(function() {
      if (tags_value === "") {
        return tags_value += $(this).data("value").toString().toLowerCase().trim();
      } else {
        return tags_value += "," + $(this).data("value").toString().toLowerCase().trim();
      }
    });
    return $("#tags_hidden").val(tags_value);
  };

  kids.TagInput = function(url, key, value, classValue, tags, maxTag) {
    var bloodHound, elementTag;
    if (maxTag == null) {
      maxTag = 10;
    }
    bloodHound = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name'),
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: url
    });
    bloodHound.clearPrefetchCache();
    bloodHound.initialize(true);
    elementTag = $("." + classValue);
    elementTag.tagsinput({
      itemValue: key,
      itemText: value,
      freeInput: false,
      trimValue: true,
      order: "asc",
      maxItem: 10,
      maxTags: maxTag,
      typeaheadjs: [
        {
          minLength: 1,
          highlight: true
        }, {
          minlength: 1,
          name: tags,
          displayKey: value,
          source: bloodHound.ttAdapter()
        }
      ]
    });
    return elementTag;
  };

  kids.reformatDateString = function(s) {
    var b;
    b = s.split(/\D/);
    return b.reverse().join("-");
  };

  kids.Notify = function(title, text, style) {
    return $.notify({
      title: title,
      text: text
    }, {
      style: 'metro',
      className: style,
      autoHide: true,
      clickToHide: true,
      showDuration: 400
    });
  };

}).call(this);
