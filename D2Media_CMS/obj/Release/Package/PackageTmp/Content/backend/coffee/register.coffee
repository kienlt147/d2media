﻿$ "#m_login_signup_submit" 
    .validate
        rules: 
            UserName: 
                required: true
                minlength: 6
                maxlength: 16
            FullName: 
                required: true
            Password: 
                required: true
                minlength: 6
                maxlength: 16
            Rpassword: 
                required: true
                minlength: 6
                maxlength: 16
                equalTo: "#Password"
            Email: 
                required: true
                email: true
        ,
        messages: 
            UserName: 
                required: "Tên đăng nhập không được để trống"
                minlength: "Tên đăng nhập phải từ 6 ký tự trở lên"
                maxlength: "Tên đăng nhập không được dài quá 16 ký tự"
            FullName:
                required: "Mật khẩu không được để trống"
            Password:
                required: "Mật khẩu không được để trống"
                minlength: "Mật khẩu phải từ 6 ký tự trở lên"
                maxlength: "Mật khẩu phải nhỏ hơn 16 ký tự"
            Rpassword: 
                required: "Xác nhận mật khẩu không được để trống"
                minlength: "Xác nhận mật khẩu phải từ 6 ký tự trở lên"
                maxlength: "Xác nhận mật khẩu phải nhỏ hơn 16 ký tự"
                equalTo: "Xác nhận mật khẩu không trùng khớp với mật khẩu"
            Email: 
                required: "Email không được để trống"
                email: "Định dạng email không chính xác"