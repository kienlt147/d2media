﻿kids = exports ? this
kids.RemoveUTF8 = (str) -> 
    str.toLowerCase()
        .replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a")
        .replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e")
        .replace(/ì|í|ị|ỉ|ĩ/g,"i")
        .replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o")
        .replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u")
        .replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/đ/g,"d")

kids.TreeViewList = (treeList) ->
    $ "#tree"
        .treeview
            data: treeList
            levels: 5
            showIcon: false
            showCheckbox: true
            showTags: true

kids.GetCurrentDate = ->
    today = new Date()
    dd = today.getDate()
    mm = today.getMonth() + 1
    yyyy = today.getFullYear()
    hh = today.getHours()
    minutes = today.getMinutes()

    dd = "0" + dd if dd < 10
    mm = "0" + mm if mm < 10
    hh = "0" + hh if hh < 10
    minutes = '0' + minutes if minutes < 10

    dd + "/" + mm + "/" + yyyy + " " + hh + ":" + minutes

kids.UseCurrentDate = (fieldId, postId)->
    if postId == 0
        $ "#" + fieldId 
            .val(GetCurrentDate)
    
kids.FormatTypeDatetime = (datetime, type)->
    today = new Date(datetime)
    dd = today.getDate()
    mm = today.getMonth() + 1
    yyyy = today.getFullYear()
    hh = today.getHours()
    minutes = today.getMinutes()

    dd = "0" + dd if dd < 10
    mm = "0" + mm if mm < 10
    hh = "0" + hh if hh < 10
    minutes = '0' + minutes if minutes < 10
    if type is "date"
        dd + "/" + mm + "/" + yyyy
    else if type is "time"
        hh + ":" + minutes

kids.SetDate = (fieldId, datetime)->
    $ "#" + fieldId 
        .val(FormatTypeDatetime(datetime, 'date'))

kids.SetTime = (fieldId, datetime)->
    $ "#" + fieldId 
        .val(FormatTypeDatetime(datetime, 'time'))

kids.GetCurrentDateFullCalendar = ->
    today = new Date()
    dd = today.getDate()
    mm = today.getMonth() + 1
    yyyy = today.getFullYear()
    hh = today.getHours()
    minutes = today.getMinutes()

    dd = "0" + dd if dd < 10
    mm = "0" + mm if mm < 10
    hh = "0" + hh if hh < 10
    minutes = '0' + minutes if minutes < 10

    yyyy + "/" + mm + "/" + dd

kids.RemoveTags = (element) ->
    tags_value = ""
    $("#post_tag_" + element).parent().remove()
    $("#view_all_tags").find("a").each -> 
        if tags_value is ""
            tags_value += $ this
                .data "value"
                .toString()
                .toLowerCase()
                .trim()
        else
            tags_value += "," + $ this
                .data "value"
                .toString()
                .toLowerCase()
                .trim()
    $("#tags_hidden").val tags_value

kids.TagInput = (url, key, value, classValue, tags, maxTag = 10) ->
    bloodHound = new Bloodhound
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Name')
        queryTokenizer: Bloodhound.tokenizers.whitespace
        prefetch: url

    bloodHound.clearPrefetchCache()
    bloodHound.initialize true

    elementTag = $("." + classValue)
    elementTag.tagsinput
        itemValue: key
        itemText: value
        freeInput: false
        trimValue: true
        order: "asc"
        maxItem: 10
        maxTags: maxTag
        typeaheadjs: [
            minLength: 1
            highlight: true
        ,
            minlength: 1
            name: tags
            displayKey: value
            source: bloodHound.ttAdapter()
        ]
    elementTag

kids.reformatDateString = (s)->
    b = s.split(/\D/)
    b.reverse().join("-")

kids.notify = (title, text, style)->
    $.notify
        title: title,
        text: text
    ,
        style: 'metro',
        className: style,
        autoHide: true,
        clickToHide: true,
        showDuration: 400