﻿$(document).on "click", ".changeStatus", ()->
    $ ".loader_main"
        .show()
    offer_status = $(this)
        .data "value"
    offer_id = $(this)
        .data "id"
    that = $(this)
    $.ajax
        url : "/api/offers/changeStatus/"
        type : "GET"
        data : { offerId: offer_id, offerStatus: offer_status }
        dataType : "json"
        success : (data) ->
            $ ".loader_main"
                .hide()
            if data.ErrCode is 200
                if offer_status is 1
                    that
                        .data "value", 0
                    that
                        .html 'Stop'
                    that
                        .removeClass "m-badge--success"
                        .addClass "m-badge--danger"
                else
                    that
                        .data "value", 1
                    that
                        .html 'Start'
                    that
                        .removeClass "m-badge--danger"
                        .addClass "m-badge--success"
                $.notify
                    title: '<strong>Saving.</strong>',
                    message: data.ErrMessage
                ,
                    element: 'body',
                    type: 'success',
                    animate: 
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
            else
                $.notify
                    title: '<strong>Error.</strong>',
                    message: data.ErrMessage
                ,
                    element: 'body',
                    type: 'danger',
                    animate:
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'

$(".requestApproval").click ->
    $ ".loader_main"
        .show()
    offer_id = $(this)
        .data "offerid"
    $(this).html 'Sending...'
    $(this).addClass 'm-loader m-loader--primary m-loader--left'
    $(this).prop 'disabled', true
    that = $(this)
    $.ajax
        url : apiUrl + "/api/offers/requestApproval/"
        crossDomain: true
        type : "GET"
        data : { offerId: offer_id, authToken: authToken }
        dataType : "json"
        success : (data) ->
            $ ".loader_main"
                .hide()
            if data.ErrCode is 200
                location.reload()
            else
                that.removeClass 'm-loader m-loader--primary m-loader--left'
                that.html '<i class="flaticon-cart"></i> Request Approval' 
                that.prop 'disabled', false
                $.notify
                    title: '<strong>Request Approval.</strong>',
                    message: data.ErrMessage
                ,
                    element: 'body',
                    type: 'danger',
                    animate:
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'

$(document).on "click", ".acceptPartnerRequest", ()->
    $ ".loader_main"
        .show()
    id = $(this)
        .data "id"
    offer_id = $(this)
        .data "offerid"
    user_id = $(this)
        .data "userid"
    $(this).prop 'disabled', true
    that = $(this)
    $.ajax
        url : apiUrl + "/api/offers/acceptPartnerRequest/"
        crossDomain: true
        type : "GET"
        data : { id: id, offerId: offer_id, userRequestId: user_id , authToken: authToken }
        dataType : "json"
        success : (data) ->
            $ ".loader_main"
                .hide()
            if data.ErrCode is 200
                $(".m_datatable").mDatatable("reload")
                $.notify
                    title: '<strong>Success.</strong>',
                    message: data.ErrMessage
                ,
                    element: 'body',
                    type: 'success',
                    animate: 
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
            else
                that.prop 'disabled', false
                $.notify
                    title: '<strong>Error.</strong>',
                    message: data.ErrMessage
                ,
                    element: 'body',
                    type: 'danger',
                    animate:
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'