(function() {
  var Kigo;

  Kigo = typeof exports !== "undefined" && exports !== null ? exports : this;

  Kigo.Configs = {
    Pagination: {
      PageSize: '27'
    }
  };

}).call(this);
