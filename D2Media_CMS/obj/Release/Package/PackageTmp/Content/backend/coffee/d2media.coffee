﻿d2media = exports ? this

d2media.ToggleIcon = (e) ->
    $ e.target
        .prev '.panel-heading'
        .find ".more-less"
        .toggleClass 'glyphicon-plus glyphicon-minus'

d2media.CheckElement = (ele) ->
    all = document.getElementsByTagName("*")
    per_inc = 100 / all.length
    if $(ele).on()
        prog_width = per_inc + Number(document.getElementById("progress_width").value)
        document.getElementById("progress_width").value = prog_width
        $("#bar1").animate
            width: prog_width + "%"
            , 10
            , ->
                if document.getElementById("bar1").style.width is "100%"
                    $(".progress").fadeOut("slow")
                    $("#wrapper").show()
                else
                    CheckElement ele

d2media.LoadingPage = (e) ->
    if document.readyState is "interactive"
        all = document.getElementsByTagName("*")
        for a in all
            LoadingPage a

d2media.HighChartReport = (listDate, listSeries) ->
    Highcharts.chart 'container12',
        title:
            text: ''
        ,
        subtitle:
            text: ''
        ,
        xAxis:
            categories: listDate
        ,
        yAxis:
            title:
                text: ''
        ,
        legend:
            layout: 'vertical'
            align: 'right'
            verticalAlign: 'middle'
        ,
        colors: [
            '#4572A7'
            '#AA4643'
        ],
        plotOptions:
            series:
                label:
                    connectorAllowed: false
                ,
                marker:
                    radius: 2
                ,
                lineWidth: 2
            ,
            column:
                colorByPoint: true
        ,
        tooltip:
            shared: true
            crosshairs: true
        ,
        series: listSeries
        responsive:
            rules: [
                condition:
                    maxWidth: 500
                ,
                chartOptions:
                    legend:
                        layout: 'horizontal'
                        align: 'center'
                        verticalAlign: 'bottom'
            ]
        