﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace D2Media_CMS.HtmlHelpers
{
    public class HelperPager
    {
        public static int PageSize = int.Parse(ConfigurationManager.AppSettings["PageSize"]);
    }

    public class HelperPagination<T> : List<T>
    {
        public int TotalPages { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public string Link { get; set; } 

        public HelperPagination(List<T> dataSource, int pageIndex, int pageSize, int totalCount, string link)
        {
            TotalCount = totalCount;
            CurrentPage = pageIndex;
            PageSize = pageSize;
            TotalPages = (int)Math.Ceiling(TotalCount / (double)PageSize);
            Link = link;
            this.AddRange(dataSource);
        }


    }
}