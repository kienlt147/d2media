﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;
using Microsoft.Ajax.Utilities;

namespace D2Media_CMS.Helpers
{
    public static class Utility
    {
        private static readonly string[] VietnameseSigns =
        {
            "aAceEoOuUiIdDyYszngr ",
            "áàạảãâấầậẩẫăắằặẳẵ",
            "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
            "çćčĉ",
            "éèẹẻẽêếềệểễ",
            "ÉÈẸẺẼÊẾỀỆỂỄ",
            "óòọỏõôốồộổỗơớờợởỡ",
            "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
            "úùụủũưứừựửữ",
            "ÚÙỤỦŨƯỨỪỰỬỮ",
            "íìịỉĩ",
            "ÍÌỊỈĨ",
            "đ",
            "Đ",
            "ýỳỵỷỹ",
            "ÝỲỴỶỸ",
            "śşšŝ",
            "żźž",
            "ñń",
            "ğĝ",
            "ř",
            "~!@#$%^&*()?/<>.:;,|=\"\'/"
        };

        public static string RemoveSign4VietnameseString(this string str)
        {
            //Tiến hành thay thế , lọc bỏ dấu cho chuỗi

            for (var i = 1; i < VietnameseSigns.Length; i++)
            {
                for (var j = 0; j < VietnameseSigns[i].Length; j++)
                {
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
                }
            }
            str = str.Replace(" ", "-").ToLower();
            return str;
        }

        public static string GetYoutubeId(this string linkYoutube)
        {
            if (linkYoutube.IsNullOrWhiteSpace())
            {
                return null;
            }
            var youtubeLink = linkYoutube;
            linkYoutube = linkYoutube.Replace("https://www.youtube.com/watch?v=", string.Empty);
            linkYoutube = linkYoutube.Replace("http://www.youtube.com/watch?v=", string.Empty);
            string urlYoutube = String.Format("https://www.youtube.com/oembed?format=json&url={0}", youtubeLink);
            try
            {
                var json = new System.Net.WebClient().DownloadString(urlYoutube);
            }
            catch (Exception e)
            {
                return null;
            }

            return linkYoutube;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}