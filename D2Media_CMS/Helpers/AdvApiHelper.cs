﻿using D2Media_CMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;

namespace D2Media_CMS.Helpers
{
    public class AdvApiHelper
    {
        public static OfferLook GetOffers(string url, string accessToken)
        {
            OfferLook responseUser = null;
            //var parameters = new Dictionary<string, string> {
            //    { "TokenId", tokenId }
            //};
            //var encodedContent = new FormUrlEncodedContent(parameters);
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", accessToken);
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                responseUser = j.Deserialize<OfferLook>(result);
            }
            return responseUser;
        }

        public static Orangear GetOrangearOffers(string url)
        {
            Orangear responseUser = null;
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = Int32.MaxValue;
                responseUser = j.Deserialize<Orangear>(result);
            }
            return responseUser;
        }

        public static Adxmi GetAdxmiOffers(string url)
        {
            Adxmi responseUser = null;
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = Int32.MaxValue;
                responseUser = j.Deserialize<Adxmi>(result);
            }
            return responseUser;
        }

        public static Affise GetAffixateAffiseOffers(string url)
        {
            Affise responseUser = null;
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = Int32.MaxValue;
                responseUser = j.Deserialize<Affise>(result);
            }
            return responseUser;
        }

        public static Cygobel GetCygobelOffers(string url)
        {
            Cygobel responseUser = null;
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = Int32.MaxValue;
                responseUser = j.Deserialize<Cygobel>(result);
            }
            return responseUser;
        }

        public static ExpandMobile GetExpandMobile(string url, string token)
        {
            ExpandMobile responseUser = null;
            HttpClient _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _client.DefaultRequestHeaders.Add("x-eflow-api-key", token);
            var response = _client.GetAsync(url).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                JavaScriptSerializer j = new JavaScriptSerializer();
                j.MaxJsonLength = Int32.MaxValue;
                responseUser = j.Deserialize<ExpandMobile>(result);
            }
            return responseUser;
        }
    }
}