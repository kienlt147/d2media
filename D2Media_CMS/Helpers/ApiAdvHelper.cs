﻿using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.Helpers
{
    public class ApiAdvHelper
    {
        public static void OfferLook(string link, string token, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerLooks = AdvApiHelper.GetOffers(link, token);
            if (offerLooks == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            var offerDataRowsets = offerLooks.data.rowset;
            foreach (var offerDataRowSet in offerDataRowsets)
            {
                var index = offerDataRowsets.IndexOf(offerDataRowSet);
                List<int> countryList = new List<int>();
                foreach (var countries in offerDataRowSet.offer_geo.target)
                {
                    var country = Countries.FindByCountryCode(countries.country_code);
                    countryList.Add(country.Id);
                }
                var countryIds = String.Join(",", countryList);
                try
                {
                    offer.CategoryOfferId = offerDataRowSet.offer.id;
                    offer.CategoryId = category.CategoryId;
                    offer.Name = offerDataRowSet.offer.name;
                    offer.HasOfferLink = offerDataRowSet.offer.tracking_link;
                    offer.PreviewLink = offerDataRowSet.offer.preview_url;
                    offer.ConversionLimit = offerDataRowSet.offer_cap.IsNull() ? 0 : offerDataRowSet.offer_cap.cap_conversion;
                    offer.Description = offerDataRowSet.offer.name;
                    offer.DeviceBrand = offerDataRowSet.offer_platform.target[0].system.ToLower();
                    offer.DeviceModel = offerDataRowSet.offer_platform.target[0].system.ToLower();
                    offer.DeviceOs = offerDataRowSet.offer_platform.target[0].system.ToLower();
                    offer.DeviceOsVersion = offerDataRowSet.offer_platform.target[0].version[0];
                    offer.Images = offerDataRowSet.offer.thumbnail;
                    offer.Status = 1;
                    offer.HasOfferRevenue = 0;
                    offer.Revenue = Double.Parse(offerDataRowSet.offer.payout);
                    offer.Payout = (Double.Parse(offerDataRowSet.offer.payout) * 50 / 100);
                    Offers offers = new Offers();
                    var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                        offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                        offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                    switch (rs)
                    {
                        case 0:
                            offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                            {
                                Id = offer.CategoryOfferId.ToString(),
                                Name = offer.Name,
                                Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                            });
                            break;
                        case 2:
                            offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                            {
                                Id = offer.CategoryOfferId.ToString(),
                                Name = offer.Name,
                                Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                            });
                            break;
                        default:
                            offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                            {
                                Id = offer.CategoryOfferId.ToString(),
                                Name = offer.Name,
                                Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                            });
                            break;
                    }
                }
                catch (Exception e)
                {
                    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                    {
                        Id = offer.CategoryOfferId.ToString(),
                        Name = offer.Name,
                        Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                    });
                }
            }
        }

        public static void OfferOrangear(string link, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerLooks = AdvApiHelper.GetOrangearOffers(link);
            if (offerLooks == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            if (offerLooks.success)
            {
                try
                {
                    var offerDataRowsets = offerLooks.offers;
                    var total = offerDataRowsets.Count;
                    foreach (string key in offerDataRowsets.Keys)
                    {
                        var offerData = offerDataRowsets[key];

                        List<int> countryList = new List<int>();
                        var country = Countries.FindByCountryCode(offerData["Countries"].ToUpper());
                        if (country == null)
                        {
                            continue;
                        }
                        countryList.Add(country.Id);
                        var countryIds = String.Join(",", countryList);

                        offer.CategoryOfferId = Int64.Parse(offerData["ID"]);
                        offer.CategoryId = category.CategoryId;
                        offer.Name = offerData["Name"];
                        offer.HasOfferLink = offerData["Tracking_url"];
                        offer.PreviewLink = offerData["Preview_url"];
                        offer.ConversionLimit = 0;
                        offer.Description = offerData["Description"];
                        offer.DeviceBrand = offerData["Platforms"].ToLower();
                        offer.DeviceModel = offerData["Platforms"].ToLower();
                        offer.DeviceOs = offerData["Platforms"].ToLower();
                        offer.DeviceOsVersion = "1";
                        offer.Images = offerData["Icon_url"];
                        offer.Status = 1;
                        offer.HasOfferRevenue = 0;
                        offer.Revenue = Double.Parse(offerData["Payout"]);
                        offer.Payout = (Double.Parse(offerData["Payout"]) * 50 / 100);
                        Offers offers = new Offers();
                        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                        switch (rs)
                        {
                            case 0:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                });
                                break;
                            case 2:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                });
                                break;
                            default:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                });
                                break;
                        }
                    }
                }
                catch(Exception e)
                {
                    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                    {
                        Id = offer.CategoryOfferId.ToString(),
                        Name = offer.Name,
                        Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                    });
                }
            }
        }

        public static void OfferAdxmi(string link, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerAdxmi = AdvApiHelper.GetAdxmiOffers(link);
            if (offerAdxmi == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            if (offerAdxmi.d.offers != null)
            {
                var offerDataRowsets = offerAdxmi.d.offers;
                foreach (var offerDataRowSet in offerDataRowsets)
                {
                    var index = offerDataRowsets.IndexOf(offerDataRowSet);
                    List<int> countryList = new List<int>();
                    foreach (var countries in offerDataRowSet.country)
                    {
                        //var indexCountry = offerDataRowSet.country.IndexOf(countries);
                        var country = Countries.FindByCountryCode(countries);
                        countryList.Add(country.Id);
                    }
                    var countryIds = String.Join(",", countryList);
                    try
                    {
                        offer.CategoryOfferId = offerDataRowSet.offer_id;
                        offer.CategoryId = category.CategoryId;
                        offer.Name = offerDataRowSet.offer_name;
                        offer.HasOfferLink = offerDataRowSet.tracking_link;
                        offer.PreviewLink = offerDataRowSet.preview_url;
                        offer.ConversionLimit = offerDataRowSet.daily_cap;
                        offer.Description = offerDataRowSet.offer_description;
                        offer.DeviceBrand = offerDataRowSet.system_type.ToLower();
                        offer.DeviceModel = offerDataRowSet.system_type.ToLower();
                        offer.DeviceOs = offerDataRowSet.system_type.ToLower();
                        offer.DeviceOsVersion = "1";
                        offer.Images = offerDataRowSet.icon_url;
                        offer.Status = 1;
                        offer.HasOfferRevenue = 0;
                        offer.Revenue = offerDataRowSet.offer_payout;
                        offer.Payout = (offerDataRowSet.offer_payout * 50 / 100);
                        Offers offers = new Offers();
                        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                        switch (rs)
                        {
                            case 0:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                });
                                break;
                            case 2:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                });
                                break;
                            default:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                });
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                        {
                            Id = offer.CategoryOfferId.ToString(),
                            Name = offer.Name,
                            Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                        });
                    }
                }
            }
        }

        public static void OfferAffixateAffise(string link, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerAdxmi = AdvApiHelper.GetAffixateAffiseOffers(link);
            if (offerAdxmi == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            if (offerAdxmi.status == 1)
            {
                var offerDataRowsets = offerAdxmi.offers;
                foreach (var offerDataRowSet in offerDataRowsets)
                {
                    var index = offerDataRowsets.IndexOf(offerDataRowSet);
                    List<int> countryList = new List<int>();
                    foreach (var countries in offerDataRowSet.countries)
                    {
                        //var indexCountry = offerDataRowSet.country.IndexOf(countries);
                        var country = Countries.FindByCountryCode(countries);
                        countryList.Add(country.Id);
                    }
                    var countryIds = String.Join(",", countryList);
                    try
                    {
                        offer.CategoryOfferId = offerDataRowSet.id;
                        offer.CategoryId = category.CategoryId;
                        offer.Name = offerDataRowSet.title;
                        offer.HasOfferLink = offerDataRowSet.link;
                        offer.PreviewLink = offerDataRowSet.preview_url;
                        offer.ConversionLimit = offerDataRowSet.caps[0].value;
                        offer.Description = offerDataRowSet.description;
                        offer.DeviceBrand = offerDataRowSet.payments[0].os[0].ToLower();
                        offer.DeviceModel = offerDataRowSet.payments[0].os[0].ToLower();
                        offer.DeviceOs = offerDataRowSet.payments[0].os[0].ToLower();
                        offer.DeviceOsVersion = "1";
                        offer.Images = null;
                        offer.Status = 1;
                        offer.HasOfferRevenue = 0;
                        offer.Revenue = offerDataRowSet.payments[0].revenue;
                        offer.Payout = (offerDataRowSet.payments[0].revenue * 50 / 100);
                        Offers offers = new Offers();
                        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                        switch (rs)
                        {
                            case 0:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                });
                                break;
                            case 2:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                });
                                break;
                            default:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                });
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                        {
                            Id = offer.CategoryOfferId.ToString(),
                            Name = offer.Name,
                            Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                        });
                    }
                }
            }
        }

        public static void OfferCygobel(string link, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerCybobel = AdvApiHelper.GetCygobelOffers(link);
            if (offerCybobel == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            if (offerCybobel.err == 0)
            {
                var offerDataRowsets = offerCybobel.offers;
                foreach (var offerDataRowSet in offerDataRowsets)
                {
                    var index = offerDataRowsets.IndexOf(offerDataRowSet);
                    List<int> countryList = new List<int>();
                    foreach (var countries in offerDataRowSet.countries)
                    {
                        //var indexCountry = offerDataRowSet.country.IndexOf(countries);
                        var country = Countries.FindByCountryCode(countries);
                        countryList.Add(country.Id);
                    }
                    var countryIds = String.Join(",", countryList);
                    try
                    {
                        var image = offerDataRowSet.creatives[0] != null ? offerDataRowSet.creatives[0].url : "";
                        offer.CategoryOfferId = offerDataRowSet.id;
                        offer.CategoryId = category.CategoryId;
                        offer.Name = offerDataRowSet.name;
                        offer.HasOfferLink = offerDataRowSet.tracking_url;
                        offer.PreviewLink = offerDataRowSet.preview_url;
                        offer.ConversionLimit = 10000;
                        offer.Description = "";
                        offer.DeviceBrand = offerDataRowSet.platform[0].ToLower();
                        offer.DeviceModel = offerDataRowSet.platform[0].ToLower();
                        offer.DeviceOs = offerDataRowSet.platform[0].ToLower();
                        offer.DeviceOsVersion = "1";
                        offer.Images = image;
                        offer.Status = 1;
                        offer.HasOfferRevenue = 0;
                        offer.Revenue = offerDataRowSet.payout;
                        offer.Payout = (offerDataRowSet.payout * 50 / 100);
                        Offers offers = new Offers();
                        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                        switch (rs)
                        {
                            case 0:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                });
                                break;
                            case 2:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                });
                                break;
                            default:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                });
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                        {
                            Id = offer.CategoryOfferId.ToString(),
                            Name = offer.Name,
                            Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                        });
                    }
                }
            }
        }

        public static void OfferExpandMobile(string link, ApiCategory category, OfferApiMessageList offerApiList)
        {
            Offer offer = new Offer();
            var offerCybobel = AdvApiHelper.GetExpandMobile(link, category.ApiKey);
            if (offerCybobel == null)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = "0",
                    Name = "Api get null",
                    Message = "Api get null"
                });
                return;
            }
            if (offerCybobel.offers != null)
            {
                var offerDataRowsets = offerCybobel.offers;
                foreach (var offerDataRowSet in offerDataRowsets)
                {
                    var index = offerDataRowsets.IndexOf(offerDataRowSet);
                    
                    List<int> countryList = new List<int>();
                    foreach (var countries in offerDataRowSet.relationship.ruleset.countries)
                    {
                        //var indexCountry = offerDataRowSet.country.IndexOf(countries);
                        var country = Countries.FindByCountryCode(countries.country_code.ToUpper());
                        countryList.Add(country.Id);
                    }
                    var countryIds = String.Join(",", countryList);
                    try
                    {
                        offer.CategoryOfferId = offerDataRowSet.network_offer_id;
                        offer.CategoryId = category.CategoryId;
                        offer.Name = offerDataRowSet.name;
                        offer.HasOfferLink = offerDataRowSet.tracking_url;
                        offer.PreviewLink = offerDataRowSet.preview_url;
                        offer.ConversionLimit = offerDataRowSet.daily_conversion_cap;
                        offer.Description = "";
                        offer.DeviceBrand = offerDataRowSet.relationship.ruleset.platforms[0].label.ToLower();
                        offer.DeviceModel = offerDataRowSet.relationship.ruleset.platforms[0].label.ToLower();
                        offer.DeviceOs = offerDataRowSet.relationship.ruleset.platforms[0].label.ToLower();
                        offer.DeviceOsVersion = "1";
                        offer.Images = offerDataRowSet.thumbnail_url;
                        offer.Status = 1;
                        offer.HasOfferRevenue = 0;
                        offer.Revenue = offerDataRowSet.relationship.payouts.entries[0].payout_amount;
                        offer.Payout = (offerDataRowSet.relationship.payouts.entries[0].payout_amount * 50 / 100);
                        Offers offers = new Offers();
                        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                        switch (rs)
                        {
                            case 0:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                });
                                break;
                            case 2:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                });
                                break;
                            default:
                                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                {
                                    Id = offer.CategoryOfferId.ToString(),
                                    Name = offer.Name,
                                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                });
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                        {
                            Id = offer.CategoryOfferId.ToString(),
                            Name = offer.Name,
                            Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                        });
                    }
                }
            }
        }
    }
}