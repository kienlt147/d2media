﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace D2Media_CMS
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Backend

            bundles.Add(new ScriptBundle("~/bundles/backend-js").Include(
                        "~/Content/backend/js/jquery.unobtrusive*",
                        "~/Content/backend/js/jquery.validate*",
                        //                        "~/Content/backend/js/bootstrap.min.js",
                        "~/Content/backend/js/moment-with-locales.js",
                        "~/Content/backend/js/bootstrap-select.js",
                        "~/Content/backend/js/bootstrap-tokenfield.js",
                        "~/Content/backend/js/bootstrap-datetimepicker.js",
                        "~/Content/backend/js/url.js",
                        "~/Content/backend/js/bootstrap-notify.js",
                        "~/Content/backend/js/shieldui-all.min.js",
                        "~/Content/backend/coffee/d2media.min.js",
                        //"~/Content/backend/js/process.js",
                        "~/Content/shared/js/jquery.*", //This entry will catch all new jquery plugins named jquery.xxxxx.js, where xxxxx stands for the name of the new plugin. e.g. jquery.slide.js
                        "~/Content/backend/js/mvcwcms-*" //This entry will catch all new modules named DatPhong_CMS-xxxxx.js, where xxxxx stands for the name of the new module. e.g. DatPhong_CMS-news.js
                                                         //                        "~/Content/shared/js/ays-beforeunload-shim.js"
                                                         //                        "~/Content/backend/coffee/CoffeeScript1.js",
                                                         //                        "~/Content/backend/js/bootstrap-treeview.js"
                        ));

            bundles.Add(new StyleBundle("~/bundles/backend-css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/shared/css/fontawesome-all.css",
                        "~/Content/bootstrap-select.css",
                        "~/Content/bootstrap-tokenfield.css",
                        "~/Content/bootstrap-datetimepicker.css",
                        "~/Content/backend/css/animate.css",
                        "~/Content/shared/css/jquery.*", //This entry will catch all new jquery plugins named jquery.xxxxx.css, where xxxxx stands for the name of the new plugin. e.g. jquery.slide.css
                        "~/Content/backend/css/mvcwcms-*", //This entry will catch all new modules named DatPhong_CMS-xxxxx.css, where xxxxx stands for the name of the new module. e.g. DatPhong_CMS-news.css
                        "~/Content/backend/css/styles.css",
                        "~/Content/backend/css/all.min.css"
                        ));


            //Enables optimization only in Release mode
#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = false;
#endif
        }
    }
}