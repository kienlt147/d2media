﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Canonicalize;
using D2Media_CMS.Models;

namespace D2Media_CMS
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            GlobalConfiguration globalConfiguration = new GlobalConfigurations().GetGlobalConfiguration();

            if (globalConfiguration.IsCanonicalizeActive)
            {
                if (globalConfiguration.HostNameLabel == "www.")
                    routes.Canonicalize().Www();
                else
                    routes.Canonicalize().NoWww();
            }

            //Introduced to support the FrontEndCmsPage.Parameter property in DefaultController
            routes.Canonicalize().TrailingSlash();

            routes.MapRoute(
                name: "Sitemap",
                url: "sitemap.xml",
                defaults: new { controller = "Sitemap", action = "Index" }
            );

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{*AnyAppleTouchIconInTheRoot}", new { AnyAppleTouchIconInTheRoot = @"apple-touch-icon(.*)\.png" });
            routes.IgnoreRoute("{*AllPngFiles}", new { AllPngFiles = @".*\.png(/.*)?" });
            routes.IgnoreRoute("{*AllJpgFiles}", new { AllJpgFiles = @".*\.jpg(/.*)?" });
            routes.IgnoreRoute("{*AllGifFiles}", new { AllGifFiles = @".*\.gif(/.*)?" });
            routes.IgnoreRoute("{*AllPhpFiles}", new { AllPhpFiles = @".*\.php(/.*)?" });
            routes.IgnoreRoute("{*AllXmlFiles}", new { AllXmlFiles = @".*\.xml(/.*)?" });
            routes.IgnoreRoute("{*AllHtmFiles}", new { AllHtmFiles = @".*\.htm(/.*)?" });
            routes.IgnoreRoute("{*AllHtmlFiles}", new { AllHtmlFiles = @".*\.html(/.*)?" });
            routes.IgnoreRoute("{*AllCssFiles}", new { AllCssFiles = @".*\.css(/.*)?" });
            routes.IgnoreRoute("{*AllBsFiles}", new { AllBsFiles = @".*\.bs(/.*)?" });
            routes.IgnoreRoute("{*AllJsFiles}", new { AllBsFiles = @".*\.js(/.*)?" });
            routes.IgnoreRoute("{*AllTxtFiles}", new { AllTxtFiles = @".*\.txt(/.*)?" });
            routes.IgnoreRoute("{*botdetect}", new { botdetect = @"(.*)BotDetectCaptcha\.ashx" });

            routes.MapRoute(
                name: "Mysql",
                url: "Mysql/{action}",
                defaults: new { controller = "Mysql", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Home",
                url: "Home/{action}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ErrorPage",
                url: "ErrorPage/{action}",
                defaults: new { controller = "ErrorPage", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "",
                url: "{action}/{id}",
                defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
