﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class ApiCategory
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Domain { get; set; }
        public string SubQuery { get; set; }
        public string UserName { get; set; }
        public string ApiKey { get; set; }
    }

    public class ApiCategories
    {
        public static int TotalCount { get; set; }
        public List<ApiCategory> FilterAll(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<ApiCategory>("sp_cms_api_categories_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public ApiCategory FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<ApiCategory>("sp_cms_api_category_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? AddEdit(int? id, string title, int categoryId, string domain, string subQuery, string userName, string apiKey)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_api_category_insert_edit",
                    "@Id", id,
                    "@Title", title,
                    "@CategoryId", categoryId,
                    "@Domain", domain,
                    "@SubQuery", subQuery,
                    "@UserName", userName,
                    "@ApiKey", apiKey,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_api_category_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}