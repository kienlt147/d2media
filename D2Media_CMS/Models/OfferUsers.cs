﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_CMS.Models
{
    public class OfferUser
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int OfferId { get; set; }
        public string Link { get; set; }
        public int UniqueClick { get; set; }
        public int Click { get; set; }
        public int? Status { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }

        [DataAnnotationsDisplay("OfferName")]
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public string RequestDate { get; set; }
    }

    public class OfferUsers
    {
        public static int TotalCount { get; set; }

        public OfferUser FindByUserIdOfferId(int? offerId, int? userId)
        {
            var rs = AdoHelper.ExecCachedListProc<OfferUser>("sp_cms_offer_users_select", true, false, null, null,
                "@OfferId", offerId, "@UserId", userId);
            return rs.IgnoreNulls().Any() ? rs.First() : null;
        }

        public List<OfferUser> GetPartnerRequest(int currentPage, int pageSize, 
            string userName, string offerName, int? status, out int total)
        {
            AdoHelper db = new AdoHelper();
            var resultOutput = db.CreateParamOutValue("@TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<OfferUser>("sp_cms_partner_request_offer_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, 
                "@UserName", userName, "@OfferName", offerName, "@Status", status, resultOutput);
            total = (int)resultOutput.Value;
            return rs;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_offer_user_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}