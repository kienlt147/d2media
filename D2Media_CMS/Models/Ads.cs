﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class Ad
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string AdName { get; set; }
        public string AdContent { get; set; }
        public string Link { get; set; }
        public string CategoryName { get; set; }
        public int TotalRecords { get; set; }
    }

    public class Ads
    {
        public List<Ad> FilterAll(int currentPage, int pageSize, string searchText)
        {
            return AdoHelper.ExecCachedListProc<Ad>("sp_cms_ads_select", true, false, null, null, 
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public Ad FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Ad>("sp_cms_ad_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? AddEdit(int? id, int categoryId, string adName, string adContent, string link)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_ad_insert_edit",
                    "@Id", id,
                    "@CategoryId", categoryId,
                    "@AdName", adName,
                    "@AdContent", adContent,
                    "@Link", link,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
            }

            return result;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_ad_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
            }

            return result;
        }
    }
}