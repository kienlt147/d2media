﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;
using Newtonsoft.Json;

namespace D2Media_CMS.Models
{
    public class ReportClick
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public int Click { get; set; }
        public int UniqueClick { get; set; }
        public int Conversion { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ReportClickDetail
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public double Total { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ReportConversion
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public int HasOfferId { get; set; }
        public string HasOfferTransId { get; set; }
        public DateTime CreatedDate { get; set; }
        public double Revenue { get; set; }
        public double Payout { get; set; }
    }

    public class ReportGlobal
    {
        public int OfferId { get; set; }
        public string OfferName { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public int TotalClick { get; set; }
        public int TotalUniqueClick { get; set; }
        public int TotalConversion { get; set; }
        public double Payout { get; set; }
        public double Revenue { get; set; }
        public double TotalPayout { get; set; }
        public double TotalRevenue { get; set; }
        public DateTime CreatedDate { get; set; }
        public string DateFormat { get; set; }
        public int Hour { get; set; }
    }

    public class ReportHour
    {
        public int Hour { get; set; }
        public int TotalClick { get; set; }
        public int TotalConversion { get; set; }
        public double TotalPayout { get; set; }
        public double TotalRevenue { get; set; }
        public string DateFormat { get; set; }
    }

    public class ChartReport
    {
        public string GetCategories(List<ReportHour> listHorHours)
        {
            return listHorHours.IgnoreNulls().Any() ? JsonConvert.SerializeObject(listHorHours.Select(x => x.Hour)) : JsonConvert.SerializeObject("[]");
        }

        public string GetTotalConversion(List<ReportHour> listHorHours)
        {
            return listHorHours.IgnoreNulls().Any() ? JsonConvert.SerializeObject(listHorHours.Select(x => x.TotalConversion)) : JsonConvert.SerializeObject("[]");
        }

        public string GetTotalClick(List<ReportHour> listHorHours)
        {
            return listHorHours.IgnoreNulls().Any() ? JsonConvert.SerializeObject(listHorHours.Select(x => x.TotalClick)) : JsonConvert.SerializeObject("[]");
        }

        public string GetTotalRevenue(List<ReportHour> listHorHours)
        {
            return listHorHours.IgnoreNulls().Any() ? JsonConvert.SerializeObject(listHorHours.Select(x => x.TotalRevenue)) : JsonConvert.SerializeObject("[]");
        }

        public string GetTotalPayout(List<ReportHour> listHorHours)
        {
            return listHorHours.IgnoreNulls().Any() ? JsonConvert.SerializeObject(listHorHours.Select(x => x.TotalPayout)) : JsonConvert.SerializeObject("[]");
        }
    }

    public class Reports
    {
        public List<ReportClick> FilterReportClickByWeek(int userId)
        {
            return AdoHelper.ExecCachedListProc<ReportClick>("sp_cms_report_click_one_week_select", true, false, null, null,
                "@UserId", userId);
        }

        public List<ReportClick> FilterReportCurrentDate(int userId)
        {
            return AdoHelper.ExecCachedListProc<ReportClick>("sp_cms_report_click_current_date_select", true, false, null, null,
                "@UserId", userId);
        }

        public List<ReportConversion> FilterReportPayByWeek(int userId)
        {
            return AdoHelper.ExecCachedListProc<ReportConversion>("sp_cms_report_pay_one_week_select", true, false, null, null,
                "@UserId", userId);
        }

        public List<ReportClickDetail> FilterReportClickByTopOffer(int? userId, int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_click_filter_by_top_offer_select", true, false, null, null,
                "@UserId", userId, "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportClickDetail> FilterAllUserReportClickByTopOffer(int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_click_filter_all_user_by_top_offer_select", true, false, null, null,
                "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportClickDetail> FilterReportConverisonByTopOffer(int? userId, int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_conversion_filter_by_top_offer_select", true, false, null, null,
                "@UserId", userId, "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportClickDetail> FilterAllUserReportConverisonByTopOffer(int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_conversion_filter_all_user_by_top_offer_select", true, false, null, null,
                "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportClickDetail> FilterReportPayoutByTopOffer(int? userId, int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_payout_filter_by_top_offer_select", true, false, null, null,
                "@UserId", userId, "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportClickDetail> FilterAllUserReportPayoutByTopOffer(int topOffer, DateTime startDate, DateTime endDate)
        {
            return AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_payout_filter_all_user_by_top_offer_select", true, false, null, null,
                "@TopOffer", topOffer, "@StartDate", startDate, "@EndDate", endDate);
        }

        public List<ReportConversion> ConversionWithMonth(int diffMonth)
        {
            return AdoHelper.ExecCachedListProc<ReportConversion>("sp_cms_conversion_this_month_select", true, false, null, null,
                "@DiffMonth", diffMonth);
        }

        public List<ReportConversion> ConversionToday(int diffDay)
        {
            return AdoHelper.ExecCachedListProc<ReportConversion>("sp_cms_conversion_today_select", true, false, null, null,
                "@DiffDay", diffDay);
        }

        public List<ReportGlobal> ReportAffiliates(int userId, DateTime startDate, DateTime endDate, int limit)
        {
            return AdoHelper.ExecCachedListProc<ReportGlobal>("sp_cms_affiliate_report_select", true, false, null, null,
                "@UserId", userId, "@StartDate", startDate, "@EndDate", endDate, "@Limit", limit);
        }

        public List<ReportGlobal> ReportDaily(int userId, DateTime startDate, DateTime endDate, int limit)
        {
            return AdoHelper.ExecCachedListProc<ReportGlobal>("sp_cms_daily_report_select", true, false, null, null,
                "@UserId", userId, "@StartDate", startDate, "@EndDate", endDate, "@Limit", limit);
        }

        public List<ReportGlobal> ReportOffer(int userId, DateTime startDate, DateTime endDate, int limit)
        {
            return AdoHelper.ExecCachedListProc<ReportGlobal>("sp_cms_offers_report_select", true, false, null, null,
                "@UserId", userId, "@StartDate", startDate, "@EndDate", endDate, "@Limit", limit);
        }

        public List<ReportGlobal> ReportHourConversion(int userId, DateTime date)
        {
            return AdoHelper.ExecCachedListProc<ReportGlobal>("sp_cms_report_hour_conversion_select", true, false, null, null,
                "@UserId", userId, "@Date", date);
        }

        public List<ReportGlobal> ReportHourClick(int userId, DateTime date)
        {
            return AdoHelper.ExecCachedListProc<ReportGlobal>("sp_cms_report_hour_click_select", true, false, null, null,
                "@UserId", userId, "@Date", date);
        }
    }
}