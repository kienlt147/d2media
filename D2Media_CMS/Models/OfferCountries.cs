﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_CMS.Models
{
    public class OfferCountry
    {
        public int OfferId { get; set; }
        public int CountryId { get; set; }
    }

    public class OfferCountries
    {
        public static int TotalCount { get; set; }

        public List<OfferCountry> FindByOfferId(int? offerId)
        {
            return AdoHelper.ExecCachedListProc<OfferCountry>("sp_cms_offer_countries_select", true, false, null, null,
                "@OfferId", offerId);
        }
    }
}