﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_CMS.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public string Company { get; set; }
        public string Tax { get; set; }
        public string NumberPhone { get; set; }
        public string Skype { get; set; }
        public int Click { get; set; }
        public int Conversion { get; set; }
        public int UniqueClick { get; set; }
        public int TotalConversion { get; set; }
        public double Revenue { get; set; }
        public double Payout { get; set; }
        public string PostBackLink { get; set; }
        public int Status { get; set; }
    }

    public class Users
    {
        private List<User> _AllItems;

        private List<User> GetAllItems(bool force = false)
        {
            return AdoHelper.ExecCachedListProc<User>("sp_admin_users_select", force);
        }

        public Users()
        {
            _AllItems = GetAllItems(true);
        }

        public List<User> TopUserHasConversion(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<User>("sp_cms_user_top_conversion_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public List<User> TopPayoutRevenueUsers(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<User>("sp_cms_user_top_payout_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public List<User> GetAllUsers(string userName = null, string fullName = null, string email = null, int? groupId = null)
        {
            List<User> result = null;

            if (_AllItems.IsNotNull())
            {
                result = (from i in _AllItems
                          where (userName.IsNull() || i.UserName.Contains(userName, StringComparison.OrdinalIgnoreCase))
                          && (fullName.IsNull() || i.FullName.Contains(fullName, StringComparison.OrdinalIgnoreCase))
                          && (email.IsNull() || i.Email.Contains(email, StringComparison.OrdinalIgnoreCase))
                          && (groupId.IsNull() || i.GroupId == groupId)
                          select i).ToList();
            }

            return result;
        }
        
        public List<User> FilterByGroupId(int? groupId = null)
        {
            List<User> result = null;

            if (_AllItems.IsNotNull())
            {
                result = (from i in _AllItems
                          where (groupId.IsNull() || i.GroupId == groupId)
                          select i).ToList();
            }

            return result;
        }

        public List<User> FilterNotAdmin(int? groupId = null)
        {
            List<User> result = null;

            if (_AllItems.IsNotNull())
            {
                result = (from i in _AllItems
                    where (groupId.IsNull() || i.GroupId != groupId)
                    select i).ToList();
            }

            return result;
        }

        public User GetUserByUserName(string userName)
        {
            User result = null;

            if (_AllItems.IsNotNull())
            {
                result = (from i in _AllItems
                          where i.UserName.ToLower() == userName.ToLower()
                          select i).FirstOrDefault();
            }

            return result;
        }

        public User GetUserById(int id)
        {
            User result = null;

            if (_AllItems.IsNotNull())
            {
                result = (from i in _AllItems
                    where i.Id == id
                    select i).FirstOrDefault();
            }

            return result;
        }

        public User GetUserByUserNameAndPassword(string userName, string password)
        {
            User result = null;

            if (_AllItems.IsNotNull())
            {
                string userNameSalt = (from i in _AllItems
                                       where i.UserName.ToLower() == userName.ToLower()
                                       select i.Salt).FirstOrDefault();

                string EncryptedPassword = SecurityHelper.EncryptPassword(password, userNameSalt);

                result = (from i in _AllItems
                          where i.UserName.ToLower() == userName.ToLower() 
                            && i.Password == EncryptedPassword
                            && i.Status == 1
                          select i).FirstOrDefault();
            }

            return result;
        }

        public int? ChangePassword(string userName, string salt, string currentPassword, string newPassword)
        {
            int? result;

            string CurrentEncryptedPassword = SecurityHelper.EncryptPassword(currentPassword, salt);
            string NewEncryptedPassword = SecurityHelper.EncryptPassword(newPassword, salt);

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_change_password", "@UserName", userName, "@CurrentPassword", CurrentEncryptedPassword, "@NewPassword", NewEncryptedPassword, returnValue);
                result = db.GetParamReturnValue(returnValue);
                if (result == 0)
                    BackEndSessions.CurrentUser.Password = NewEncryptedPassword;
                db.Dispose();
            }

            return result;
        }

        public int? Delete(string userName)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_delete", "@UserName", userName, returnValue);
                result = db.GetParamReturnValue(returnValue);
                if (result == 0)
                    _AllItems = GetAllItems(true);
                db.Dispose();
            }

            return result;
        }

        public int? Add(string username, string password, string fullName, string email, 
            int? groupId, string description, string numberPhone, string company, string skype, string postbackLink)
        {
            int? result;

            string salt;
            string encryptedPassword = SecurityHelper.EncryptPassword(password, out salt);

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_insert",
                    "@UserName", username,
                    "@Password", encryptedPassword,
                    "@Salt", salt,
                    "@FullName", fullName,
                    "@Email", email,
                    "@GroupId", groupId,
                    "@Description", description,
                    "@NumberPhone", numberPhone,
                    "@Company", company,
                    "@Skype", skype,
                    "@PostBackLink", postbackLink,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                if (result == 0)
                    _AllItems = GetAllItems(true);
                db.Dispose();
            }

            return result;
        }

        public int? Edit(int? id, string username, string password, string fullName, string email, 
            int? groupId, string description, string numberPhone, string company, string skype, 
            string tax, string postBackLink, int status, string advIds)
        {
            int? result;

            string salt = null;
            string encryptedPassword = null;
            if (password.IsNotEmptyOrWhiteSpace())
            {
                encryptedPassword = SecurityHelper.EncryptPassword(password, out salt);
            }

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_update",
                    "@Id", id,
                    "@UserName", username, 
                    "@Password", encryptedPassword, 
                    "@Salt", salt, 
                    "@FullName", fullName, 
                    "@Email", email, 
                    "@GroupId", groupId,
                    "@Description", description,
                    "@NumberPhone", numberPhone,
                    "@Company", company,
                    "@Skype", skype,
                    "@Tax", tax,
                    "@PostBackLink", postBackLink,
                    "@Status", status,
                    "@AdvIds", advIds,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                if (result == 0)
                    _AllItems = GetAllItems(true);
                db.Dispose();
            }

            return result;
        }

        public int? EditInfo(string username, string fullName, string email, string description, string numberPhone, string company, string skype, string tax)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_info_update",
                    "@UserName", username,
                    "@FullName", fullName,
                    "@Email", email,
                    "@Description", description,
                    "@NumberPhone", numberPhone,
                    "@Company", company,
                    "@Skype", skype,
                    "@Tax", tax,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                if (result == 0)
                    _AllItems = GetAllItems(true);
                db.Dispose();
            }

            return result;
        }
    }
}