﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Net;
using System.Web.Routing;
using System.Resources;
using System.Globalization;
using System.Collections;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;

namespace D2Media_CMS.Models
{
    public static class BackEndSessions
    {
        public static string CurrentMenu
        {
            get
            {
                if (HttpContext.Current.Session["BackEnd_CurrentMenu"] != null)
                    return HttpContext.Current.Session["BackEnd_CurrentMenu"] as string;
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["BackEnd_CurrentMenu"] = value;
            }
        }

        public static User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["BackEnd_CurrentUser"] != null)
                    return HttpContext.Current.Session["BackEnd_CurrentUser"] as User;
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["BackEnd_CurrentUser"] = value;
            }
        }

        public static UserBank CurrentUserBankInfo
        {
            get
            {
                if (HttpContext.Current.Session["BackEnd_CurrentUserBankInfo"] != null)
                    return HttpContext.Current.Session["BackEnd_CurrentUserBankInfo"] as UserBank;
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["BackEnd_CurrentUserBankInfo"] = value;
            }
        }

        public static string SecurityToken
        {
            get
            {
                if (HttpContext.Current.Session["BackEnd_SecurityToken"] != null)
                    return HttpContext.Current.Session["BackEnd_SecurityToken"] as string;
                else
                    return null;
            }
            set
            {
                HttpContext.Current.Session["BackEnd_SecurityToken"] = value;
            }
        }
    }
}