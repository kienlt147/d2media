﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class UserBank
    {
        public int UserId { get; set; }
        public string BeneficiaryName { get; set; }
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string BankAddress { get; set; }
        public string SwiftNumber { get; set; }
        public string Paypal { get; set; }
    }
    public class UserBanks
    {
        public UserBank FilterByUserId(int? userId)
        {
            var rs = AdoHelper.ExecCachedListProc<UserBank>("sp_cms_user_bank_filter_by_userid_select", true, false, null, null,
                "@UserId", userId);
            return rs.IgnoreNulls().Any() ? rs.First() : null;
        }

        public int? AddEdit(int? id, string beneficiaryName, string bankName, string accountNumber, string bankAddress, string swiftNumber, string paypal)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_bank_info_insert_edit",
                    "@UserId", id,
                    "@BeneficiaryName", beneficiaryName,
                    "@BankName", bankName,
                    "@AccountNumber", accountNumber,
                    "@BankAddress", bankAddress,
                    "@SwiftNumber", swiftNumber,
                    "@Paypal", paypal,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
            }

            return result;
        }
    }
}