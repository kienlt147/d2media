﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.Models
{
    public class AdvOffer
    {
        public Int64 id { get; set; }
        public string name { get; set; }
        public string status { get; set; }
        public string category { get; set; }
        public int? offer_approval { get; set; }
        public string offer_approval_msg { get; set; }
        public string tracking_link { get; set; }
        public int? end_date { get; set; }
        public string pricing_type { get; set; }
        public string payout { get; set; }
        public string percent_payout { get; set; }
        public string preview_url { get; set; }
        public string currency { get; set; }
        //public int conversion_protocol { get; set; }
        //public string conversion_protocol_msg { get; set; }
        public string thumbnail { get; set; }
        //public string app_id { get; set; }
        //public List<object> carrier { get; set; }
        //public List<object> offer_url { get; set; }
    }

    public class Target
    {
        public int type { get; set; }
        public string type_msg { get; set; }
        public string country { get; set; }
        public string country_code { get; set; }
        //public List<object> city { get; set; }
    }

    public class OfferGeo
    {
        public int? type { get; set; }
        public string type_msg { get; set; }
        public List<Target> target { get; set; }
    }

    public class Target2
    {
        public string platform { get; set; }
        public string system { get; set; }
        public List<string> version { get; set; }
        public int? is_above { get; set; }
        public string above_version { get; set; }
    }

    public class OfferPlatform
    {
        public List<Target2> target { get; set; }
    }

    public class OfferEvent
    {
        public int event_id { get; set; }
        public int conversion_protocol { get; set; }
        public string conversion_protocol_msg { get; set; }
        public string event_name { get; set; }
        public string event_payout_type { get; set; }
        public string event_payout { get; set; }
        public object event_percent_payout { get; set; }
    }

    public class OfferCap
    {
        //public string cap_type { get; set; }
        //public string cap_type_msg { get; set; }
        public int cap_click { get; set; }
        public int cap_conversion { get; set; }
        //public string cap_budget { get; set; }
        //public string category { get; set; }
        //public object category_msg { get; set; }
        //public string cap_timezone { get; set; }
    }

    public class Rowset
    {
        public AdvOffer offer { get; set; }
        public OfferGeo offer_geo { get; set; }
        public OfferPlatform offer_platform { get; set; }
        //public List<object> offer_creative { get; set; }
        //public List<OfferEvent> offer_event { get; set; }
        //public object offer_vbt { get; set; }
        public OfferCap offer_cap { get; set; }
        //public List<object> offer_postback { get; set; }
    }

    public class Data
    {
        public List<Rowset> rowset { get; set; }
        public int totalPages { get; set; }
        public int totalRows { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class OfferLook
    {
        public int code { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
    }

    //
    // Orangear Api ////////////////////////////////////////////////////////
    //
    public class Orangear
    {
        public bool success { get; set; }
        public dynamic offers { get; set; }
    }

    //
    //Adxmi Api ////////////////////////////////////////////////////////////
    //
    public class TrafficSettings
    {
        public int incentive { get; set; }
        public int adult { get; set; }
        public int push { get; set; }
        public int search { get; set; }
        public int inapp { get; set; }
        public int facebook { get; set; }
        public int wifi { get; set; }
        public int is3g { get; set; }
        public int sms { get; set; }
        public int email { get; set; }
    }

    public class AdxmiOffer
    {
        public Int64 offer_id { get; set; }
        public string offer_name { get; set; }
        public double offer_payout { get; set; }
        public string offer_status { get; set; }
        public string offer_type { get; set; }
        public string system_type { get; set; }
        public string traffic_type { get; set; }
        public string conversion_flow { get; set; }
        //public List<object> conversion_type { get; set; }
        public string offer_description { get; set; }
        public string kpi { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public List<string> country { get; set; }
        public List<string> categories { get; set; }
        public string preview_url { get; set; }
        public string creative_url { get; set; }
        public TrafficSettings traffic_settings { get; set; }
        //public List<object> carriers { get; set; }
        public int daily_cap { get; set; }
        public int monthly_cap { get; set; }
        public string platform { get; set; }
        public string payout_type { get; set; }
        public string tracking_link { get; set; }
        public string package_name { get; set; }
        public int itunes_id { get; set; }
        public string icon_url { get; set; }
    }

    public class D
    {
        public int max_record { get; set; }
        public int max_page { get; set; }
        public int page { get; set; }
        public int limit { get; set; }
        public List<AdxmiOffer> offers { get; set; }
    }

    public class Adxmi
    {
        public int c { get; set; }
        public D d { get; set; }
    }

    //
    // Affixate Affise /////////////////////////////////////////////////////////
    //
    public class AffisePayment
    {
        public List<string> countries { get; set; }
        //public List<object> cities { get; set; }
        //public List<object> devices { get; set; }
        public List<string> os { get; set; }
        public string goal { get; set; }
        public double revenue { get; set; }
        public string currency { get; set; }
        //public string title { get; set; }
        //public string type { get; set; }
        //public bool country_exclude { get; set; }
    }

    public class AffiseGoals
    {
    }

    public class AffiseCap
    {
        public string goal_type { get; set; }
        public string period { get; set; }
        public string type { get; set; }
        public int value { get; set; }
        //public AffiseGoals goals { get; set; }
    }

    public class AffiseLink
    {
        //public object id { get; set; }
        //public object title { get; set; }
        //public object hash { get; set; }
        public string url { get; set; }
        //public List<object> postbacks { get; set; }
        //public object created { get; set; }
    }

    public class AffiseKpi
    {
        public string en { get; set; }
    }

    public class AffiseOffer
    {
        public int id { get; set; }
        public string offer_id { get; set; }
        public string title { get; set; }
        public string preview_url { get; set; }
        public string description { get; set; }
        //public int cr { get; set; }
        //public int epc { get; set; }
        //public string logo { get; set; }
        //public string logo_source { get; set; }
        //public string stop_at { get; set; }
        //public List<object> sources { get; set; }
        //public List<object> categories { get; set; }
        //public List<object> full_categories { get; set; }
        public List<string> countries { get; set; }
        public List<AffisePayment> payments { get; set; }
        public List<AffiseCap> caps { get; set; }
        //public int cap { get; set; }
        //public bool required_approval { get; set; }
        //public int strictly_country { get; set; }
        //public List<object> strictly_os { get; set; }
        //public List<object> strictly_brands { get; set; }
        //public bool is_cpi { get; set; }
        //public List<object> creatives { get; set; }
        //public object creatives_zip { get; set; }
        //public object impressions_link { get; set; }
        //public List<object> landings { get; set; }
        public List<AffiseLink> links { get; set; }
        //public object macro_url { get; set; }
        public string link { get; set; }
        //public bool use_https { get; set; }
        //public bool use_http { get; set; }
        //public int hold_period { get; set; }
        //public AffiseKpi kpi { get; set; }
        //public string click_session { get; set; }
        //public List<object> strictly_isp { get; set; }
        //public List<object> restriction_isp { get; set; }
    }

    public class Affise
    {
        public int status { get; set; }
        public List<AffiseOffer> offers { get; set; }
    }

    //
    // CP-Ology /////////////////////////////////////////////////////////
    //
    public class OlogyPayouts
    {
        public double US { get; set; }
    }

    public class OlogyDevice
    {
        public string name { get; set; }
        //public object min_os_version { get; set; }
    }

    public class OlogyCaps
    {
        public int overall_cap { get; set; }
        public int daily_cap { get; set; }
        public int weekly_cap { get; set; }
        public int monthly_cap { get; set; }
        public int overall_payout_cap { get; set; }
        public int daily_payout_cap { get; set; }
        public int weekly_payout_cap { get; set; }
        public int monthly_payout_cap { get; set; }
    }

    public class OlogyOffer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string currency { get; set; }
        public string preview_url { get; set; }
        //public object app_code { get; set; }
        public string icon_url { get; set; }
        public string description { get; set; }
        public string payout_model { get; set; }
        public string incent_type { get; set; }
        public string status { get; set; }
        //public object expiration_date { get; set; }
        public int approved { get; set; }
        public string tracking_link { get; set; }
        public int requires_approval { get; set; }
        public int kpi { get; set; }
        public string time_zone { get; set; }
        //public object restrict_countries { get; set; }
        public string verticals { get; set; }
        public int dynamic_payout { get; set; }
        public OlogyPayouts payouts { get; set; }
        public List<OlogyDevice> devices { get; set; }
        public OlogyCaps caps { get; set; }
    }

    public class Ology
    {
        public bool success { get; set; }
        public object error { get; set; }
        public int offer_total { get; set; }
        public int page_index { get; set; }
        public int page_size { get; set; }
        public int page_total { get; set; }
        public List<OlogyOffer> offers { get; set; }
    }

    //
    // Cygobel ////////////////////////////////////////////////////
    //
    public class CygobelCreative
    {
        public string type { get; set; }
        public string url { get; set; }
    }

    public class CygobelOffer
    {
        public int id { get; set; }
        public string name { get; set; }
        public string daily_cap { get; set; }
        public double payout { get; set; }
        public string type { get; set; }
        public List<CygobelCreative> creatives { get; set; }
        public List<object> white_list { get; set; }
        public List<object> black_list { get; set; }
        public string action { get; set; }
        public List<string> countries { get; set; }
        public string budget { get; set; }
        public string required_gaid_idfa { get; set; }
        public string preview_url { get; set; }
        public string city { get; set; }
        public string kpi { get; set; }
        public string os_version { get; set; }
        public List<string> platform { get; set; }
        public List<string> traffic_allowed { get; set; }
        public string tracking_url { get; set; }
    }

    public class Cygobel
    {
        public int err { get; set; }
        public string errdesc { get; set; }
        public int total_offers { get; set; }
        public int num_pages { get; set; }
        public int page { get; set; }
        public int page_size { get; set; }
        public List<CygobelOffer> offers { get; set; }
    }

    //
    // Expand Mobile //////////////////////////////////////////////////
    //
    public class ExpandCategory
    {
        public int network_category_id { get; set; }
        public int network_id { get; set; }
        public string name { get; set; }
        public int time_created { get; set; }
        public int time_saved { get; set; }
    }

    public class ExpandCreatives
    {
        public int total { get; set; }
        //public List<object> entries { get; set; }
    }

    public class ExpandReporting
    {
        public int imp { get; set; }
        public int total_click { get; set; }
        public int unique_click { get; set; }
        public int invalid_click { get; set; }
        public int duplicate_click { get; set; }
        public int ctr { get; set; }
        public int cv { get; set; }
        public int view_through_cv { get; set; }
        public int @event { get; set; }
        public int cvr { get; set; }
        public int evr { get; set; }
        public int rpc { get; set; }
        public int rpm { get; set; }
        public int revenue { get; set; }
    }

    public class ExpandEntry
    {
        public int network_offer_payout_revenue_id { get; set; }
        public bool custom_payout_overwrite { get; set; }
        public int network_custom_payout_revenue_setting_id { get; set; }
        public string payout_type { get; set; }
        public string entry_name { get; set; }
        public double payout_amount { get; set; }
        public int payout_percentage { get; set; }
        public bool is_default { get; set; }
        public bool is_postback_disabled { get; set; }
    }

    public class ExpandPayouts
    {
        public int total { get; set; }
        public List<ExpandEntry> entries { get; set; }
    }

    public class ExpandPlatform
    {
        public int network_id { get; set; }
        public int network_targeting_platform_id { get; set; }
        public int platform_id { get; set; }
        public string label { get; set; }
        public string targeting_type { get; set; }
        public string match_type { get; set; }
    }

    public class ExpandOsVersion
    {
        public int network_id { get; set; }
        public int network_targeting_os_version_id { get; set; }
        public int platform_id { get; set; }
        public int os_version_id { get; set; }
        public string label { get; set; }
        public string targeting_type { get; set; }
        public string match_type { get; set; }
    }

    public class ExpandCountry
    {
        public int network_id { get; set; }
        public int network_targeting_country_id { get; set; }
        public int country_id { get; set; }
        public string label { get; set; }
        public string country_code { get; set; }
        public string targeting_type { get; set; }
        public string match_type { get; set; }
    }

    public class ExpandRuleset
    {
        public int network_id { get; set; }
        public int network_ruleset_id { get; set; }
        public int time_created { get; set; }
        public int time_saved { get; set; }
        public List<ExpandPlatform> platforms { get; set; }
        //public List<object> device_types { get; set; }
        public List<ExpandOsVersion> os_versions { get; set; }
        //public List<object> browsers { get; set; }
        //public List<object> languages { get; set; }
        public List<ExpandCountry> countries { get; set; }
        //public List<object> regions { get; set; }
        //public List<object> cities { get; set; }
        //public List<object> dmas { get; set; }
        //public List<object> mobile_carriers { get; set; }
        //public List<object> connection_types { get; set; }
        //public List<object> ips { get; set; }
        public bool is_block_proxy { get; set; }
        public bool is_use_day_parting { get; set; }
        public string day_parting_apply_to { get; set; }
        public int day_parting_timezone_id { get; set; }
        //public List<object> days_parting { get; set; }
        //public List<object> isps { get; set; }
    }

    public class ExpandUrls
    {
        public int total { get; set; }
        //public List<object> entries { get; set; }
    }

    public class ExpandCustomPayoutSettings
    {
        public int total { get; set; }
        //public List<object> entries { get; set; }
    }

    public class ExpandRemainingCaps
    {
        public int remaining_daily_payout_cap { get; set; }
        public int remaining_daily_conversion_cap { get; set; }
        public int remaining_daily_click_cap { get; set; }
    }

    public class ExpandRelationship
    {
        public Category category { get; set; }
        public ExpandCreatives creatives { get; set; }
        public ExpandReporting reporting { get; set; }
        public ExpandPayouts payouts { get; set; }
        public string offer_affiliate_status { get; set; }
        public ExpandRuleset ruleset { get; set; }
        public ExpandUrls urls { get; set; }
        public ExpandCustomPayoutSettings custom_payout_settings { get; set; }
        public ExpandRemainingCaps remaining_caps { get; set; }
    }

    public class ExpandOffer
    {
        public int network_offer_id { get; set; }
        public int network_id { get; set; }
        public string name { get; set; }
        public string thumbnail_url { get; set; }
        public int network_category_id { get; set; }
        public string preview_url { get; set; }
        public string offer_status { get; set; }
        public string currency_id { get; set; }
        public string date_live_until { get; set; }
        public string html_description { get; set; }
        public string terms_and_conditions { get; set; }
        public string visibility { get; set; }
        public bool is_caps_enabled { get; set; }
        public bool is_using_suppression_list { get; set; }
        public int suppression_list_id { get; set; }
        public int network_tracking_domain_id { get; set; }
        public int daily_conversion_cap { get; set; }
        public int weekly_conversion_cap { get; set; }
        public int monthly_conversion_cap { get; set; }
        public int global_conversion_cap { get; set; }
        public int daily_payout_cap { get; set; }
        public int weekly_payout_cap { get; set; }
        public int monthly_payout_cap { get; set; }
        public int global_payout_cap { get; set; }
        public int daily_click_cap { get; set; }
        public int weekly_click_cap { get; set; }
        public int monthly_click_cap { get; set; }
        public int global_click_cap { get; set; }
        public string tracking_url { get; set; }
        public string app_identifier { get; set; }
        public int time_created { get; set; }
        public int time_saved { get; set; }
        public ExpandRelationship relationship { get; set; }
    }

    public class ExpandMobile
    {
        public List<ExpandOffer> offers { get; set; }
    }
}