﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_CMS.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
    }

    public class Countries
    {
        private List<Country> _AllItems;

        public static int TotalCount { get; set; }

        public List<Country> GetAllItems(int currentPage, int pageSize, string searchText)
        {
            AdoHelper db = new AdoHelper();
            var totalCount = db.CreateParamOutValue("TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Country>("sp_cms_countries_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText, totalCount);
            TotalCount = (int) totalCount.Value;
            return rs;
        }

        public static List<Country> GetAll(int currentPage, int pageSize, string searchText)
        {
            AdoHelper db = new AdoHelper();
            var totalCount = db.CreateParamOutValue("TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Country>("sp_cms_countries_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText, totalCount);
            TotalCount = (int)totalCount.Value;
            return rs;
        }

        public static Country FindByCountryCode(string countryCode)
        {
            var rs = AdoHelper.ExecCachedListProc<Country>("sp_cms_country_find_by_code_select", true, false, null, null,
                "@CountryCode", countryCode);
            return rs.IgnoreNulls().Any() ? rs.First() : null;
        }
    }
}