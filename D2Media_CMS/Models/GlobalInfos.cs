﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Routing.Constraints;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class GlobalInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Status { get; set; }
    }

    public class GlobalInfos
    {
        public List<GlobalInfo> FindAll(string name, string type)
        {
            return AdoHelper.ExecCachedListProc<GlobalInfo>("sp_cms_global_info_select", true, true, null, null,
                "@Name", name, "@Type", type);
        }

        public List<GlobalInfo> FindByType(string type)
        {
            return AdoHelper.ExecCachedListProc<GlobalInfo>("sp_cms_find_by_type_select", true, false, null, null,
                "@Type", type);
        }

        public GlobalInfo FindById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<GlobalInfo>("sp_cms_find_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public List<GlobalInfo> FindBy(int? videoId, string type)
        {
            return AdoHelper.ExecCachedListProc<GlobalInfo>("sp_cms_find_by_videoid_type_select", true, false, null, null,
                "@VideoId", videoId, "@Type", type);
        }

        public int? AddEdit(int id, string name, string type)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecScalarProc("sp_cms_video_global_infos_insert_update",
                    "@Id", id,
                    "@Name", name,
                    "@Type", type,
                    returnValue).ConvertTo<int?>(null, true);
                result = db.GetParamReturnValue(returnValue);
            }
            return result;
        }

        public int? AddEditVideoGlobalInfo(int? videoId, int? globalInfoId)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecScalarProc("sp_cms_video_global_infos_insert",
                    "@GlobalInfoId", globalInfoId,
                    "@VideoId", videoId,
                    returnValue).ConvertTo<int?>(null, true);
                result = db.GetParamReturnValue(returnValue);
            }
            return result;
        }

        public int? AddInVideo(int? id, string name)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecScalarProc("sp_cms_video_tag_insert_update",
                    "@VideoId", id,
                    "@Name", name,
                    returnValue).ConvertTo<int?>(null, true);
                result = db.GetParamReturnValue(returnValue);
            }
            return result;
        }

        public int? AddInArticle(int? id, string name)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecScalarProc("sp_cms_article_tag_insert_update",
                    "@ArticleId", id,
                    "@Name", name,
                    returnValue).ConvertTo<int?>(null, true);
                result = db.GetParamReturnValue(returnValue);
            }
            return result;
        }

        public int? Delete(int id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_global_info_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
            }
            return result;
        }
    }
}