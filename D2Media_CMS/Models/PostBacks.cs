﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class Postback
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Url { get; set; }
        public int Status { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class PostBacks
    {
        public static int TotalCount { get; set; }
        public List<Postback> GetAll(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<Postback>("sp_cms_postbacks_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public Postback FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Postback>("sp_cms_postback_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? AddEdit(int? id, int userId, string url)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_postback_insert_edit",
                    "@Id", id,
                    "@UserId", userId,
                    "@Url", url,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_postback_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}