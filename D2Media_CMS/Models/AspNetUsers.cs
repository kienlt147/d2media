﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class AspNetUser
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class AspNetUsers
    {
        public List<AspNetUser> FilterAll()
        {
            return AdoHelper.ExecCachedListProc<AspNetUser>("sp_cms_asp_net_users_select", true, false, null, null);
        }
    }
}