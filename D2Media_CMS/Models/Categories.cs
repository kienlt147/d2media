﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string TransId { get; set; }
        public string Sub1 { get; set; }
        public string Sub2 { get; set; }
        public string Sub3 { get; set; }
        public string Sub4 { get; set; }
        public string Sub5 { get; set; }
        public string CountryCode { get; set; }
        public string AddressIp { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceOsVersion { get; set; }
        public int Status { get; set; }
    }

    public class Categories
    {
        public static int TotalCount { get; set; }
        public List<Category> FilterAll(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<Category>("sp_cms_categories_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public Category FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Category>("sp_cms_category_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? AddEdit(int? id, string name, string description, string transId, string sub1,
            string sub2, string sub3, string sub4, string sub5, string countryCode, string addressIp, 
            string deviceOs, string deviceOsVersion, int status)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_category_insert_edit",
                    "@Id", id,
                    "@CategoryName", name,
                    "@Description", description,
                    "@TransId", transId,
                    "@Sub1", sub1,
                    "@Sub2", sub2,
                    "@Sub3", sub3,
                    "@Sub4", sub4,
                    "@Sub5", sub5,
                    "@CountryCode", countryCode,
                    "@AddressIp", addressIp,
                    "@DeviceOs", deviceOs,
                    "@DeviceOsVersion", deviceOsVersion,
                    "@Status", status,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_category_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}