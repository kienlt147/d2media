﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Microsoft.SqlServer.Management.Smo.Agent;

namespace D2Media_CMS.Models
{
    public class Offer
    {
        public int Id { get; set; }
        public Int64 CategoryOfferId { get; set; }

        [DataAnnotationsDisplay("AdvName")]
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public string HasOfferLink { get; set; }
        public double HasOfferRevenue { get; set; }
        public int ConversionLimit { get; set; }
        public string PreviewLink { get; set; }
        public string Description { get; set; }
        public string DeviceBrand { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceOsVersion { get; set; }
        public int Status { get; set; }
        public string Images { get; set; }
        public double Revenue { get; set; }
        public double Payout { get; set; }
        public string CountryName { get; set; }
        public int Click { get; set; }
        public int Conversion { get; set; }
        public int UniqueClick { get; set; }
        public string ResetTime { get; set; }
    }

    public class Offers
    {
        public static int TotalCount { get; set; }

        public List<Offer> GetAll(int currentPage, int pageSize, string searchText, int? categoryId, out int total)
        {
            AdoHelper db = new AdoHelper();
            var resultOutput = db.CreateParamOutValue("@TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_offers_select", true, false, null, null,
                "@PageNumber", currentPage, 
                "@PageSize", pageSize, 
                "@SearchText", searchText,
                "@CategoryId", categoryId, 
                resultOutput);
            total = (int)resultOutput.Value;
            return rs;
        }

        public List<Offer> GetOfferByUserId(int currentPage, int pageSize, string searchText, int? categoryId, int userId, out int total)
        {
            AdoHelper db = new AdoHelper();
            var resultOutput = db.CreateParamOutValue("@TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_my_offers_select", true, false, null, null,
                "@PageNumber", currentPage,
                "@PageSize", pageSize,
                "@SearchText", searchText,
                "@CategoryId", categoryId,
                "@UserId", userId, 
                resultOutput);
            total = (int)resultOutput.Value;
            return rs;
        }

        public List<Offer> GetAllOfferPartner(int currentPage, int pageSize, string searchText, out int total)
        {
            AdoHelper db = new AdoHelper();
            var resultOutput = db.CreateParamOutValue("@TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_offer_partner_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText, resultOutput);
            total = (int)resultOutput.Value;
            return rs;
        }

        public List<Offer> GetAllOfferPartnerApi(int currentPage, int pageSize, string searchText, out int total)
        {
            AdoHelper db = new AdoHelper();
            var resultOutput = db.CreateParamOutValue("@TotalCount", SqlDbType.Int);
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_offer_partner_api_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText, resultOutput);
            total = (int)resultOutput.Value;
            return rs;
        }

        public List<Offer> TopOfferHasConversion(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<Offer>("sp_cms_offer_top_conversion_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public Offer FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_offer_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? AddEdit(int? id, string name, string hasOfferLink, double hasOfferRevenue, int conversionLimit, 
            string prevewLink, string description, string deviceBrand, string deviceModel, 
            string deviceOs, string deviceOsVersion, string images, double revenue, double payout, 
            string countryIds, int categoryId, string resetTime)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_offer_insert_edit",
                    "@Id", id,
                    "@Name", name,
                    "@HasOfferLink", hasOfferLink,
                    "@HasOfferRevenue", hasOfferRevenue,
                    "@ConversionLimit", conversionLimit,
                    "@PreviewLink", prevewLink,
                    "@Description", description,
                    "@DeviceBrand", deviceBrand,
                    "@DeviceModel", deviceModel,
                    "@DeviceOs", deviceOs,
                    "@DeviceOsVersion", deviceOsVersion,
                    "@Images", images,
                    "@Revenue", revenue,
                    "@Payout", payout,
                    "@CountryIds", countryIds,
                    "@CategoryId", categoryId,
                    "@ResetTime", resetTime,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? AddEditGetApi(Int64? categoryOfferid, string name, string hasOfferLink, double hasOfferRevenue, int conversionLimit,
            string prevewLink, string description, string deviceBrand, string deviceModel,
            string deviceOs, string deviceOsVersion, string images, double revenue, double payout,
            string countryIds, int categoryId, string resetTime)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_offer_get_api_insert_edit",
                    "@CategoryOfferId", categoryOfferid,
                    "@Name", name,
                    "@HasOfferLink", hasOfferLink,
                    "@HasOfferRevenue", hasOfferRevenue,
                    "@ConversionLimit", conversionLimit,
                    "@PreviewLink", prevewLink,
                    "@Description", description,
                    "@DeviceBrand", deviceBrand,
                    "@DeviceModel", deviceModel,
                    "@DeviceOs", deviceOs,
                    "@DeviceOsVersion", deviceOsVersion,
                    "@Images", images,
                    "@Revenue", revenue,
                    "@Payout", payout,
                    "@CountryIds", countryIds,
                    "@CategoryId", categoryId,
                    "@ResetTime", resetTime,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? UpdateStatus(int? id, int status)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_offer_update_status",
                    "@Id", id,
                    "@Status", status,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? Delete(int? id)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_cms_offer_delete", "@Id", id, returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}