﻿using D2Media_CMS.Models;
using System.Web.Mvc;
using D2Media_CMS.ViewModels;
using System.Collections.Generic;
using System.Collections;
using System;
using System.IO;
using System.Text;
using System.Web;
using D2Media_CMS.Helpers;
using System.Web.Script.Serialization;
using D2Media_CMS.Filters;
using System.Linq;
using System.Web.Routing;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Web.Configuration;
using Microsoft.IdentityModel.Tokens;
using Offer = D2Media_API.Models.Offer;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        //  /Admin/
        [IsRestricted]
        public ActionResult Index()
        {
            Reports reports = new Reports();
            Offers offers = new Offers();
            Users users = new Users();
            DashboardList dashboardList = new DashboardList
            {
                ReportClicks = reports.FilterReportClickByWeek(BackEndSessions.CurrentUser.Id),
                ReportPayList = reports.FilterReportPayByWeek(BackEndSessions.CurrentUser.Id),
                ConversionThisMonth = reports.ConversionWithMonth(0),
                ConversionLastMonth = reports.ConversionWithMonth(-1),
                ConversionToday = reports.ConversionToday(0),
                ConversionYesterday = reports.ConversionToday(-1),
                TopOfferHasConversion = offers.TopOfferHasConversion(1, 5),
                TopPayoutRevenueUsers = users.TopPayoutRevenueUsers(1, 5)
            };
            return View(dashboardList);
        }

        // /Admin/ClearCache/
        [IsRestricted]
        public ActionResult ClearCache()
        {
            HttpRuntime.UnloadAppDomain();

            return RedirectToAction("Index");
        }

        // /Admin/Uninstall/
        [IsRestricted]
        public ActionResult UninstallCms()
        {
            Configuration webConfigConfiguration = WebConfigurationManager.OpenWebConfiguration("~");
            webConfigConfiguration.ConnectionStrings.ConnectionStrings.Remove("MainConnectionString");
            webConfigConfiguration.Save();

            return Redirect("~/");
        }

        //  /Admin/Login/
        [HttpGet]
        [IsRestricted]
        public ActionResult Login(string ReturnUrl)
        {
            AdminPage backEndPage = new AdminPage();
            backEndPage.PageName = "Login";
            ViewBag.AdminPage = backEndPage;

            BackEndLogin backEndLogin = new BackEndLogin()
            {
                ReturnUrl = ReturnUrl
            };

            return View(backEndLogin);
        }
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult Login(BackEndLogin backEndLogin, string ReturnUrl)
        {
            AdminPage backEndPage = new AdminPage();
            backEndPage.PageName = "Login";
            ViewBag.AdminPage = backEndPage;

            if (ModelState.IsValidOrRefresh())
            {
                Users users = new Users();
                UserBanks userBanks = new UserBanks();
                User user = users.GetUserByUserNameAndPassword(backEndLogin.Username, backEndLogin.Password);
                if (user.IsNotNull())
                {
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.SuccessfullyLoggedIn);
                    var userBank = userBanks.FilterByUserId(user.Id);
                    BackEndSessions.CurrentUser = user;
                    BackEndSessions.CurrentUserBankInfo = userBank;

                    string key = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
                    var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
                    // Also note that securityKey length should be >256b
                    // so you have to make sure that your private key has a proper length
                    //
                    var credentials = new Microsoft.IdentityModel.Tokens.SigningCredentials
                        (securityKey, SecurityAlgorithms.HmacSha256Signature);
                    //  Finally create a Token
                    var header = new JwtHeader(credentials);
                    var payload = new JwtPayload
                    {
                        { "UserId", user.Id },
                        { "UserName", user.UserName },
                        { "Expiration", DateTime.Now.AddHours(2).ToString("yyyyMMddHHmmssffff") }
                    };

                    var secToken = new JwtSecurityToken(header, payload);
                    
                    var handler = new JwtSecurityTokenHandler();

                    // Token to String so you can use it in your client
                    var tokenString = handler.WriteToken(secToken);
                    BackEndSessions.SecurityToken = tokenString;
                    // And finally when  you received token from client
                    // you can  either validate it or try to  read
                    //var token = handler.ReadJwtToken(tokenString);

                    AdminPages backEndPages = new AdminPages();
                    BackEndSessions.CurrentMenu = backEndPages.GetMenuByGroupId(user.GroupId);

                    if (ReturnUrl.IsNotEmptyOrWhiteSpace())
                    {
                        return Redirect(HttpUtility.UrlDecode(ReturnUrl));
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UsernameOrPasswordNotValid);
                }
            }

            return View(backEndLogin);
        }

        [HttpPost]
        [IsRestricted]
        public ActionResult Register(BackEndRegister backEndRegister)
        {
            if (ModelState.IsValidOrRefresh())
            {
                Users users = new Users();
                int? result = users.Add(backEndRegister.username, backEndRegister.password,
                    backEndRegister.fullName, backEndRegister.email, 0, null, null, null, null, null);
                switch (result)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAdded);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UsernameAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }

            return RedirectToAction("Login");
        }

        //  /Admin/Logout/
        [IsRestricted]
        public ActionResult Logout()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }

        //  /Admin/ChangePassword/
        [HttpGet]
        [IsRestricted]
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(BackEndChangePassword backEndChangePassword)
        {
            if (ModelState.IsValidOrRefresh())
            {
                Users users = new Users();
                int? result = users.ChangePassword(BackEndSessions.CurrentUser.UserName, BackEndSessions.CurrentUser.Salt, backEndChangePassword.CurrentPassword, backEndChangePassword.NewPassword);
                switch (result)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.PasswordSuccessfullyChanged);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.CurrentPasswordNotValid);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }

            }

            return View(backEndChangePassword);
        }

        //  /Admin/GetCmsModules/
        public ActionResult GetCmsModules()
        {
            return Json(ExtensionsHelper.GetCmsModules(false), JsonRequestBehavior.AllowGet);
        }

        //  /Admin/GetContentTemplates/
        public ActionResult GetContentTemplates()
        {
            ContentTemplates contentTemplates = new ContentTemplates();
            return Json(contentTemplates.GetAllContentTemplates(isActive: true), JsonRequestBehavior.AllowGet);
        }

        //  /Admin/IsSessionActive/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IsSessionActive()
        {
            return Content(BackEndSessions.CurrentUser.IsNotNull().ToString(), "text/plain");
        }

        //  /Admin/IsPageBrowseAuthorized/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult IsPageBrowseAuthorized(string id)
        {
            AdminPages backEndPages = new AdminPages();
            AdminPage backEndPage = backEndPages.GetPageByAction(id);
            return Content(backEndPages.IsPermissionGranted(backEndPage.PageId, PermissionCode.Browse).ToString(), "text/plain");
        }

        //  /Admin/GetUniqueKey/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetUniqueKey()
        {
            return Content(DateTime.Now.Ticks.ToBase36(), "text/plain");
        }

        //  /Admin/GlobalConfiguration/
        [HttpGet]
        [IsRestricted]
        public ActionResult GlobalConfiguration()
        {
            BackEndGlobalConfigurationEdit backEndGlobalConfigurationEdit = new BackEndGlobalConfigurationEdit();

            GlobalConfigurations backEndGlobalConfigurations = new GlobalConfigurations();
            GlobalConfiguration globalConfiguration = backEndGlobalConfigurations.GetGlobalConfiguration();
            if (globalConfiguration.IsNotNull())
            {
                backEndGlobalConfigurationEdit.SiteName = globalConfiguration.SiteName;
                backEndGlobalConfigurationEdit.MetaTitle = globalConfiguration.MetaTitle;
                backEndGlobalConfigurationEdit.MetaKeywords = globalConfiguration.MetaKeywords;
                backEndGlobalConfigurationEdit.MetaDescription = globalConfiguration.MetaDescription;
                backEndGlobalConfigurationEdit.Robots = globalConfiguration.Robots;
                backEndGlobalConfigurationEdit.NotificationEmail = globalConfiguration.NotificationEmail;
                backEndGlobalConfigurationEdit.IsCanonicalizeActive = globalConfiguration.IsCanonicalizeActive;
                backEndGlobalConfigurationEdit.HostNameLabel = globalConfiguration.HostNameLabel;
                backEndGlobalConfigurationEdit.DomainName = globalConfiguration.DomainName;
                backEndGlobalConfigurationEdit.BingVerificationCode = globalConfiguration.BingVerificationCode;
                backEndGlobalConfigurationEdit.GoogleVerificationCode = globalConfiguration.GoogleVerificationCode;
                backEndGlobalConfigurationEdit.GoogleAnalyticsTrackingCode = globalConfiguration.GoogleAnalyticsTrackingCode;
                backEndGlobalConfigurationEdit.GoogleSearchCode = globalConfiguration.GoogleSearchCode;
                backEndGlobalConfigurationEdit.IsOffline = globalConfiguration.IsOffline;
                backEndGlobalConfigurationEdit.OfflineCode = globalConfiguration.OfflineCode;
                backEndGlobalConfigurationEdit.ServerTimeZone = globalConfiguration.ServerTimeZone;
                backEndGlobalConfigurationEdit.DateFormat = globalConfiguration.DateFormat;
                backEndGlobalConfigurationEdit.TimeFormat = globalConfiguration.TimeFormat;
                backEndGlobalConfigurationEdit.DefaultLanguageCode = globalConfiguration.DefaultLanguageCode;
                backEndGlobalConfigurationEdit.DefaultErrorPageTemplateId = globalConfiguration.DefaultErrorPageTemplateId;
            }
            else
            {
                ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                ViewData.IsFormVisible(false);
            }

            return View(backEndGlobalConfigurationEdit);
        }
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult GlobalConfiguration(BackEndGlobalConfigurationEdit backEndGlobalConfigurationEdit)
        {
            GlobalConfigurations backEndGlobalConfigurations = new GlobalConfigurations();
            GlobalConfiguration globalConfiguration = backEndGlobalConfigurations.GetGlobalConfiguration();
            int? result = backEndGlobalConfigurations.Edit(
                backEndGlobalConfigurationEdit.SiteName,
                backEndGlobalConfigurationEdit.MetaTitle,
                backEndGlobalConfigurationEdit.MetaKeywords,
                backEndGlobalConfigurationEdit.MetaDescription,
                backEndGlobalConfigurationEdit.Robots,
                backEndGlobalConfigurationEdit.NotificationEmail,
                backEndGlobalConfigurationEdit.IsCanonicalizeActive,
                backEndGlobalConfigurationEdit.HostNameLabel,
                backEndGlobalConfigurationEdit.DomainName,
                backEndGlobalConfigurationEdit.BingVerificationCode,
                backEndGlobalConfigurationEdit.GoogleVerificationCode,
                backEndGlobalConfigurationEdit.GoogleAnalyticsTrackingCode,
                backEndGlobalConfigurationEdit.GoogleSearchCode,
                backEndGlobalConfigurationEdit.IsOffline,
                backEndGlobalConfigurationEdit.OfflineCode,
                backEndGlobalConfigurationEdit.ServerTimeZone,
                backEndGlobalConfigurationEdit.DateFormat,
                backEndGlobalConfigurationEdit.TimeFormat,
                backEndGlobalConfigurationEdit.DefaultLanguageCode,
                backEndGlobalConfigurationEdit.DefaultErrorPageTemplateId);
            switch (result)
            {
                case 0:
                    if (globalConfiguration.HostNameLabel != backEndGlobalConfigurationEdit.HostNameLabel)
                    {
                        //Updates RouteTable
                        using (RouteTable.Routes.GetWriteLock())
                        {
                            RouteTable.Routes.Clear();
                            RouteConfig.RegisterRoutes(RouteTable.Routes);
                        }
                    }

                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyEdited);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    ViewData.IsFormVisible(false);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }

            return View(backEndGlobalConfigurationEdit);
        }

        //  /Admin/ErrorPage/
        public ActionResult ErrorPage(string errorPage, string errorMessage)
        {
            AdminPage backEndPage = new AdminPage();
            backEndPage.PageName = Resources.Strings.ErrorOccurred;
            ViewBag.AdminPage = backEndPage;

            ViewBag.ErrorPage = errorPage;
            ViewBag.ErrorMessage = errorMessage;

            return View();
        }
    }
}