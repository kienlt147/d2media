﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D2Media_CMS.Filters;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferReport1(ReportList reportList)
        {
            Reports reports = new Reports();
            var arrDatetimeRanger = reportList.DatetimeRanger.IsNotEmptyOrWhiteSpace()
                ? reportList.DatetimeRanger.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            switch (reportList.Type)
            {
                case 1:
                    reportList.ReportDetails = reports.FilterReportClickByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
                case 2:
                    reportList.ReportDetails = reports.FilterReportConverisonByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
                default:
                    reportList.ReportDetails = reports.FilterReportPayoutByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
            }
            
            return View(reportList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult UserReport1(UserReportList reportList)
        {
            Reports reports = new Reports();
            var arrDatetimeRanger = reportList.DatetimeRanger.IsNotEmptyOrWhiteSpace()
                ? reportList.DatetimeRanger.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            switch (reportList.Type)
            {
                case 1:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportClickByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportClickByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
                case 2:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportConverisonByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportConverisonByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
                default:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportPayoutByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportPayoutByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
            }

            return View(reportList);
        }
    }
}