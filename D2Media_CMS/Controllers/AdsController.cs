﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using D2Media_CMS.Filters;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [OutputCache(Location = OutputCacheLocation.None)]
        public ActionResult GetAdsList(int pageSize = 5, int currentPage = 1, string searchText = "")
        {
            Ads ads = new Ads();
            var adList = ads.FilterAll(currentPage, pageSize, searchText);
            AdsList banksList = new AdsList
            {
                MgsCode = 0,
                List = adList,
                CurrentPage = currentPage,
                PageSize = pageSize,
                TotalPage = adList.IgnoreNulls().Any() 
                    ? adList.FirstOrDefault().TotalRecords / pageSize + ((adList.FirstOrDefault().TotalRecords % pageSize) > 0 ? 1 : 0) 
                    : 0,
                SearchText = searchText
            };
            return Json(banksList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult Ads()
        {
            return View();
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult AdAddEdit(int? id)
        {
            AdAddEdit adAddEdit = new AdAddEdit();
            if (id.IsNotNull())
            {
                Ads ads = new Ads();
                var ad = ads.FilterById(id);
                if (ad.IsNotNull())
                {
                    adAddEdit.Id = ad.Id;
                    adAddEdit.CategoryId = ad.CategoryId;
                    adAddEdit.AdName = ad.AdName;
                    adAddEdit.AdContent = ad.AdContent;
                    adAddEdit.Link = ad.Link;
                }
            }
            return View(adAddEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult AdAddEdit(AdAddEdit adAddEdit)
        {
            Ads ads = new Ads();
            int? currentId = adAddEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
                var rs = ads.AddEdit(
                    currentId,
                    adAddEdit.CategoryId,
                    adAddEdit.AdName,
                    adAddEdit.AdContent,
                    adAddEdit.Link
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(adAddEdit);
        }

        public ActionResult AdDelete(int? id, int pageSize = 5, int currentPage = 1)
        {
            Ads ads = new Ads();
            JsonMessages jsonMessages = new JsonMessages();
            int? rs = ads.Delete(id);
            jsonMessages.Code = rs;
            switch (rs)
            {
                case 0:
                    jsonMessages.Message = Resources.Strings.DataDeleteSuccess;
                    break;
                case 2:
                    jsonMessages.Message = Resources.Strings.DataDoesNotExistInSystem;
                    break;
                default:
                    jsonMessages.Message = Resources.Strings.DataDeleteFailed;
                    break;
            }
            return Json(jsonMessages, JsonRequestBehavior.AllowGet);
        }
    }
}