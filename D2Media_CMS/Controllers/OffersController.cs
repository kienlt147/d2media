﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using D2Media_CMS.Entities;
using D2Media_CMS.Filters;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;
using D2Media_CMS.HtmlHelpers;
using log4net;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        protected ILog Log = LogManager.GetLogger(typeof(AdminController));

        [HttpPost]
        public ActionResult GetOffersList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[Name]") ?? "";
            int queryCategoryId = request.Form.Get("query[CategoryId]").IsNotEmptyOrWhiteSpace() ? int.Parse(request.Form.Get("query[CategoryId]")) : 0;
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";
            
            Offers offers = new Offers();
            var totalCount = 0;
            var offerList = offers.GetAll(int.Parse(page), int.Parse(perpage), querySearch, queryCategoryId, out totalCount);
            total = totalCount.ToString();
            DataTable<Offer> dataTable = new DataTable<Offer>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = offerList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMyOffersList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[Name]") ?? "";
            int queryCategoryId = request.Form.Get("query[CategoryId]").IsNotEmptyOrWhiteSpace() ? int.Parse(request.Form.Get("query[CategoryId]")) : 0;
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            Offers offers = new Offers();
            var totalCount = 0;
            var offerList = offers.GetOfferByUserId(int.Parse(page), int.Parse(perpage), querySearch, queryCategoryId, BackEndSessions.CurrentUser.Id, out totalCount);
            total = totalCount.ToString();
            DataTable<Offer> dataTable = new DataTable<Offer>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = offerList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetOfferPartnerList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[Name]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            Offers offers = new Offers();
            var totalCount = 0;
            var offerList = offers.GetAllOfferPartner(int.Parse(page), int.Parse(perpage), querySearch, out totalCount);
            total = totalCount.ToString();
            DataTable<Offer> dataTable = new DataTable<Offer>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = offerList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetOfferPartnerApiList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[Name]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            Offers offers = new Offers();
            var totalCount = 0;
            var offerList = offers.GetAllOfferPartnerApi(int.Parse(page), int.Parse(perpage), querySearch, out totalCount);
            total = totalCount.ToString();
            DataTable<Offer> dataTable = new DataTable<Offer>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = offerList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPartnerRequestList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[UserName]") ?? null;
            var queryOfferName = request.Form.Get("query[OfferName]") ?? null;
            var queryStatus = request.Form.Get("query[Status]").IsNotEmptyOrWhiteSpace() ? int.Parse(request.Form.Get("query[Status]")) : -1;
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";
            
            OfferUsers offerUsers = new OfferUsers();
            var totalCount = 0;
            var partnerRequest = offerUsers.GetPartnerRequest(int.Parse(page), int.Parse(perpage), querySearch, queryOfferName, queryStatus, out totalCount);
            total = totalCount.ToString();
            DataTable<OfferUser> dataTable = new DataTable<OfferUser>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = partnerRequest
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult Offers(OffersList offer, int page = 1)
        {
//            Offers offers = new Offers();
//            var offerList = offers.GetAll(page, HelperPager.PageSize, offer.Name, offer.CategoryId);
//            HelperPagination<Offer> pager = null;
//            if (offerList.IgnoreNulls().Any())
//            {
//                pager = new HelperPagination<Offer>(offerList, page, HelperPager.PageSize, offerList.Count, "Offers/");
//            }
            return View();
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferPartner(OffersList offer, int page = 1)
        {
            return View();
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferPartnerApi(OffersList offer, int page = 1)
        {
            return View();
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult MyOffers(OffersList offer, int page = 1)
        {
            return View();
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult PartnerRequest()
        {
            return View();
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferPublisher(int? offerId)
        {
            Offers offers = new Offers();
            OfferUsers offerUsers = new OfferUsers();
            OffersList offersList = new OffersList
            {
                Offer = offers.FilterById(offerId),
                OfferUser = offerUsers.FindByUserIdOfferId(offerId, BackEndSessions.CurrentUser.Id)
            };
            return View(offersList);
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult OfferAddEdit(int? id)
        {
            OfferAddEdit offerAddEdit = new OfferAddEdit();
            if (id.IsNotNull())
            {
                Offers offers = new Offers();
                OfferCountries offerCountries = new OfferCountries();
                var offer = offers.FilterById(id);
                if (offer.IsNotNull())
                {
                    offerAddEdit.Id = offer.Id;
                    offerAddEdit.Name = offer.Name;
                    offerAddEdit.HasOfferLink = offer.HasOfferLink;
                    offerAddEdit.HasOfferRevenue = offer.HasOfferRevenue;
                    offerAddEdit.ConversionLimit = offer.ConversionLimit;
                    offerAddEdit.PreviewLink = offer.PreviewLink;
                    offerAddEdit.Description = offer.Description;
                    offerAddEdit.DeviceBrand = offer.DeviceBrand;
                    offerAddEdit.DeviceModel = offer.DeviceModel;
                    offerAddEdit.DeviceOs = offer.DeviceOs;
                    offerAddEdit.DeviceOsVersion = offer.DeviceOsVersion;
                    offerAddEdit.Images = offer.Images;
                    offerAddEdit.Revenue = offer.Revenue;
                    offerAddEdit.Payout = offer.Payout;
                    offerAddEdit.CategoryId = offer.CategoryId;
                    offerAddEdit.ResetTime = offer.ResetTime;

                    var offerCountry = offerCountries.FindByOfferId(id);
                    if (offerCountry.IgnoreNulls().Any())
                    {
                        int[] selectedItemId = new int[offerCountry.Count];
                        foreach (var oCountry in offerCountry)
                        {
                            selectedItemId[offerCountry.IndexOf(oCountry)] = oCountry.CountryId;
                        }
                        offerAddEdit.SelectedItemIds = selectedItemId;
                    }
                }
            }
            return View(offerAddEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult OfferAddEdit(OfferAddEdit offerAddEdit)
        {
            Offers offers = new Offers();
            int? currentId = offerAddEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
//                var hasOfferLink = offerAddEdit.HasOfferLink + "&aff_sub4=" + BackEndSessions.CurrentUser.Id;
                var countryIds = String.Join(",", offerAddEdit.SelectedItemIds);
                var rs = offers.AddEdit(
                    currentId,
                    offerAddEdit.Name,
                    offerAddEdit.HasOfferLink,
                    offerAddEdit.HasOfferRevenue,
                    offerAddEdit.ConversionLimit,
                    offerAddEdit.PreviewLink,
                    offerAddEdit.Description,
                    "Apple",
                    "IPhone",
                    offerAddEdit.DeviceOs,
                    offerAddEdit.DeviceOsVersion,
                    offerAddEdit.Images,
                    offerAddEdit.Revenue,
                    offerAddEdit.Payout,
                    countryIds,
                    offerAddEdit.CategoryId,
                    offerAddEdit.ResetTime
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(offerAddEdit);
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult OfferDelete(int deleteId)
        {
            Offers offers = new Offers();
            int? rs = offers.Delete(deleteId);
            switch (rs)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }
            return RedirectToAction("Offers");
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult PartnerRequestDelete(int deleteId)
        {
            OfferUsers offers = new OfferUsers();
            int? rs = offers.Delete(deleteId);
            switch (rs)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }
            return RedirectToAction("PartnerRequest");
        }

        public ActionResult DemoTest()
        {
            return View();
        }
    }
}