﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D2Media_CMS.Entities;
using D2Media_CMS.Filters;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [HttpPost]
        public ActionResult GetReportAffiliateList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[DatetimeRanger]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            var arrDatetimeRanger = querySearch.IsNotEmptyOrWhiteSpace()
                ? querySearch.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }

            Reports reports = new Reports();
            var totalCount = 0;
            var affiliatesList = reports.ReportAffiliates(BackEndSessions.CurrentUser.Id, startDate, endDate, 10);
            total = totalCount.ToString();
            DataTable<ReportGlobal> dataTable = new DataTable<ReportGlobal>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = affiliatesList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReportDailyList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[DatetimeRanger]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            var arrDatetimeRanger = querySearch.IsNotEmptyOrWhiteSpace()
                ? querySearch.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }

            Reports reports = new Reports();
            var dailyList = reports.ReportDaily(BackEndSessions.CurrentUser.Id, startDate, endDate, 10);
            DataTable<ReportGlobal> dataTable = new DataTable<ReportGlobal>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = dailyList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReporOffersList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[DatetimeRanger]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            var arrDatetimeRanger = querySearch.IsNotEmptyOrWhiteSpace()
                ? querySearch.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }

            Reports reports = new Reports();
            var dailyList = reports.ReportOffer(BackEndSessions.CurrentUser.Id, startDate, endDate, 10);
            DataTable<ReportGlobal> dataTable = new DataTable<ReportGlobal>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = dailyList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetReporHourList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[TimePicker]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";

            DateTime date;
            date = querySearch.IsNotEmptyOrWhiteSpace() ? DateTime.Parse(querySearch) : DateTime.Now;

            Reports reports = new Reports();
            var conversionList = reports.ReportHourConversion(BackEndSessions.CurrentUser.Id, date);
            var clickList = reports.ReportHourClick(BackEndSessions.CurrentUser.Id, date);
            var hourList = new List<ReportHour>();
            if (conversionList.IgnoreNulls().Any() && clickList.IgnoreNulls().Any())
            {
                hourList = conversionList.Join(
                    clickList, 
                    u => u.Hour, 
                    c => c.Hour, 
                    (u, c) => new
                    {
                        u.Hour,
                        u.DateFormat,
                        u.TotalRevenue,
                        u.TotalPayout,
                        u.TotalConversion,
                        c.TotalClick
                    }
                ).Select(x => new ReportHour
                {
                    Hour = x.Hour,
                    TotalConversion = x.TotalConversion,
                    TotalClick = x.TotalClick,
                    TotalPayout = x.TotalPayout,
                    TotalRevenue = x.TotalPayout,
                    DateFormat = x.DateFormat
                }).ToList();
            }

            DataTable<ReportHour> dataTable = new DataTable<ReportHour>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = hourList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetChartReporHourList(string timePicker)
        {
            DateTime date;
            date = timePicker.IsNotEmptyOrWhiteSpace() ? DateTime.Parse(timePicker) : DateTime.Now;

            Reports reports = new Reports();
            var conversionList = reports.ReportHourConversion(BackEndSessions.CurrentUser.Id, date);
            var clickList = reports.ReportHourClick(BackEndSessions.CurrentUser.Id, date);
            var hourList = new List<ReportHour>();
            if (conversionList.IgnoreNulls().Any() && clickList.IgnoreNulls().Any())
            {
                hourList = conversionList.Join(
                    clickList,
                    u => u.Hour,
                    c => c.Hour,
                    (u, c) => new
                    {
                        u.Hour,
                        u.DateFormat,
                        u.TotalRevenue,
                        u.TotalPayout,
                        u.TotalConversion,
                        c.TotalClick
                    }
                ).Select(x => new ReportHour
                {
                    Hour = x.Hour,
                    TotalConversion = x.TotalConversion,
                    TotalClick = x.TotalClick,
                    TotalPayout = x.TotalPayout,
                    TotalRevenue = x.TotalPayout,
                    DateFormat = x.DateFormat
                }).ToList();
            }

            DataTable<ReportHour> dataTable = new DataTable<ReportHour>
            {
                data = hourList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferReport(ReportList reportList)
        {
            Reports reports = new Reports();
            var arrDatetimeRanger = reportList.DatetimeRanger.IsNotEmptyOrWhiteSpace()
                ? reportList.DatetimeRanger.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            switch (reportList.Type)
            {
                case 1:
                    reportList.ReportDetails = reports.FilterReportClickByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
                case 2:
                    reportList.ReportDetails = reports.FilterReportConverisonByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
                default:
                    reportList.ReportDetails = reports.FilterReportPayoutByTopOffer(
                        BackEndSessions.CurrentUser.Id, 
                        reportList.TopOffer, 
                        startDate, 
                        endDate);
                    break;
            }
            
            return View(reportList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult UserReport(UserReportList reportList)
        {
            Reports reports = new Reports();
            var arrDatetimeRanger = reportList.DatetimeRanger.IsNotEmptyOrWhiteSpace()
                ? reportList.DatetimeRanger.Split("-")
                : null;
            DateTime startDate;
            DateTime endDate;
            if (arrDatetimeRanger.IsNotNull())
            {
                startDate = DateTime.Parse(arrDatetimeRanger[0].Trim());
                endDate = DateTime.Parse(arrDatetimeRanger[1].Trim());
            }
            else
            {
                startDate = DateTime.Now;
                endDate = DateTime.Now;
            }
            switch (reportList.Type)
            {
                case 1:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportClickByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportClickByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
                case 2:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportConverisonByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportConverisonByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
                default:
                    if (reportList.UserName.IsNull())
                    {
                        reportList.ReportDetails = reports.FilterAllUserReportPayoutByTopOffer(
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    else
                    {
                        reportList.ReportDetails = reports.FilterReportPayoutByTopOffer(
                            reportList.UserName,
                            reportList.TopOffer,
                            startDate,
                            endDate);
                    }
                    break;
            }

            return View(reportList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult AffiliateReports(ReportGlobalList reportGlobalList)
        {
            return View(reportGlobalList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult DailyReports(ReportGlobalList reportGlobalList)
        {
            return View(reportGlobalList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult OfferReports(ReportGlobalList reportGlobalList)
        {
            return View(reportGlobalList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult ConversionReports(ReportGlobalList reportGlobalList)
        {
            return View(reportGlobalList);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult HourReports(ReportHourList reportList)
        {
            DateTime date;
            date = reportList.TimePicker.IsNotEmptyOrWhiteSpace() ? DateTime.Parse(reportList.TimePicker) : DateTime.Now;

            Reports reports = new Reports();
            var conversionList = reports.ReportHourConversion(BackEndSessions.CurrentUser.Id, date);
            var clickList = reports.ReportHourClick(BackEndSessions.CurrentUser.Id, date);
            var hourList = new List<ReportHour>();
            if (conversionList.IgnoreNulls().Any() && clickList.IgnoreNulls().Any())
            {
                hourList = conversionList.Join(
                    clickList,
                    u => u.Hour,
                    c => c.Hour,
                    (u, c) => new
                    {
                        u.Hour,
                        u.DateFormat,
                        u.TotalRevenue,
                        u.TotalPayout,
                        u.TotalConversion,
                        c.TotalClick
                    }
                ).Select(x => new ReportHour
                {
                    Hour = x.Hour,
                    TotalConversion = x.TotalConversion,
                    TotalClick = x.TotalClick,
                    TotalPayout = x.TotalPayout,
                    TotalRevenue = x.TotalPayout,
                    DateFormat = x.DateFormat
                }).ToList();
            }
            reportList.ReportHours = hourList;
            return View(reportList);
        }
    }
}