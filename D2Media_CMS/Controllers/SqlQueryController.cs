﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using D2Media_CMS.Filters;
using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [HttpGet]
        [IsRestricted]
        public ActionResult SqlQuery()
        {
            return View();
        }

        [HttpPost]
        [IsRestricted]
        public ActionResult SqlQuery(SqlQuery sqlQuery)
        {
            try
            {
                using (AdoHelper db = new AdoHelper())
                {
                    db.ExecDataReader(sqlQuery.Query);
                    ModelState.AddResult(ViewData, ModelStateResult.Success, "Thực hiện truy vấn thành công.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddResult(ViewData, ModelStateResult.Success, ex.ToString());
            }

            return View(sqlQuery);
        }
    }
}