﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D2Media_CMS.Entities;
using D2Media_CMS.Filters;
using D2Media_CMS.HtmlHelpers;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [HttpPost]
        public ActionResult GetPostbackList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[PostbackName]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";
            PostBacks offers = new PostBacks();
            var postBackList = offers.GetAll(int.Parse(page), int.Parse(perpage), querySearch);
            DataTable<Postback> dataTable = new DataTable<Postback>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = postBackList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult Postback(PostBacksList postbacksList, int page = 1)
        {
            PostBacks offers = new PostBacks();
            var countryList = offers.GetAll(page, HelperPager.PageSize, postbacksList.Name);
            HelperPagination<Postback> pager = null;
            if (countryList.IgnoreNulls().Any())
            {
                pager = new HelperPagination<Postback>(countryList, page, HelperPager.PageSize, countryList.Count, "PostBack/");
            }
            return View(pager);
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult PostBackAddEdit(int? id)
        {
            PostBackAddEdit postBackAddEdit = new PostBackAddEdit();
            if (id.IsNotNull())
            {
                PostBacks postBacks = new PostBacks();
                OfferCountries offerCountries = new OfferCountries();
                var postback = postBacks.FilterById(id);
                if (postback.IsNotNull())
                {
                    postBackAddEdit.Id = postback.Id;
                    postBackAddEdit.UserId = postback.UserId;
                    postBackAddEdit.Url = postback.Url;
                    //postBackAddEdit.Status = postback.Status;
                }
            }
            return View(postBackAddEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult PostBackAddEdit(PostBackAddEdit postBackAddEdit)
        {
            PostBacks postBacks = new PostBacks();
            int? currentId = postBackAddEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
                var rs = postBacks.AddEdit(
                    currentId,
                    1,
                    postBackAddEdit.Url
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(postBackAddEdit);
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult PostBackDelete(int deleteId)
        {
            PostBacks postBacks = new PostBacks();
            int? rs = postBacks.Delete(deleteId);
            switch (rs)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }
            return RedirectToAction("Postback");
        }
    }
}