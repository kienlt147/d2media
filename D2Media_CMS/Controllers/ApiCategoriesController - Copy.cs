﻿using D2Media_CMS.Entities;
using D2Media_CMS.Filters;
using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        [HttpPost]
        public ActionResult GetApiCategoriesList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[CategoryName]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";
            ApiCategories apiCategories = new ApiCategories();
            var categoryList = apiCategories.FilterAll(int.Parse(page), int.Parse(perpage), querySearch);
            DataTable<ApiCategory> dataTable = new DataTable<ApiCategory>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = categoryList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult ApiCategories(CategoriesList categoriesList, int page = 1)
        {
            return View();
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult ApiCategoryAddEdit(int? id)
        {
            ApiCategoryAddEdit categoryAddEdit = new ApiCategoryAddEdit();
            if (id.IsNotNull())
            {
                ApiCategories categories = new ApiCategories();
                var category = categories.FilterById(id);
                if (category.IsNotNull())
                {
                    categoryAddEdit.Id = category.Id;
                    categoryAddEdit.Title = category.Title;
                    categoryAddEdit.CategoryId = category.CategoryId;
                    categoryAddEdit.Domain = category.Domain;
                    categoryAddEdit.SubQuery = category.SubQuery;
                    categoryAddEdit.UserName = category.UserName;
                    categoryAddEdit.ApiKey = category.ApiKey;
                }
            }
            return View(categoryAddEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult ApiCategoryAddEdit(ApiCategoryAddEdit categoryAddEdit)
        {
            ApiCategories categories = new ApiCategories();
            int? currentId = categoryAddEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
                var rs = categories.AddEdit(
                    currentId,
                    categoryAddEdit.Title,
                    categoryAddEdit.CategoryId,
                    categoryAddEdit.Domain,
                    categoryAddEdit.SubQuery,
                    categoryAddEdit.UserName,
                    categoryAddEdit.ApiKey
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(categoryAddEdit);
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult ApiCategoryDelete(int deleteId)
        {
            Categories categories = new Categories();
            int? rs = categories.Delete(deleteId);
            switch (rs)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }
            return RedirectToAction("Categories");
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult ApiCategoryResquest(int? id)
        {
            OfferApiMessageList offerApiList = new OfferApiMessageList();
            Offer offer = new Offer();
            if (id.IsNotNull())
            {                
                ApiCategories categories = new ApiCategories();
                var category = categories.FilterById(id);
                var link = category.Domain + category.SubQuery;
                var base64Decode = category.UserName + ":" + category.ApiKey;
                var token = Utility.Base64Encode(base64Decode);
                ApiAdvHelper.OfferLook(link, token, category, offerApiList);
                //var offerLooks = AdvApiHelper.GetOffers(link, token);
                //if(offerLooks == null)
                //{
                //    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage {
                //        Id = "0",
                //        Name = "Api get null",
                //        Message = "Api get null"
                //    });
                //}
                //var offerDataRowsets = offerLooks.data.rowset;
                //foreach(var offerDataRowSet in offerDataRowsets)
                //{
                //    var index = offerDataRowsets.IndexOf(offerDataRowSet);
                //    List<int> countryList = new List<int>();
                //    foreach(var countries in offerDataRowSet.offer_geo.target)
                //    {
                //        var country = Countries.FindByCountryCode(countries.country_code);
                //        countryList.Add(country.Id);
                //    }
                //    var countryIds = String.Join(",", countryList);
                //    try
                //    {                        
                //        offer.CategoryOfferId = offerDataRowSet.offer.id;
                //        offer.CategoryId = category.CategoryId;
                //        offer.Name = offerDataRowSet.offer.name;
                //        offer.HasOfferLink = offerDataRowSet.offer.tracking_link;
                //        offer.PreviewLink = offerDataRowSet.offer.preview_url;
                //        offer.ConversionLimit = offerDataRowSet.offer_cap.IsNull() ? 0 : offerDataRowSet.offer_cap.cap_conversion;
                //        offer.Description = offerDataRowSet.offer.name;
                //        offer.DeviceBrand = offerDataRowSet.offer_platform.target[0].system.ToLower();
                //        offer.DeviceModel = offerDataRowSet.offer_platform.target[0].system.ToLower();
                //        offer.DeviceOs = offerDataRowSet.offer_platform.target[0].system.ToLower();
                //        offer.DeviceOsVersion = offerDataRowSet.offer_platform.target[0].version[0];
                //        offer.Images = offerDataRowSet.offer.thumbnail;
                //        offer.Status = 1;
                //        offer.HasOfferRevenue = 0;
                //        offer.Revenue = Double.Parse(offerDataRowSet.offer.payout);
                //        offer.Payout = (Double.Parse(offerDataRowSet.offer.payout) * 50 / 100);
                //        Offers offers = new Offers();
                //        var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                //            offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                //            offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                //        switch (rs)
                //        {
                //            case 0:
                //                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage {
                //                    Id = offer.CategoryOfferId.ToString(),
                //                    Name = offer.Name,
                //                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                //                });                                
                //                break;
                //            case 2:
                //                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                //                {
                //                    Id = offer.CategoryOfferId.ToString(),
                //                    Name = offer.Name,
                //                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                //                });
                //                break;
                //            default:
                //                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                //                {
                //                    Id = offer.CategoryOfferId.ToString(),
                //                    Name = offer.Name,
                //                    Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                //                });
                //                break;
                //        }
                //    }
                //    catch (Exception e)
                //    {
                //        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                //        {
                //            Id = offer.CategoryOfferId.ToString(),
                //            Name = offer.Name,
                //            Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Exception: {e.Message}"
                //        });
                //    }
                //}
            }
            return View(offerApiList);
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult ApiCategoryOrangearResquest(int? id)
        {
            OfferApiMessageList offerApiList = new OfferApiMessageList();
            Offer offer = new Offer();
            try
            {
                if (id.IsNotNull())
                {
                    ApiCategories categories = new ApiCategories();
                    var category = categories.FilterById(id);
                    var link = category.Domain + category.SubQuery;
                    var offerLooks = AdvApiHelper.GetOrangearOffers(link);
                    if (offerLooks == null)
                    {
                        offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                        {
                            Id = "0",
                            Name = "Api get null",
                            Message = "Api get null"
                        });
                    }
                    if (offerLooks.success)
                    {
                        var offerDataRowsets = offerLooks.offers;
                        var total = offerDataRowsets.Count;
                        foreach (string key in offerDataRowsets.Keys)
                        {
                            var offerData = offerDataRowsets[key];

                            List<int> countryList = new List<int>();
                            var country = Countries.FindByCountryCode(offerData["Countries"].ToUpper());
                            countryList.Add(country.Id);
                            var countryIds = String.Join(",", countryList);

                            offer.CategoryOfferId = int.Parse(offerData["ID"]);
                            offer.CategoryId = category.CategoryId;
                            offer.Name = offerData["Name"];
                            offer.HasOfferLink = offerData["Tracking_url"];
                            offer.PreviewLink = offerData["Preview_url"];
                            offer.ConversionLimit = 0;
                            offer.Description = offerData["Description"];
                            offer.DeviceBrand = offerData["Platforms"].ToLower();
                            offer.DeviceModel = offerData["Platforms"].ToLower();
                            offer.DeviceOs = offerData["Platforms"].ToLower();
                            offer.DeviceOsVersion = "1";
                            offer.Images = offerData["Icon_url"];
                            offer.Status = 1;
                            offer.HasOfferRevenue = 0;
                            offer.Revenue = Double.Parse(offerData["Payout"]);
                            offer.Payout = (Double.Parse(offerData["Payout"]) * 50 / 100);
                            Offers offers = new Offers();
                            var rs = offers.AddEditGetApi(offer.CategoryOfferId, offer.Name, offer.HasOfferLink, offer.HasOfferRevenue, offer.ConversionLimit, offer.PreviewLink,
                                offer.Description, offer.DeviceBrand, offer.DeviceModel, offer.DeviceOs, offer.DeviceOsVersion, offer.Images, offer.Revenue,
                                offer.Payout, countryIds, offer.CategoryId, offer.ResetTime);
                            switch (rs)
                            {
                                case 0:
                                    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                    {
                                        Id = offer.CategoryOfferId.ToString(),
                                        Name = offer.Name,
                                        Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert successfully."
                                    });
                                    break;
                                case 2:
                                    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                    {
                                        Id = offer.CategoryOfferId.ToString(),
                                        Name = offer.Name,
                                        Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer already exists."
                                    });
                                    break;
                                default:
                                    offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                                    {
                                        Id = offer.CategoryOfferId.ToString(),
                                        Name = offer.Name,
                                        Message = $"Name: {offer.Name} - HasOfferLink: {offer.HasOfferLink} - Conversion: {offer.ConversionLimit} - Offer insert database error."
                                    });
                                    break;
                            }
                        }
                    }
                }
            }
            catch(Exception e)
            {
                offerApiList.OfferApiMessagesList.Add(new OfferApiMessage
                {
                    Id = offer.CategoryOfferId.ToString(),
                    Name = offer.Name,
                    Message = $"Exception: {e.Message}"
                });
            }            
            return View(offerApiList);
        }
    }
}