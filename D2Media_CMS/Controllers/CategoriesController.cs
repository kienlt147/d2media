﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using D2Media_CMS.Entities;
using D2Media_CMS.Filters;
using D2Media_CMS.Helpers;
using D2Media_CMS.HtmlHelpers;
using D2Media_CMS.Models;
using D2Media_CMS.ViewModels;
using Newtonsoft.Json;

namespace D2Media_CMS.Controllers
{
    public class Demo
    {
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public string CreatedDate { get; set; }
    }
    public partial class AdminController : AdminBaseController
    {
        [HttpPost]
        public ActionResult GetCategoriesList()
        {
            var request = System.Web.HttpContext.Current.Request;
            var page = request.Form.Get("pagination[page]") ?? "1";
            var pages = request.Form.Get("pagination[pages]") ?? "0";
            var perpage = request.Form.Get("pagination[perpage]") ?? "10";
            var total = request.Form.Get("pagination[total]") ?? "0";
            var querySearch = request.Form.Get("query[CategoryName]") ?? "";
            var sortField = request.Form.Get("sort[field]") ?? "";
            var sortType = request.Form.Get("sort[sort]") ?? "";
            Categories categories = new Categories();
            var categoryList = categories.FilterAll(int.Parse(page), int.Parse(perpage), querySearch);
            DataTable<Category> dataTable = new DataTable<Category>
            {
                meta = new Meta
                {
                    page = page,
                    perpage = perpage,
                    total = total,
                    field = sortField,
                    pages = pages,
                    sort = sortType
                },
                data = categoryList
            };
            return Json(dataTable, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult Categories(CategoriesList categoriesList, int page = 1)
        {
//            Demo demo = new Demo
//            {
//                UserId = 1,
//                OfferId = 1,
//                CreatedDate = DateTime.Now.ToString("ddMMyyyyHHmmssffffff")
//            };
//            string json = JsonConvert.SerializeObject(demo);
//            var endCode = SecurityHelper.EncodeBase64(json);
//            var decode = SecurityHelper.DecodeBase64(endCode);
//            var obj = JsonConvert.DeserializeObject<Demo>(decode);

            /*
             */
//            Categories categories = new Categories();
//            var categoryList = categories.FilterAll(page, HelperPager.PageSize, categoriesList.CategoryName);
//            HelperPagination<Category> pager = null;
//            if (categoryList.IgnoreNulls().Any())
//            {
//                pager = new HelperPagination<Category>(categoryList, page, HelperPager.PageSize, categoryList.Count, "Categories/");
//            }
            return View();
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult CategoryAddEdit(int? id)
        {
            CategoryAddEdit categoryAddEdit = new CategoryAddEdit();
            if (id.IsNotNull())
            {
                Categories categories = new Categories();
                var category = categories.FilterById(id);
                if (category.IsNotNull())
                {
                    categoryAddEdit.Id = category.Id;
                    categoryAddEdit.CategoryName = category.CategoryName;
                    categoryAddEdit.Description = category.Description;
                    categoryAddEdit.TransId = category.TransId;
                    categoryAddEdit.Sub1 = category.Sub1;
                    categoryAddEdit.Sub2 = category.Sub2;
                    categoryAddEdit.Sub3 = category.Sub3;
                    categoryAddEdit.Sub4 = category.Sub4;
                    categoryAddEdit.Sub5 = category.Sub5;
                    categoryAddEdit.CountryCode = category.CountryCode;
                    categoryAddEdit.AddressIp = category.AddressIp;
                    categoryAddEdit.DeviceOs = category.DeviceOs;
                    categoryAddEdit.DeviceOsVersion = category.DeviceOsVersion;
                    categoryAddEdit.Status = category.Status;
                }
            }
            return View(categoryAddEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult CategoryAddEdit(CategoryAddEdit categoryAddEdit)
        {
            Categories categories = new Categories();
            int? currentId = categoryAddEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
                var rs = categories.AddEdit(
                    currentId,
                    categoryAddEdit.CategoryName,
                    categoryAddEdit.Description,
                    categoryAddEdit.TransId,
                    categoryAddEdit.Sub1,
                    categoryAddEdit.Sub2,
                    categoryAddEdit.Sub3,
                    categoryAddEdit.Sub4,
                    categoryAddEdit.Sub5,
                    categoryAddEdit.CountryCode,
                    categoryAddEdit.AddressIp,
                    categoryAddEdit.DeviceOs,
                    categoryAddEdit.DeviceOsVersion,
                    categoryAddEdit.Status
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(categoryAddEdit);
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult CategoryDelete(int deleteId)
        {
            Categories categories = new Categories();
            int? rs = categories.Delete(deleteId);
            switch (rs)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }
            return RedirectToAction("Categories");
        }
    }
}