﻿using D2Media_CMS.Models;
using System.Web.Mvc;
using D2Media_CMS.ViewModels;
using System.Collections.Generic;
using System.Collections;
using D2Media_CMS.Filters;
using System;

namespace D2Media_CMS.Controllers
{
    public partial class AdminController : AdminBaseController
    {
        //  /Admin/Users/
        [HttpGet]
        [IsRestricted]
        [PersistQuerystring]
        [ImportModelStateFromTempData]
        public ActionResult Users(BackEndUsersList backEndUsersList)
        {
            Users users = new Users();
            backEndUsersList.UserList = users.GetAllUsers(backEndUsersList.Username, backEndUsersList.FullName, backEndUsersList.Email, backEndUsersList.GroupId);
            if (backEndUsersList.UserList.IsNull() || backEndUsersList.UserList.Count == 0)
            {
                ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.NoDataFound);
            }

            return View(backEndUsersList);
        }

        //  /Admin/UsersAdd/
        [HttpGet]
        [IsRestricted]
        public ActionResult UsersAdd()
        {
            return View();
        }
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult UsersAdd(BackEndUsersAdd backEndUsersAdd)
        {
            if (ModelState.IsValidOrRefresh())
            {
                Users users = new Users();
                int? result = users.Add(backEndUsersAdd.Username, backEndUsersAdd.Password, 
                    backEndUsersAdd.FullName, backEndUsersAdd.Email, backEndUsersAdd.GroupId, 
                    backEndUsersAdd.Description, backEndUsersAdd.NumberPhone, 
                    backEndUsersAdd.Company, backEndUsersAdd.Skype, null);
                switch (result)
                {
                    case 0:
                        ModelState.Clear();
                        backEndUsersAdd = new BackEndUsersAdd();

                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAdded);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UsernameAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }

            return View(backEndUsersAdd);
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult UsersInfo()
        {
            UserInfo userInfo = new UserInfo();

            Users users = new Users();
            User user = users.GetUserById(BackEndSessions.CurrentUser.Id);
            if (user.IsNotNull())
            {
                userInfo.Username = BackEndSessions.CurrentUser.UserName;
                userInfo.FullName = user.FullName;
                userInfo.Email = user.Email;
                userInfo.NumberPhone = user.NumberPhone;
                userInfo.Company = user.Company;
                userInfo.Skype = user.Skype;
                userInfo.Tax = user.Tax;
                userInfo.Description = user.Description;
            }
            else
            {
                ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                ViewData.IsFormVisible(false);
            }

            return View(userInfo);
        }

        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult UsersInfo(UserInfo userInfo)
        {
            Users users = new Users();
            int? result = users.EditInfo(userInfo.Username, userInfo.FullName,
                userInfo.Email, userInfo.Description,
                userInfo.NumberPhone, userInfo.Company, userInfo.Skype, userInfo.Tax);
            switch (result)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyEdited);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    ViewData.IsFormVisible(false);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }

            return View(userInfo);
        }

        //  /Admin/UsersEdit/
        [HttpGet]
        [IsRestricted]
        public ActionResult UsersEdit(string id)
        {
            BackEndUsersEdit backEndUsersEdit = new BackEndUsersEdit();

            Users users = new Users();
            User user = users.GetUserByUserName(id);
            if (user.IsNotNull())
            {
                backEndUsersEdit.UserId = user.Id;
                backEndUsersEdit.Username = user.UserName;
                backEndUsersEdit.FullName = user.FullName;
                backEndUsersEdit.Email = user.Email;
                backEndUsersEdit.GroupId = user.GroupId;
                backEndUsersEdit.Description = user.Description;
                backEndUsersEdit.Company = user.Company;
                backEndUsersEdit.Skype = user.Skype;
                backEndUsersEdit.Tax = user.Tax;
                backEndUsersEdit.NumberPhone = user.NumberPhone;
                backEndUsersEdit.PostBackLink = user.PostBackLink;
                backEndUsersEdit.Status = user.Status;
            }
            else
            {
                ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                ViewData.IsFormVisible(false);
            }

            return View(backEndUsersEdit);
        }
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult UsersEdit(BackEndUsersEdit backEndUsersEdit, string id)
        {
            Users users = new Users();
            var advIds = String.Join(",", backEndUsersEdit.SelectedItemIds);
            int? result = users.Edit(backEndUsersEdit.UserId, id, backEndUsersEdit.Password, backEndUsersEdit.FullName, 
                backEndUsersEdit.Email, backEndUsersEdit.GroupId, backEndUsersEdit.Description, 
                backEndUsersEdit.NumberPhone, backEndUsersEdit.Company, backEndUsersEdit.Skype, 
                backEndUsersEdit.Tax, backEndUsersEdit.PostBackLink, backEndUsersEdit.Status, advIds);
            switch (result)
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyEdited);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    ViewData.IsFormVisible(false);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, "UserId already exists.");
                    ViewData.IsFormVisible(false);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }

            return View(backEndUsersEdit);
        }

        [HttpGet]
        [IsRestricted]
        public ActionResult UserBankAddEdit(int? id)
        {
            UserBankEdit userBankEdit = new UserBankEdit();
            if (id.IsNotNull())
            {
                UserBanks userBanks = new UserBanks();
                UserBank userBank = userBanks.FilterByUserId(id);
                if (userBank.IsNotNull())
                {
                    userBankEdit.BeneficiaryName = userBank.BeneficiaryName;
                    userBankEdit.BankName = userBank.BankName;
                    userBankEdit.AccountNumber = userBank.AccountNumber;
                    userBankEdit.BankAddress = userBank.BankAddress;
                    userBankEdit.SwiftNumber = userBank.SwiftNumber;
                    userBankEdit.Paypal = userBank.Paypal;
                }
            }

            return View(userBankEdit);
        }

        [HttpPost, ValidateInput(false)]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        public ActionResult UserBankAddEdit(UserBankEdit userBankEdit)
        {
            UserBanks userBanks = new UserBanks();
            int? currentId = userBankEdit.Id;
            if (ModelState.IsValidOrRefresh())
            {
                var rs = userBanks.AddEdit(
                    currentId,
                    userBankEdit.BeneficiaryName,
                    userBankEdit.BankName,
                    userBankEdit.AccountNumber,
                    userBankEdit.BankAddress,
                    userBankEdit.SwiftNumber,
                    userBankEdit.Paypal
                );
                switch (rs)
                {
                    case 0:
                        ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyAddEdit);
                        break;
                    case 2:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemAlreadyExists);
                        break;
                    default:
                        ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                        break;
                }
            }
            return View(userBankEdit);
        }

        //  /Admin/UsersDelete/
        [HttpPost]
        [IsRestricted]
        [ValidateAntiForgeryToken]
        [ExportModelStateToTempData]
        public ActionResult UsersDelete(string deleteId)
        {
            Users users = new Users();
            switch (users.Delete(deleteId))
            {
                case 0:
                    ModelState.AddResult(ViewData, ModelStateResult.Success, Resources.Strings.ItemSuccessfullyDeleted);
                    break;
                case 2:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemDoesNotExist);
                    break;
                case 3:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.ItemUsedSomewhereElse);
                    break;
                default:
                    ModelState.AddResult(ViewData, ModelStateResult.Error, Resources.Strings.UnexpectedError);
                    break;
            }

            return RedirectToAction("Users");
        }
    }
}
