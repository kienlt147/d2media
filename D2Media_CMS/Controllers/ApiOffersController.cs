﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using D2Media_CMS.Entities;
using D2Media_CMS.Models;

namespace D2Media_CMS.Controllers
{
    [RoutePrefix("api/offers")]
    public class ApiOffersController : ApiController
    {
        [HttpGet]
        [Route("changeStatus")]
        public IHttpActionResult ChangeStatus(int offerId, int offerStatus)
        {
            OffersProperty offersProperty = new OffersProperty();
            Offers offers = new Offers();
            try
            {
                var status = offerStatus == 1 ? 0 : 1;
                var rs = offers.UpdateStatus(offerId, status);
                switch (rs)
                {
                    case 0:
                        offersProperty.ErrCode = 200;
                        offersProperty.ErrMessage = "The item has been successfully changed.";
                        break;
                    default:
                        offersProperty.ErrCode = 401;
                        offersProperty.ErrMessage = "An unexpected error occurred. Please try again later.";
                        break;
                }
            }
            catch (Exception e)
            {
                offersProperty.ErrCode = 405;
                offersProperty.ErrMessage = e.ToString();
            }
            return Ok(offersProperty);
        }

        [HttpGet]
        [Route("requestApproval")]
        public IHttpActionResult RequestApproval(int offerId)
        {
             var xx = (HttpRequestMessage)HttpContext.Current.Items["MS_HttpRequestMessage"];
            //System.Net.Http.Headers.HttpRequestHeaders headers = this.Request.Headers;
//            IEnumerable<string> values = new List<string>();
//            this.Request.Headers.TryGetValues("Authorization", out values);
//            var handler = new JwtSecurityTokenHandler();
//            var token = handler.ReadJwtToken(values.First());
            return Ok();
        }
    }
}