﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.Entities
{
    public class Meta
    {
        public string page { get; set; }
        public string pages { get; set; }
        public string perpage { get; set; }
        public string total { get; set; }
        public string sort { get; set; }
        public string field { get; set; }
    }
    
    public class DataTable<T>
    {
        public Meta meta { get; set; }
        public List<T> data { get; set; }
    }
}