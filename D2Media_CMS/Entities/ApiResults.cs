﻿namespace D2Media_CMS.Entities
{
    public class ApiResults
    {
        public string ErrMessage { get; set; }
        public int ErrCode { get; set; }
    }
}