﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;

namespace D2Media_CMS.Entities
{
    public class OffersProperty : ApiResults
    {
        public List<Offer> Offers { get; set; }
    }
}