﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class OfferAddEdit
    {
        public int Id { get; set; }

        [DataAnnotationsRequired]
        [DataAnnotationsDisplay("CategoryName")]
        public int CategoryId { get; set; }

        [DataAnnotationsRequired]
        public int CountryId { get; set; }

        [DataAnnotationsDisplay("OfferName")]
        [DataAnnotationsRequired]
        public string Name { get; set; }

        [DataAnnotationsDisplay("HasOfferLink")]
        [DataAnnotationsRequired]
        public string HasOfferLink { get; set; }

        [DataAnnotationsDisplay("HasOfferRevenue")]
        [DataAnnotationsRequired]
        public double HasOfferRevenue { get; set; }

        [DataAnnotationsDisplay("ConversionLimit")]
        public int ConversionLimit { get; set; }

        [DataAnnotationsDisplay("PreviewLink")]
        public string PreviewLink { get; set; }

        [DataAnnotationsDisplay("Description")]
        public string Description { get; set; }

        [DataAnnotationsDisplay("DeviceBrand")]
        public string DeviceBrand { get; set; }

        [DataAnnotationsDisplay("DeviceModel")]
        public string DeviceModel { get; set; }

        [DataAnnotationsDisplay("DeviceOs")]
        public string DeviceOs { get; set; }

        [DataAnnotationsDisplay("DeviceOsVersion")]
        [DataAnnotationsRequired]
        public string DeviceOsVersion { get; set; }

        [DataAnnotationsDisplay("Images")]
        public string Images { get; set; }

        [DataAnnotationsDisplay("Revenue")]
        public double Revenue { get; set; }

        [DataAnnotationsDisplay("Payout")]
        public double Payout { get; set; }

        [DataAnnotationsDisplay("CountryName")]
        [DataAnnotationsRequired]
        public int[] SelectedItemIds { get; set; }

        [DataAnnotationsDisplay("ResetTime")]
        public string ResetTime { get; set; }
    }
}