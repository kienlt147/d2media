﻿using D2Media_CMS.Helpers;
using System.ComponentModel.DataAnnotations;

namespace D2Media_CMS.ViewModels
{
    public class BackEndLanguagesEdit
    {
        [DataAnnotationsDisplay("LanguageCode")]
        [DataAnnotationsOnlyLetters]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(2)]
        public string LanguageCode { get; set; }

        [DataAnnotationsDisplay("LanguageName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string LanguageName { get; set; }

        [DataAnnotationsDisplay("LanguageNameOriginal")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string LanguageNameOriginal { get; set; }

        [DataAnnotationsDisplay("Active")]
        public bool IsActive { get; set; }
    }
}
