﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;

namespace D2Media_CMS.ViewModels
{
    public class AdsList
    {
        public int? MgsCode { get; set; }
        public List<Ad> List { get; set; }
        public int TotalPage { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string SearchText { get; set; }
    }
}