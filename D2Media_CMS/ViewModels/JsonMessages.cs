﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class JsonMessages
    {
        public int? Code { get; set; }
        public string Message { get; set; }
    }
}