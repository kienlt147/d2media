﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class ApiCategoryAddEdit
    {
        public int Id { get; set; }

        [DataAnnotationsDisplay("Title")]
        [DataAnnotationsRequired]
        public string Title { get; set; }

        [DataAnnotationsDisplay("CategoryName")]
        [DataAnnotationsRequired]
        public int CategoryId { get; set; }

        [DataAnnotationsDisplay("Domain")]
        public string Domain { get; set; }

        [DataAnnotationsDisplay("SubQuery")]
        public string SubQuery { get; set; }

        [DataAnnotationsDisplay("UserName")]
        public string UserName { get; set; }

        [DataAnnotationsDisplay("ApiKey")]
        public string ApiKey { get; set; }
    }
}