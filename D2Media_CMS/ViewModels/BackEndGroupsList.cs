﻿using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace D2Media_CMS.ViewModels
{
    public class BackEndGroupsList
    {
        [DataAnnotationsDisplay("GroupName")]
        [DataAnnotationsStringLengthMax(255)]
        public string GroupName { get; set; }

        public List<Group> GroupList { get; set; }
    }
}
