﻿using D2Media_CMS.Models;
using System.Web.Mvc;
using D2Media_CMS.ViewModels;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using D2Media_CMS.Helpers;
using System.Text.RegularExpressions;

namespace D2Media_CMS.ViewModels
{
    public class BackEndPageTemplatesAdd
    {
        [DataAnnotationsDisplay("TemplateName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string Title { get; set; }

        [AllowHtml]
        //[CustomValidation(typeof(NoDuplicateModules), "Validate")]
        [DataAnnotationsDisplay("HtmlCode")]
        [DataAnnotationsRequired]
        public string HtmlCode { get; set; }

        [DataAnnotationsDisplay("Active")]
        public bool IsActive { get; set; }
    }
}
