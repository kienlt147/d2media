﻿using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace D2Media_CMS.ViewModels
{
    public class BackEndGroupsAdd
    {
        [DataAnnotationsDisplay("GroupName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string GroupName { get; set; }
    }
}
