﻿using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace D2Media_CMS.ViewModels
{
    public class BackEndPageTemplatesEdit
    {
        [DataAnnotationsDisplay("Title")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string Title { get; set; }

        [AllowHtml]
        [DataAnnotationsDisplay("HtmlCode")]
        [DataAnnotationsRequired]
        public string HtmlCode { get; set; }

        [DataAnnotationsDisplay("Active")]
        public bool IsActive { get; set; }
    }
}
