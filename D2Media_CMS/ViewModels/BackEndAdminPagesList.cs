﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class BackEndAdminPagesList
    {
        [DataAnnotationsDisplay("PageName")]
        [DataAnnotationsStringLengthMax(255)]
        public string PageName { get; set; }

        public HtmlString TreeTablePageList { get; set; }
    }
}