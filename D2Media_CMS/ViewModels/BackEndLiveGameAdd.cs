﻿using D2Media_CMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class BackEndLiveGameAdd
    {
        [DataAnnotationsDisplay("LiveGameName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Days { get; set; }
        public string GameUrl { get; set; }
        public string BannerUrl { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
    }

    public class BackEndLiveGameEdit
    {
        [DataAnnotationsDisplay("LiveGameName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string Name { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Days { get; set; }
        public string GameUrl { get; set; }
        public string BannerUrl { get; set; }
        public bool Status { get; set; }
        public string Description { get; set; }
    }
}