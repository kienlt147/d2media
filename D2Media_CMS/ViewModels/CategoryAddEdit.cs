﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class CategoryAddEdit
    {
        public int Id { get; set; }

        [DataAnnotationsDisplay("CategoryName")]
        [DataAnnotationsRequired]
        public string CategoryName { get; set; }

        [DataAnnotationsDisplay("Description")]
        public string Description { get; set; }

        [DataAnnotationsDisplay("TransId")]
        public string TransId { get; set; }

        [DataAnnotationsDisplay("Sub1")]
        public string Sub1 { get; set; }

        [DataAnnotationsDisplay("Sub2")]
        public string Sub2 { get; set; }

        [DataAnnotationsDisplay("Sub3")]
        public string Sub3 { get; set; }

        [DataAnnotationsDisplay("Sub4")]
        public string Sub4 { get; set; }

        [DataAnnotationsDisplay("Sub5")]
        public string Sub5 { get; set; }

        [DataAnnotationsDisplay("CountryCode")]
        public string CountryCode { get; set; }

        [DataAnnotationsDisplay("AddressIp")]
        public string AddressIp { get; set; }

        [DataAnnotationsDisplay("DeviceOs")]
        public string DeviceOs { get; set; }

        [DataAnnotationsDisplay("DeviceOsVersion")]
        public string DeviceOsVersion { get; set; }

        [DataAnnotationsDisplay("Status")]
        public int Status { get; set; }
    }
}