﻿using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class BackEndRegister
    {
        [DataAnnotationsDisplay("Username")]
        [DataAnnotationsRequired]
        public string username { get; set; }

        [DataAnnotationsDisplay("Password")]
        [DataAnnotationsRequired]
        public string password { get; set; }

        [DataAnnotationsDisplay("Password")]
        [DataAnnotationsRequired]
        public string fullName { get; set; }

        [DataAnnotationsDisplay("Email")]
        [DataAnnotationsRequired]
        public string email { get; set; }
    }
}