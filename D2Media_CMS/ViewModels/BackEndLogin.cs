﻿using D2Media_CMS.Helpers;
using D2Media_CMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class BackEndLogin
    {
        [DataAnnotationsDisplay("Username")]
        [DataAnnotationsRequired]
        public string Username { get; set; }

        [DataAnnotationsDisplay("Password")]
        [DataAnnotationsRequired]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}