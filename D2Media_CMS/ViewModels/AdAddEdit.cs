﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class AdAddEdit
    {
        public int Id { get; set; }
        
        [DataAnnotationsDisplay("CategoryId")]
        [DataAnnotationsRequired]
        public int CategoryId { get; set; }

        [DataAnnotationsDisplay("AdName")]
        [DataAnnotationsRequired]
        public string AdName { get; set; }

        [DataAnnotationsDisplay("AdContent")]
        [DataAnnotationsRequired]
        public string AdContent { get; set; }

        [DataAnnotationsDisplay("Link")]
        public string Link { get; set; }
    }
}