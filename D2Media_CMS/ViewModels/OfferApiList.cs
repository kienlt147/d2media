﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_CMS.ViewModels
{
    public class OfferApiMessageList
    {
        public List<OfferApiMessage> OfferApiMessagesList = new List<OfferApiMessage>();
        public string CategoryName { get; set; }
    }

    public class OfferApiMessage
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Message { get; set; }
    }
}