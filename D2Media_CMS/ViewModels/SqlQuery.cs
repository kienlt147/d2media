﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class SqlQuery
    {
        [DataAnnotationsDisplay("Query")]
        [DataAnnotationsRequired]
        public string Query { get; set; }
    }
}