﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;

namespace D2Media_CMS.ViewModels
{
    public class DashboardList
    {
        public List<ReportClick> ReportClicks { get; set; }
        public List<ReportConversion> ReportPayList { get; set; }
        public List<ReportConversion> ConversionThisMonth { get; set; }
        public List<ReportConversion> ConversionLastMonth { get; set; }
        public List<ReportConversion> ConversionToday { get; set; }
        public List<ReportConversion> ConversionYesterday { get; set; }
        public List<Offer> TopOfferHasConversion { get; set; }
        public List<User> TopUserHasConversion { get; set; }
        public List<User> TopPayoutRevenueUsers { get; set; }
    }
}