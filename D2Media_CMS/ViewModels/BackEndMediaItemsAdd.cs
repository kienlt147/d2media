﻿using D2Media_CMS.Helpers;
using System.ComponentModel.DataAnnotations;

namespace D2Media_CMS.ViewModels
{
    public class BackEndMediaItemsAdd
    {
        [DataAnnotationsDisplay("MediaGalleryCode")]
        public string MediaGalleryCode { get; set; }

        [DataAnnotationsDisplay("AllActive")]
        public bool IsAllActive { get; set; }

        [DataAnnotationsDisplay("Photos")]
        public string Photos { get; set; }

        [DataAnnotationsDisplay("YouTubeVideos")]
        public string YouTubeVideos { get; set; }
    }
}
