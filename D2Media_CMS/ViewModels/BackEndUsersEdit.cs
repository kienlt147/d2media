﻿using D2Media_CMS.Helpers;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace D2Media_CMS.ViewModels
{
    public class BackEndUsersEdit
    {
        [DataAnnotationsDisplay("UserId")]
        [DataAnnotationsRequired]
        [DataAnnotationsOnlyIntegers]
        public int? UserId { get; set; }

        [DataAnnotationsDisplay("Username")]
        public string Username { get; set; }

        [DataAnnotationsDisplay("Password")]
        [DataAnnotationsStringLengthRange(8, 255)]
        public string Password { get; set; }

        [Compare("Password", ErrorMessageResourceType = typeof(Resources.Strings), ErrorMessageResourceName = "PasswordAndConfirmationPasswordDoNotMatch")]
        [DataAnnotationsDisplay("ConfirmationPassword")]
        public string ConfirmationPassword { get; set; }

        [DataAnnotationsDisplay("FullName")]
        [DataAnnotationsRequired]
        [DataAnnotationsStringLengthMax(255)]
        public string FullName { get; set; }

        [DataAnnotationsDisplay("SubscriptionEmail")]
        [EmailAddress]
        [DataAnnotationsStringLengthMax(255)]
        public string Email { get; set; }

        [DataAnnotationsDisplay("NumberPhone")]
        [Phone]
        [DataAnnotationsStringLengthMax(25)]
        public string NumberPhone { get; set; }

        [DataAnnotationsDisplay("Group")]
        [DataAnnotationsRequired]
        public int? GroupId { get; set; }

        [DataAnnotationsDisplay("Description")]
        [DataAnnotationsStringLengthMax(1000)]
        public string Description { get; set; }

        [DataAnnotationsDisplay("Tax")]
        [DataAnnotationsStringLengthMax(50)]
        public string Tax { get; set; }

        [DataAnnotationsDisplay("Skype")]
        [DataAnnotationsStringLengthMax(50)]
        public string Skype { get; set; }

        [DataAnnotationsDisplay("Company")]
        [DataAnnotationsStringLengthMax(50)]
        public string Company { get; set; }

        [DataAnnotationsDisplay("PostBackLink")]
        public string PostBackLink { get; set; }

        [DataAnnotationsDisplay("Status")]
        public int Status { get; set; }

        [DataAnnotationsDisplay("AdvName")]
        [DataAnnotationsRequired]
        public int[] SelectedItemIds { get; set; }
    }

    public class UserBankEdit
    {
        [DataAnnotationsDisplay("UserId")]
        public int? Id { get; set; }

        [DataAnnotationsDisplay("BeneficiaryName")]
        public string BeneficiaryName { get; set; }

        [DataAnnotationsDisplay("BankName")]
        public string BankName { get; set; }

        [DataAnnotationsDisplay("AccountNumber")]
        public string AccountNumber { get; set; }

        [DataAnnotationsDisplay("BankAddress")]
        public string BankAddress { get; set; }

        [DataAnnotationsDisplay("SwiftNumber")]
        public string SwiftNumber { get; set; }

        [DataAnnotationsDisplay("Paypal")]
        public string Paypal { get; set; }
    }

    public class UserInfo
    {
        [DataAnnotationsDisplay("Username")]
        public string Username { get; set; }

        [DataAnnotationsDisplay("FullName")]
        public string FullName { get; set; }

        [DataAnnotationsDisplay("SubscriptionEmail")]
        public string Email { get; set; }

        [DataAnnotationsDisplay("NumberPhone")]
        public string NumberPhone { get; set; }

        [DataAnnotationsDisplay("Group")]
        public int? GroupId { get; set; }

        [DataAnnotationsDisplay("Description")]
        public string Description { get; set; }

        [DataAnnotationsDisplay("Tax")]
        public string Tax { get; set; }

        [DataAnnotationsDisplay("Skype")]
        public string Skype { get; set; }

        [DataAnnotationsDisplay("Company")]
        public string Company { get; set; }
    }
}
