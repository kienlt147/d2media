﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;

namespace D2Media_CMS.ViewModels
{
    public class ReportList
    {
        public string DatetimeRanger { get; set; }
        public int TopOffer { get; set; }
        public int Type { get; set; }
        public List<ReportClickDetail> ReportDetails { get; set; }
    }

    public class UserReportList
    {
        public string DatetimeRanger { get; set; }
        public int TopOffer { get; set; }
        public int Type { get; set; }
        public int? UserName { get; set; }
        public List<ReportClickDetail> ReportDetails { get; set; }
    }

    public class ReportGlobalList
    {
        public string DatetimeRanger { get; set; }
    }

    public class ReportHourList
    {
        public string TimePicker { get; set; }
        public List<ReportHour> ReportHours { get; set; }
    }

    public class HighChartSeries
    {
        public string name { get; set; }
        public double data { get; set; }
    }
}