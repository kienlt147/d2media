﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;

namespace D2Media_CMS.ViewModels
{
    public class PostBackAddEdit
    {
        public int Id { get; set; }

        [DataAnnotationsDisplay("UserName")]
        [DataAnnotationsRequired]
        public int UserId { get; set; }

        [DataAnnotationsDisplay("Url")]
        [DataAnnotationsRequired]
        public string Url { get; set; }

        [DataAnnotationsDisplay("Status")]
        public int Status { get; set; } = 1;
    }
}