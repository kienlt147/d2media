﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Helpers;
using D2Media_CMS.Models;

namespace D2Media_CMS.ViewModels
{
    public class OffersList
    {
        public string Name { get; set; } = "";

        [DataAnnotationsDisplay("CategoryName")]
        public int? CategoryId { get; set; }
        public Offer Offer { get; set; }
        public OfferUser OfferUser { get; set; }
    }
}