﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_CMS.Models;

namespace D2Media_CMS.ViewModels
{
    public class CategoriesList
    {
        public string CategoryName { get; set; } = "";
    }

    public class pagination
    {
        public string page { get; set; }
        public string pages { get; set; }
        public string perpage { get; set; }
        public string total { get; set; }
        public string generalSearch { get; set; }
    }

    public class CategoryPaging {
        public List<pagination> pagination { get; set; }
    }
}