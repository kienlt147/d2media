﻿var DataOfferPartner = {
    init: function () {
        var EditLink = "/OfferPublisher?offerId=";
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetOfferPartnerList",
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !1, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#Name") },
            columns: [
                { field: "Id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center" },
                {
                    field: "HasOfferLink", title: "HasOffer Link", filterable: !1, width: 150,
                    template: function (t) {
                        console.log(t.Images);
                        return "<div class=\"m-list-pics m-list-pics--sm\">" +
                            "<a href=\"" +
                            t.HasOfferLink +
                            "\">" +
                            "<img src=\"" +
                            (t.Images === '' ? '/Content/backend/assets/app/media/img/logos/noimage.png' : t.Images) +
                            "\" style=\"width: 50px;\"/></a></div>";
                    }
                },
                { field: "Name", title: "Offer Name", filterable: !1, width: 150 },
                { field: "ConversionLimit", title: "Conversion Limit", sortable: !1 },
                { field: "Revenue", title: "Revenue", sortable: !1, width: 100 },
                { field: "Payout", title: "Payout", sortable: !1 },
                { field: "DeviceOsVersion", title: "Device Os Version", sortable: !1 },
                { field: "DeviceOs", title: "Device Os", sortable: !1 },
                {
                    field: "Actions",
                    width: 70,
                    title: "Actions",
                    sortable: !1,
                    overflow: "visible",
                    locked: { right: "xl" },
                    template: function (t, e, a) {
                        return '\t\t\t\t\t\t<a href="' + EditLink + t.Id +
                            '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Offer Infomartion">\t\t\t\t\t\t\t' +
                            '<i class="flaticon flaticon-file-1"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t';
                    }
                }
            ]
        });
    }
};
jQuery(document).ready(function () { DataOfferPartner.init() });