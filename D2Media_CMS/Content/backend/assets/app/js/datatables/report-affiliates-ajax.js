﻿var DataReportAffiliates = {
    init: function () {
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetReportAffiliateList",
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !1, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#DatetimeRanger") },
            columns: [
                { field: "UserName", title: "Affiliate Name", filterable: !1 },
                { field: "UserId", title: "Affiliate Id", sortable: !1 },
                { field: "TotalUniqueClick", title: "Total Unique Click", sortable: !1 },
                { field: "TotalClick", title: "Total Click", sortable: !1 },
                { field: "TotalConversion", title: "Total Conversion", sortable: !1 },
                {
                    field: "CR", title: "CR", type: "number", filterable: !1,
                    template: function (t) {
                        return (t.TotalConversion / t.TotalClick).toFixed(2) + " %";
                    }
                },
                {
                    field: "TotalPayout", title: "Total Payout", sortable: !1,
                    template: function (t) {
                        return (t.TotalPayout).toFixed(2);
                    }
                },
                {
                    field: "TotalRevenue", title: "Total Revenue", sortable: !1,
                    template: function (t) {
                        return (t.TotalRevenue).toFixed(2);
                    }
                },
                {
                    field: "Profit", title: "Profit", filterable: !1,
                    template: function (t) {
                        return (t.TotalRevenue - t.TotalPayout).toFixed(2);
                    }
                }
            ]
        }), $("#DatetimeRanger").on("change", function () {
            t.search($(this).val(), "DatetimeRanger");
        });
    }
};
jQuery(document).ready(function () { DataReportAffiliates.init() });