﻿var DataPostBacks = {
    init: function () {
        var EditLink = "/PostBackAddEdit/";
        var DeleteLink = "/PostBackDelete/";
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetPostbackList/",
                        map: function (t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !1, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#PostbackName") },
            columns: [
                {
                    field: "Url", title: "Postback Link", sortable: !1,
                    width: "100%",
                    template: function (t) {
                        return t.Url;
                    } 
                },
                {
                    field: "Status",
                    title: "Status",
                    width: 70,
                    locked: { right: "xl" },
                    template: function (t) {
                        var e = {
                            1: { title: "Actice", class: " m-badge--success" },
                            0: { title: "InActive", class: " m-badge--danger" }
                        };
                        return '<span class="m-badge ' +
                            e[t.Status].class +
                            ' m-badge--wide">' +
                            e[t.Status].title +
                            "</span>";
                    }
                },
                {
                    field: "Actions",
                    width: 70,
                    title: "Actions",
                    sortable: !1,
                    overflow: "visible",
                    locked: { right: "xl" },
                    template: function (t, e, a) {
                        return '\t\t\t\t\t\t<a href="' + EditLink + t.Id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t' +
                            '<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t' +
                            '<button href="#" data-action="' + DeleteLink + '" data-id="' + t.Id + '" data-action-delete-item="' + t.Id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill action-delete" title="Delete">\t\t\t\t\t\t\t' +
                            '<i class="la la-trash"></i>\t\t\t\t\t\t</button>\t\t\t\t\t'
                    }
                }
            ]
        });
    }
};
jQuery(document).ready(function () { DataPostBacks.init() });