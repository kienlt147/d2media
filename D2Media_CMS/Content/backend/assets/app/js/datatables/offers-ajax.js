﻿var DataOffer = {
    init: function () {
        var EditLink = "/OfferAddEdit/";
        var DeleteLink = "/OfferDelete/";
        var t;
        t = $(".m_datatable").mDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: Configs.Domain.Link + "GetOffersList/",
                        map: function(t) {
                            var e = t;
                            return void 0 !== t.data && (e = t.data), e;
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: { scroll: !0, footer: !1 },
            sortable: !0,
            pagination: !0,
            toolbar: { items: { pagination: { pageSizeSelect: [10, 20, 30, 50, 100] } } },
            search: { input: $("#Name") },
            columns: [
                { field: "Id", title: "#", sortable: !1, width: 40, selector: !1, textAlign: "center" },
                {
                    field: "HasOfferLink", title: "HasOffer Link", filterable: !1, width: 150,
                    template: function (t) {
                        console.log(t.Images);
                        return "<div class=\"m-list-pics m-list-pics--sm\">" +
                            "<a href=\"" +
                            t.HasOfferLink +
                            "\">" +
                            "<img src=\"" +
                            (t.Images === '' ? '/Content/backend/assets/app/media/img/logos/noimage.png' : t.Images) +
                            "\" style=\"width: 50px;\"/></a></div>";
                    }
                },
                { field: "Name", title: "Offer Name", filterable: !1, width: 150 },
                { field: "HasOfferRevenue", title: "HasOffer Revenue", sortable: !1, width: 150 },
                { field: "ConversionLimit", title: "Conversion Limit", sortable: !1 },
                { field: "Revenue", title: "Revenue", sortable: !1, width: 100 },
                { field: "Payout", title: "Payout", sortable: !1 },
                { field: "DeviceOs", title: "Device Os", sortable: !1 },
                { field: "DeviceOsVersion", title: "Device Os Version", sortable: !1 },
                { field: "CategoryName", title: "Adv Name", sortable: !1 },
                {
                    field: "Status",
                    title: "Status",
                    width: 60,
                    locked: { right: "xl" },
                    template: function(t) {
                        var e = {
                            1: { title: "Start", class: " m-badge--success" },
                            0: { title: "Stop", class: " m-badge--danger" }
                        };
                        return '<button type="button" class="btn m-badge ' + e[t.Status].class + ' m-badge--wide changeStatus" ' +
                            'data-value="' + t.Status + '" data-id="' + t.Id + '">' +
                            e[t.Status].title +
                            "</button>";
                    }
                },
                {
                    field: "Actions",
                    width: 70,
                    title: "Actions",
                    sortable: !1,
                    overflow: "visible",
                    locked: { right: "xl" },
                    template: function(t, e, a) {
                        return '\t\t\t\t\t\t<a href="' + EditLink + t.Id +
                            '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">\t\t\t\t\t\t\t' +
                            '<i class="la la-edit"></i>\t\t\t\t\t\t</a>\t\t\t\t\t\t' +
                            '<button href="#" data-action="' + DeleteLink +
                            '" data-id="' + t.Id +
                            '" data-action-delete-item="' + t.Id +
                            '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill action-delete" title="Delete">\t\t\t\t\t\t\t' +
                            '<i class="la la-trash"></i>\t\t\t\t\t\t</button>\t\t\t\t\t';
                    }
                }
            ]
        }),
        $("#CategoryId").on("change", function () {
            t.search($(this).val(), "CategoryId");
        }),
        $("#CategoryId").selectpicker();
    }
};
jQuery(document).ready(function () { DataOffer.init() });