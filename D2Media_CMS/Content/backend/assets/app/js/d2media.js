﻿jQuery(document).ready(function($) {
    $("#side-menu").metisMenu();

    $(document).on("click", ".action-delete", function (e) {
        if (confirm(confirmDeleteItem.replace("{$ItemName}", $(this).data("action-delete-item")))) {
            $(this).closest("form")
                .attr("action", $(this).data("action"))
                .prepend('<input type="hidden" name="deleteId" value="' + $(this).data("id") + '" />');
            return true;
        } else {
            return false;
        }
    });

    $(".AdminPagesTree").treeview({
        animated: "fast",
        collapsed: true,
        control: ".AdminPagesTreeControl",
        persist: "",
        cookieId: "AdminPagesTree" //Unique name
    });

    $(".reset").click(function () {
        window.location.href = window.location.href.split('?')[0] + "?reset=true";
        return false;
    });

    $(".reset-form").click(function () {
        window.location.href = window.location.href;
        return false;
    });

    $(".redirect").click(function () {
        window.location.href = $(this).attr("data-redirect-url");
        return false;
    });

    $(".filepath-preview").each(function (index) {
        $(this).attr("id", "filepath-preview-" + index);
        swfobject.embedSWF($(this).attr("title"), "filepath-preview-" + index, "100%", "70", "9.0.0", false, false, { wmode: "transparent" }, false, false);
    });

    $(".ckfinder-file-textbox").each(function() {
        var name = $(this).attr("name");
        if ($(this).val() !== "") {
            var temp = $.string($(this).val().toLowerCase());
            if (temp.endsWith(".swf")) {
                swfobject.embedSWF($(this).val(),
                    "ckfinder-swf-preview-" + name,
                    "100%",
                    "100%",
                    "9.0.0",
                    false,
                    false,
                    { wmode: "transparent" },
                    false,
                    false);
                $("#ckfinder-swf-preview-" + name).removeClass("hidden");
            } else if (temp.endsWith(".jpg") ||
                temp.endsWith(".jpeg") ||
                temp.endsWith(".gif") ||
                temp.endsWith(".png")) {
                $("#ckfinder-img-preview-" + name).attr("src", $(this).val()).removeClass("hidden");
            } else {
                $("#ckfinder-file-preview-" + name).attr("href", $(this).val()).removeClass("hidden");
            }
        }
    });

    $(".ckfinder-standalone").each(function (index, value) {
        var finder = new CKFinder();
        finder.basePath = "/ckfinder/";
        finder.height = 600;
        finder.language = adminLanguageCode;
        finder.resourceType = adminResourceType;
        finder.selectMultiple = false;
        finder.callback = function (api) {
            api.disableFileContextMenuOption("deleteFile", false);
            api.disableFileContextMenuOption("renameFile", false);
            api.disableFolderContextMenuOption("removeFolder", false);
            api.disableFolderContextMenuOption("renameFolder", false);
        };
        ckFinderApi = finder.appendTo(value);
    });

    $(".ckfinder-file").on("click", function () {
        var finder = new CKFinder();
        finder.basePath = "/ckfinder/";
        finder.height = 600;
        finder.language = adminLanguageCode;
        finder.resourceType = $(this).attr("data-ckfinder-resourcetype");
        finder.selectActionData = $(this).attr("data-ckfinder-file");
        finder.selectMultiple = false;
        finder.selectActionFunction = function (fileUrl, data, allFiles) {
            $("#ckfinder-swf-preview-" + data["selectActionData"]).addClass("hidden");
            $("#ckfinder-img-preview-" + data["selectActionData"]).addClass("hidden");
            $("#ckfinder-file-preview-" + data["selectActionData"]).addClass("hidden");
            $("#" + data["selectActionData"]).val(fileUrl);
            console.log(fileUrl);
            
//            var temp = $.string(fileUrl.toLowerCase());
//            
//            if (temp.endsWith(".swf")) {
//                swfobject.embedSWF(fileUrl, "ckfinder-swf-preview-" + data["selectActionData"], "100%", "100%", "9.0.0", false, false, { wmode: "transparent" }, false, false);
//                $("#ckfinder-swf-preview-" + data["selectActionData"]).removeClass("hidden");
//                console.log("3");
//            } else if (temp.endsWith(".jpg") || temp.endsWith(".jpeg") || temp.endsWith(".gif") || temp.endsWith(".png")) {
//                $("#ckfinder-img-preview-" + data["selectActionData"]).attr("src", fileUrl).removeClass("hidden");
//                console.log("2");
//            } else {
//                $("#ckfinder-file-preview-" + data["selectActionData"]).attr("href", fileUrl).removeClass("hidden");
//                console.log("1");
//            }
        };
        finder.callback = function (api) {
            api.disableFileContextMenuOption("deleteFile", false);
            api.disableFileContextMenuOption("renameFile", false);
            api.disableFolderContextMenuOption("removeFolder", false);
            api.disableFolderContextMenuOption("renameFolder", false);
        };
        finder.popup();
        return false;
    });

    $(".ckfinder-file-remove").on("click", function () {
        var selectActionData = $(this).attr("data-ckfinder-file");
        $("#" + selectActionData).val("");
        $("#ckfinder-swf-preview-" + selectActionData).addClass("hidden");
        $("#ckfinder-img-preview-" + selectActionData).addClass("hidden");
        $("#ckfinder-file-preview-" + selectActionData).addClass("hidden");
    });
    
    $('input[name="DatetimeRanger"]').daterangepicker({
        "linkedCalendars": false,
        "alwaysShowCalendars": true,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, function (start, end, label) {
        console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
        $("#DatetimeRanger").val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
    });

    $('input[name="datefilter"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    });

    $('input[name="datefilter"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });

    $("#TimePicker").datepicker({
        todayHighlight: !0,
        orientation: "bottom left",
        autoclose: !0,
        templates: {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    });

//    $(".tokenfield-multifilepath").on('tokenfield:createdtoken', function (e) {
//        var tokenTitle;
//        var temp = $.string(e.attrs.value.toLowerCase());
//        if (temp.endsWith(".swf")) {
//            tokenTitle = "<object width='100%' height='100%' data='" + e.attrs.value + "' type='application/x-shockwave-flash'><param name='wmode' value='transparent'></object>";
//        } else if (temp.endsWith(".gif") || temp.endsWith(".jpg") || temp.endsWith(".jpeg") || temp.endsWith(".png")) {
//            var thumbImage = e.attrs.value;
//            if (temp.include("files/")) {
//                thumbImage = thumbImage.replace("Files/", "_thumbs/Files/");
//            } else if (temp.include("images/")) {
//                thumbImage = thumbImage.replace("Images/", "_thumbs/Images/");
//            }
//            tokenTitle = "<img src='" + thumbImage + "'/>";
//        } else {
//            tokenTitle = "";
//        }
//        if (tokenTitle !== "") {
//            $(e.relatedTarget).tooltip({
//                html: true,
//                title: tokenTitle,
//                container: e.relatedTarget,
//                placement: "bottom"
//            });
//        }
//    }).on('tokenfield:initialize', function (e) {
//        $(".tokenfield-multifilepath").each(function () {
//            $(this).parent().find(".token-input").hide();
//        });
//    }).tokenfield();

    $(".ckfinder-file-multiple").on("click", function () {
        var finder = new CKFinder();
        finder.basePath = "/ckfinder/";
        finder.height = 600;
        finder.language = adminLanguageCode;
        finder.selectActionData = $(this).attr("data-ckfinder-file");
        finder.resourceType = $(this).data("ckfinder-resourcetype");
        finder.selectMultiple = true;
        finder.selectActionFunction = function (fileUrl, data, allFiles) {
            var existingTokens = $("#" + data["selectActionData"]).tokenfield('getTokens');
            var isContained;
            for (var i = 0; i < allFiles.length; i++) {
                isContained = false;
                $.each(existingTokens, function (index, token) {
                    if (token.value === allFiles[i].url) {
                        isContained = true;
                    }
                });
                if (!isContained) {
                    $("#" + data["selectActionData"]).tokenfield("createToken", allFiles[i].url);
                }
            }
        };
        finder.callback = function (api) {
            api.disableFileContextMenuOption("deleteFile", false);
            api.disableFileContextMenuOption("deleteFiles", false);
            api.disableFileContextMenuOption("renameFile", false);
            api.disableFolderContextMenuOption("removeFolder", false);
            api.disableFolderContextMenuOption("renameFolder", false);

            var toolId = api.addTool(helpSelectMultipleFiles);
            api.showTool(toolId);
        };
        finder.popup();
    });

    tinymce.init({
        selector: ".tinymce-editor",
        height: 600,
        image_advtab: true,
        forced_root_block: "",
        valid_elements: "+*[*]",
        content_css: "/Content/backend/css/content.css", //"/bundles/frontend-css?v=" + (new Date()).getTime(),
        language: adminLanguageCode.replace('-', '_'),
        convert_urls: false,
        las_seconds: 15,
        las_keyName: "LocalAutoSave",
        las_callback: function () {
            //var content = this.content; //content saved
            //var time = this.time; //time on save action
        },
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor localautosave codemirror cmsmodules template table colorpicker"
        ],
        codemirror: {
            indentOnInit: true,
            path: "CodeMirror",
            config: {
                lineNumbers: true,
                tabSize: 4,
                indentWithTabs: false,
                indentUnit: 4,
                lineWrapping: false
            }
        },
        toolbar: "localautosave | bold italic | alignleft aligncenter alignright alignjustify| forecolor backcolor fontsizeselect fontselect | bullist numlist outdent indent | link image media | cmsmodules | template | fullscreen",

        textcolor_map: [
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Red violet",
            "FFFFFF", "White",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum"
        ],

        cmsmodules_url: "/GetCmsModules/",
        templates: "/GetContentTemplates/",
        file_browser_callback: function (field_name, url, type, win) {
            var resourceType;
            if (adminResourceType === "") {
                switch (type) {
                    case "image":
                        resourceType = "Images";
                        break;
                    case "media":
                        resourceType = "Videos";
                        break;
                    default:
                        resourceType = "Files";
                        break;
                }
            } else {
                resourceType = adminResourceType;
            }
            tinymce.activeEditor.windowManager.open({
                title: "File manager",
                url: "/ckfinder/ckfinder.html?type=" + resourceType + "&action=js&func=SetTinyMceFileUrl&langCode=" + adminLanguageCode,
                width: 950,
                height: 600
            }, {
                    oninsert: function (url) {
                        var date = new Date();
                        var timestamp = date.getTime();

                        var fieldElm = win.document.getElementById(field_name);

                        //Update the field value
                        //                    fieldElm.value = url + "?v=" + timestamp;
                        fieldElm.value = url;

                        //Fire the onchange event to auto fill width and height
                        if ("createEvent" in document) {
                            var evt = document.createEvent("HTMLEvents");
                            evt.initEvent("change", false, true);
                            fieldElm.dispatchEvent(evt);
                        } else {
                            fieldElm.fireEvent("onchange");
                        }

                        //Close the popup
                        tinymce.activeEditor.windowManager.close();
                    }
                });
        }
    });
});