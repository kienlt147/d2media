(function() {
  $(".set_slug").click(function() {
    var slugFieldValue;
    slugFieldValue = $("#Name").val().toLowerCase().replace(/\ /g, "-");
    if ($("#Slug").val().trim() !== "") {
      if (confirm(segmentNotEmptyPressOkToOverwrite)) {
        return $("#Slug").val(RemoveUTF8(slugFieldValue));
      }
    } else {
      return $("#Slug").val(RemoveUTF8(slugFieldValue));
    }
  });

}).call(this);
