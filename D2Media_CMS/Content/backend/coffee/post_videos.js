(function() {
  $(".add_new_category").click(function() {
    return $(".form_category_post").animate({
      left: "+=50",
      height: "toggle"
    }, 1000, function() {});
  });

  $("#IsYoutube").click(function() {
    $("#isYoutubeUrl").toggle(this.checked);
    return $("#isServerUrl").toggle(!this.checked);
  });

  $(".add_new_category_news").click(function() {
    var category_id, category_name;
    category_id = $("#CategoryParentId").val();
    category_name = $(".category_new_name").val();
    if ((category_name == null) || category_name === "") {
      $(".message_result_categories").addClass("alert alert-warning").html("Tên chuyên mục không được để trống.");
      return false;
    }
    if ((category_id == null) || category_id === "") {
      $(".message_result_categories").addClass("alert alert-warning").html("Chuyên mục không được để trống.");
      return false;
    }
    return $.ajax({
      url: "/AjaxCategoryAdd",
      type: "GET",
      data: {
        category_id: category_id,
        category_name: category_name
      },
      dataType: "json",
      success: function(data) {
        if (data.Status === "0") {
          $(".message_result_categories").addClass("alert alert-success").html(data.Message);
          $("#CategoryParentId").empty().append(data.Data);
          $("#CategoryParentId").selectpicker("refresh");
          $("#CategoryId").empty().append(data.Data);
          return $("#CategoryId").selectpicker("refresh");
        } else {
          return $(".message_result_categories").addClass("alert alert-danger").html(data.Message);
        }
      }
    });
  });

  $(".add_news_tags").click(function() {
    var array_tag, i, milliseconds, tags_hidden, tags_name, tags_value, _i, _len;
    tags_name = $("#tags_name").val();
    console.log(tags_name);
    tags_hidden = $("#tags_hidden").val();
    tags_value = "";
    milliseconds = new Date().getTime();
    if ((tags_name == null) || tags_name === "") {
      return false;
    }
    array_tag = tags_name.split(',');
    if (array_tag.length > 1) {
      for (_i = 0, _len = array_tag.length; _i < _len; _i++) {
        i = array_tag[_i];
        if ((i == null) || i === "") {
          continue;
        }
        $("#view_all_tags").append('<span class="btn btn-default btn-xs margin-5">' + '<a id="post_tag_' + _i + '" data-value="' + i + '" onclick="return RemoveTags(' + _i + ')">' + '<i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;' + i + '</span>');
      }
    } else {
      $("#view_all_tags").append('<span class="btn btn-default btn-xs margin-5">' + '<a id="post_tag_' + milliseconds + '" data-value="' + tags_name + '" onclick="return RemoveTags(' + milliseconds + ')">' + '<i class="fa fa-times" aria-hidden="true"></i></a>&nbsp;' + tags_name + '</span>');
    }
    $("#view_all_tags").find('a').each(function() {
      if (tags_value === "") {
        return tags_value += $(this).data("value").toString().toLowerCase().trim();
      } else {
        return tags_value += "," + $(this).data("value").toString().toLowerCase().trim();
      }
    });
    $("#tags_name").val("");
    return $("#tags_hidden").val(tags_value);
  });

}).call(this);
