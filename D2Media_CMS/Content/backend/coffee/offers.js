﻿// Generated by IcedCoffeeScript 108.0.11
(function() {
  $(document).on("click", ".changeStatus", function() {
    var offer_id, offer_status, that;
    $(".loader_main").show();
    offer_status = $(this).data("value");
    offer_id = $(this).data("id");
    that = $(this);
    return $.ajax({
      url: "/api/offers/changeStatus/",
      type: "GET",
      data: {
        offerId: offer_id,
        offerStatus: offer_status
      },
      dataType: "json",
      success: function(data) {
        $(".loader_main").hide();
        if (data.ErrCode === 200) {
          if (offer_status === 1) {
            that.data("value", 0);
            that.html('Stop');
            that.removeClass("m-badge--success").addClass("m-badge--danger");
          } else {
            that.data("value", 1);
            that.html('Start');
            that.removeClass("m-badge--danger").addClass("m-badge--success");
          }
          return $.notify({
            title: '<strong>Saving.</strong>',
            message: data.ErrMessage
          }, {
            element: 'body',
            type: 'success',
            animate: {
              enter: 'animated fadeInDown',
              exit: 'animated fadeOutUp'
            }
          });
        } else {
          return $.notify({
            title: '<strong>Error.</strong>',
            message: data.ErrMessage
          }, {
            element: 'body',
            type: 'danger',
            animate: {
              enter: 'animated fadeInDown',
              exit: 'animated fadeOutUp'
            }
          });
        }
      }
    });
  });

  $(".requestApproval").click(function() {
    var offer_id, that;
    $(".loader_main").show();
    offer_id = $(this).data("offerid");
    $(this).html('Sending...');
    $(this).addClass('m-loader m-loader--primary m-loader--left');
    $(this).prop('disabled', true);
    that = $(this);
    return $.ajax({
      url: apiUrl + "/api/offers/requestApproval/",
      crossDomain: true,
      type: "GET",
      data: {
        offerId: offer_id,
        authToken: authToken
      },
      dataType: "json",
      success: function(data) {
        $(".loader_main").hide();
        if (data.ErrCode === 200) {
          return location.reload();
        } else {
          that.removeClass('m-loader m-loader--primary m-loader--left');
          that.html('<i class="flaticon-cart"></i> Request Approval');
          that.prop('disabled', false);
          return $.notify({
            title: '<strong>Request Approval.</strong>',
            message: data.ErrMessage
          }, {
            element: 'body',
            type: 'danger',
            animate: {
              enter: 'animated fadeInDown',
              exit: 'animated fadeOutUp'
            }
          });
        }
      }
    });
  });

  $(document).on("click", ".acceptPartnerRequest", function() {
    var id, offer_id, that, user_id;
    $(".loader_main").show();
    id = $(this).data("id");
    offer_id = $(this).data("offerid");
    user_id = $(this).data("userid");
    $(this).prop('disabled', true);
    that = $(this);
    return $.ajax({
      url: apiUrl + "/api/offers/acceptPartnerRequest/",
      crossDomain: true,
      type: "GET",
      data: {
        id: id,
        offerId: offer_id,
        userRequestId: user_id,
        authToken: authToken
      },
      dataType: "json",
      success: function(data) {
        $(".loader_main").hide();
        if (data.ErrCode === 200) {
          $(".m_datatable").mDatatable("reload");
          return $.notify({
            title: '<strong>Success.</strong>',
            message: data.ErrMessage
          }, {
            element: 'body',
            type: 'success',
            animate: {
              enter: 'animated fadeInDown',
              exit: 'animated fadeOutUp'
            }
          });
        } else {
          that.prop('disabled', false);
          return $.notify({
            title: '<strong>Error.</strong>',
            message: data.ErrMessage
          }, {
            element: 'body',
            type: 'danger',
            animate: {
              enter: 'animated fadeInDown',
              exit: 'animated fadeOutUp'
            }
          });
        }
      }
    });
  });

}).call(this);
