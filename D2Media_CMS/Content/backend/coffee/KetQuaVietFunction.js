(function() {
  var kqv;

  kqv = typeof exports !== "undefined" && exports !== null ? exports : this;

  kqv.FormatNumberToMoney = function(number) {
    return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  };

}).call(this);
