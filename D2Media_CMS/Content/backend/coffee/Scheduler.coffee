﻿fn = exports ? this
fn.loadFullCalendar = (dataEvents)->
    $('#calendar').fullCalendar
        header:
            left: 'prev,next today',
            center: 'title',
            right: 'agendaWeek,agendaDay'
        ,
        buttonText: 
            today: 'Hôm nay',
            month: 'Tháng',
            week: 'Tuần',
            day: 'Ngày',
            list: 'Danh sách'
        ,
        titleFormat: 'DD/MM/YYYY',
        dayNames: ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bẩy'],
        dayNamesShort: ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bẩy'],
        defaultView: 'agendaWeek',
        columnFormat: 'ddd DD/MM',
        displayEventTime: false,
        slotLabelFormat: 'H:mm',
        allDaySlot: false,
        eventDurationEditable: false,
        selectHelper: true,
        navLinks: true,
        editable: false,
        eventColor: '#337ab7',
        eventLimit: false,
        events: dataEvents,
        eventRender: (event, element) ->
            element.attr('href', '#')
            element.attr('data-target', '#login-modal')
            element.attr('data-toggle', 'modal')
            if event.imageurl isnt ""
                element.find "div.fc-content"
                    .prepend "<img src='" + event.imageurl + "' width='16' height='16'>"
            element.click ->
                id = $("#SchedulerId").val() is "" ? 0 : $("#SchedulerId").val()
                if id > 0
                    $("#addScheduler").hide()
                $(".modal-title").html(event.title)
                $("#SchedulerId").val(event.id)
                $("#ImgPath").val(event.imageurl)
                $("#TitleScheduler").val(event.title)
                SetDate("ScheduleStartDate", event.start._i)
                SetTime("ScheduleStartTimer", event.start._i)
                SetDate("ScheduleEndDate", event.end._i)
                SetTime("ScheduleEndTimer", event.end._i)
                $(".delete_scheduler").show()
                $(".alert_scheduler").hide()
                $('#SchedulerForm')
                    .find(".scheduler_form")
                        .each (index)->
                            if index > 0
                                $(this).remove()
                    
        ,
        dayClick: (date, jsEvent, view) ->
            $("#addScheduler").show()
            $(".modal-title").html("Thêm lịch phát sóng")
            $("#SchedulerId").val(0)
            $("#ImgPath").val("")
            $("#TitleScheduler").val("")
            SetDate("ScheduleStartDate", date.format())
            SetTime("ScheduleStartTimer", date.format())
            SetDate("ScheduleEndDate", date.format())
            SetTime("ScheduleEndTimer", date.format())
            $("#login-modal").modal("show")
            $(".delete_scheduler").hide()
            $(".alert_scheduler").hide()
            $('#SchedulerForm')
                    .find(".scheduler_form")
                        .each (index)->
                            if index > 0
                                $(this).remove()
        ,
        eventMouseover: (event, jsEvent, view) ->
            $(this).css('background-color', '#5bc0de')
        ,
        eventMouseout: (event, jsEvent, view) ->
            $(this).css('background-color', '#337ab7')

fn.Scheduler = {
    data: {}
    init: () ->
        loadFullCalendar(this.data.events)
}