(function() {
  var fn;

  fn = typeof exports !== "undefined" && exports !== null ? exports : this;

  fn.loadFullCalendar = function(dataEvents) {
    return $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'Hôm nay',
        month: 'Tháng',
        week: 'Tuần',
        day: 'Ngày',
        list: 'Danh sách'
      },
      titleFormat: 'DD/MM/YYYY',
      dayNames: ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bẩy'],
      dayNamesShort: ['Chủ nhật', 'Thứ hai', 'Thứ ba', 'Thứ tư', 'Thứ năm', 'Thứ sáu', 'Thứ bẩy'],
      defaultView: 'agendaWeek',
      columnFormat: 'ddd DD/MM',
      displayEventTime: false,
      slotLabelFormat: 'H:mm',
      allDaySlot: false,
      eventDurationEditable: false,
      selectHelper: true,
      navLinks: true,
      editable: false,
      eventColor: '#337ab7',
      eventLimit: false,
      events: dataEvents,
      eventRender: function(event, element) {
        element.attr('href', '#');
        element.attr('data-target', '#login-modal');
        element.attr('data-toggle', 'modal');
        if (event.imageurl !== "") {
          element.find("div.fc-content").prepend("<img src='" + event.imageurl + "' width='16' height='16'>");
        }
        return element.click(function() {
          var id, _ref;
          id = (_ref = $("#SchedulerId").val() === "") != null ? _ref : {
            0: $("#SchedulerId").val()
          };
          if (id > 0) {
            $("#addScheduler").hide();
          }
          $(".modal-title").html(event.title);
          $("#SchedulerId").val(event.id);
          $("#ImgPath").val(event.imageurl);
          $("#TitleScheduler").val(event.title);
          SetDate("ScheduleStartDate", event.start._i);
          SetTime("ScheduleStartTimer", event.start._i);
          SetDate("ScheduleEndDate", event.end._i);
          SetTime("ScheduleEndTimer", event.end._i);
          $(".delete_scheduler").show();
          $(".alert_scheduler").hide();
          return $('#SchedulerForm').find(".scheduler_form").each(function(index) {
            if (index > 0) {
              return $(this).remove();
            }
          });
        });
      },
      dayClick: function(date, jsEvent, view) {
        $("#addScheduler").show();
        $(".modal-title").html("Thêm lịch phát sóng");
        $("#SchedulerId").val(0);
        $("#ImgPath").val("");
        $("#TitleScheduler").val("");
        SetDate("ScheduleStartDate", date.format());
        SetTime("ScheduleStartTimer", date.format());
        SetDate("ScheduleEndDate", date.format());
        SetTime("ScheduleEndTimer", date.format());
        $("#login-modal").modal("show");
        $(".delete_scheduler").hide();
        $(".alert_scheduler").hide();
        return $('#SchedulerForm').find(".scheduler_form").each(function(index) {
          if (index > 0) {
            return $(this).remove();
          }
        });
      },
      eventMouseover: function(event, jsEvent, view) {
        return $(this).css('background-color', '#5bc0de');
      },
      eventMouseout: function(event, jsEvent, view) {
        return $(this).css('background-color', '#337ab7');
      }
    });
  };

  fn.Scheduler = {
    data: {},
    init: function() {
      return loadFullCalendar(this.data.events);
    }
  };

}).call(this);
