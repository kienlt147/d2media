﻿kqv = exports ? this

kqv.FormatNumberToMoney = (number) ->
    number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
