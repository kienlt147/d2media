﻿var Draws = React.createClass({
    getInitialState: function () {
        return {
            data: this.props.initialData
        }
    },
    loadCommentsFromServer: function () {
        var xhr = new XMLHttpRequest();
        xhr.open('get', this.props.url, true);
        xhr.onload = function () {
            var data = JSON.parse(xhr.responseText);
            this.setState({ data: data });
        }.bind(this);
        xhr.send();
    },
    componentDidMount: function () {
        this.loadCommentsFromServer();
        window.setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render: function () {
        return (
            <div className="draws">
                <span className="message_server"></span>
                <h1>Draw List</h1>
                <DrawList data={this.state.data} />
            </div>
        );
    }
});

var DrawList = React.createClass({
    render: function () {
        var commentNodes = this.props.data.map(function(comment) {
            return (
                <Draw drawId={comment.DrawId} key={comment.Id}>
                    {comment.DrawId}
                </Draw>
            );
        });
        return (
            <div className="drawList">
                {commentNodes}
            </div>
        );
    }
});


var Draw = React.createClass({
    rawMarkup: function() {
        //var md = new Remarkable();
        var md = new (typeof global !== "undefined" ? global.Remarkable : window.Remarkable)();
        var rawMarkup = md.render(this.props.children.toString());
        return { __html: rawMarkup };
    },
    render: function() {
        //var md = new (global.Remarkable || window.Remarkable)();
        return (
            <div className="draw">
            <h2 className="drawId">
                {this.props.DrawId}
            </h2>
            <span dangerouslySetInnerHTML={this.rawMarkup()} />
            </div>
        );
    }
});