﻿var BankGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage,
            searchText: this.state.Data.SearchText
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    handleBankRemove: function (objectId) {
        var params = {
            Id: objectId
        }
        $.ajax({
            url: this.props.urlDelete,
            type: 'GET',
            data: params,
            success: function (data) {
                if (data.Code == 0) {
                    this.populateData();
                } else {
                    alert(data.Message);
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    searchChange: function (value) {
        var data = this.state.Data;
        data.SearchText = value;
        this.setState({
            Data: data
        });
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <BankGridRow key={item.Id} item={item} onBankRemove={that.handleBankRemove} urlBankAddEdit={that.props.urlAddEdit}/>
                )
            });
        }
        
        return (
            <div>
                <SearchBox onSearchChanged={this.searchChange} searchText={this.state.Data.SearchText} urlBankAddEdit={that.props.urlAddEdit}/>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>#</th>
                            <th>User Id</th>
                            <th>Tên ngân hàng</th>
                            <th>Số tài khoản</th>
                            <th>Số điện thoại</th>
                            <th>Email</th>
                            <th>Chi nhánh</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var BankGridRow = React.createClass({
    handleRemoveBank: function (objectId, e) {
        e.preventDefault();
        if (confirm("Bạn chắc chắn muốn xóa bản ghi?")) {
            this.props.onBankRemove(this.props.item.Id)
        }
    },
    render: function () {
        return (
            <tr>
                <td>{this.props.item.Id}</td>
                <td>{this.props.item.UserId}</td>
                <td>{this.props.item.BankName}</td>
                <td>{this.props.item.BankNumber}</td>
                <td>{this.props.item.NumberPhone}</td>
                <td>{this.props.item.Email}</td>
                <td>{this.props.item.Branch}</td>
                <td className="col-10">
                    <a href={this.props.urlBankAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
                <td className="col-10">
                    <button type="button" className="btn btn-danger" onClick={this.handleRemoveBank.bind(null,this)}>
                        <i className="fa fa-trash-o"></i>
                    </button>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    handleChange : function(e){
        this.props.onSearchChanged(e.target.value);
    },
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <div className="col-md-4">
                        <input type="text" value={this.props.searchText} className="form-control" placeholder="Tìm kiếm" onChange={this.handleChange} />
                    </div>
                </div>
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlBankAddEdit} className="btn btn-gray" title="Thêm ngân hàng">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

