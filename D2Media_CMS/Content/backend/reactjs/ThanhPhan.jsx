﻿var ThanhPhanGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    handleThanhPhanRemove: function (objectId) {
        var params = {
            Id: objectId
        }
        $.ajax({
            url: this.props.urlDelete,
            type: 'GET',
            data: params,
            success: function (data) {
                if (data.Code == 0) {
                    this.populateData();
                } else {
                    alert(data.Message);
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <ThanhPhanGridRow key={item.Id} item={item} onThanhPhanRemove={that.handleThanhPhanRemove} urlThanhPhanAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={10}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <SearchBox urlThanhPhanAddEdit={that.props.urlAddEdit}/>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>Cặp số 1</th>
                            <th>Cặp số 2</th>
                            <th>Kết quả</th>
                            <th>Ngày phán</th>
                            <th>Loại</th>
                            <th>Người phán</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var ThanhPhanGridRow = React.createClass({
    handleRemoveThanhPhan: function (objectId, e) {
        e.preventDefault();
        if (confirm("Bạn chắc chắn muốn xóa bản ghi?")) {
            this.props.onThanhPhanRemove(this.props.item.Id)
        }
    },
    render: function () {
        return (
            <tr>
                <td>{this.props.item.NumberOne}</td>
                <td>{this.props.item.NumberTwo}</td>
                <td>{this.props.item.Correct}</td>
                <td>{this.props.item.DrawDate}</td>
                <td>{this.props.item.Name}</td>
                <td>{this.props.item.FullName}</td>
                <td className="col-10">
                    <a href={this.props.urlThanhPhanAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
                <td className="col-10">
                    <button type="button" className="btn btn-danger" onClick={this.handleRemoveThanhPhan.bind(null,this)}>
                        <i className="fa fa-trash-o"></i>
                    </button>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlThanhPhanAddEdit} className="btn btn-gray" title="Thêm mới">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

