﻿var FeedBackGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage,
            searchText: this.state.Data.SearchText
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <FeedBackGridRow key={item.Id} item={item}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={4}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>#</th>
                            <th>Mã người dùng</th>
                            <th>Tên</th>
                            <th>Nội dung</th>
                            
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={that.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var FeedBackGridRow = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.item.Id}</td>
                <td>{this.props.item.UserId}</td>
                <td>{this.props.item.UserName}</td>
                <td>{this.props.item.Message}</td>
            </tr>
        )
    }
});

