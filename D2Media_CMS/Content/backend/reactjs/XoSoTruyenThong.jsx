﻿var XoSoTruyenThongGridTable = React.createClass({
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                List: [],
                Provinces: [],
                ProvinceId: 1,
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            provinceId: this.state.Data.ProvinceId,
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    searchChange: function (value) {
        var data = this.state.Data;
        data.ProvinceId = value;
        this.setState({
            Data: data
        });
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.List != null) {
            this.state.Data.List.forEach(function (item) {
                rows.push(
                    <XoSoTruyenThongGridRow key={item.Id} item={item} urlXoSoTruyenThongAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={10}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }

        return (
            <div>
                <SearchBox onSearchChanged={this.searchChange} urlXoSoTruyenThongAddEdit={that.props.urlAddEdit} provinceData={this.state.Data.Provinces}/>
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>Giải</th>
                            <th>Kết quả</th>
                            <th>Loto</th>
                            <th>Tỉnh thành</th>
                            <th>Ngày quay thưởng</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var XoSoTruyenThongGridRow = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.item.PrizeName}</td>
                <td>{this.props.item.Number}</td>
                <td>{this.props.item.Loto}</td>
                <td>{this.props.item.ProvinceName}</td>
                <td>{this.props.item.DrawDate}</td>
                <td className="col-10">
                    <a href={this.props.urlXoSoTruyenThongAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    handleChange: function (e) {
        this.props.onSearchChanged(e.target.value);
    },
    render: function () {
        var rows = [];
        if (this.props.provinceData != null) {
            this.props.provinceData.forEach(function (item) {
                rows.push(
                    <option key={item.PID} value={item.PID}>{item.Name}</option>
                )
            });
        }
        return (
            <div className="panel panel-default">
                <div className="panel-body">
                    <div className="col-md-4">
                        <select className="form-control" onChange={this.handleChange}>{rows}</select>
                    </div>
                </div>
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlXoSoTruyenThongAddEdit} className="btn btn-gray" title="Thêm mới">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});
