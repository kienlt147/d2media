﻿var Max4DGridTable = React.createClass({
    state: {
        loading: true
    },
    mixins: [Configs],
    getInitialState: function () {
        return {
            Data: {
                ResultMax4DList: [],
                TotalPage: 0,
                CurrentPage: 1,
                PageSize: this.Pagination.PageSize,
                SearchText: ''
            }
        }
    },
    componentDidMount: function () {
        this.populateData();
    },
    populateData: function () {
        var params = {
            gameId: this.props.GameId,
            pageSize: this.state.Data.PageSize,
            currentPage: this.state.Data.CurrentPage
        }
        $.ajax({
            url: this.props.url,
            type: 'GET',
            data: params,
            success: function (data) {
                if (this.isMounted()) {
                    this.setState({ Data: data });
                }
            }.bind(this),
            error: function (err) {
                alert('Error');
            }.bind(this)
        });
    },
    pageChanged: function(pageNumber, e){
        e.preventDefault();
        this.state.Data.CurrentPage = pageNumber;
        this.populateData();
    },
    render: function () {
        var rows = [];
        var that = this;
        if (this.state.Data.ResultMax4DList != null) {
            this.state.Data.ResultMax4DList.forEach(function (item) {
                rows.push(
                    <Max4DGridRow key={item.Id} item={item} urlAdAddEdit={that.props.urlAddEdit}/>
                )
            });
        } else {
            rows.push(
                <tr key="-1">
                    <td colSpan={10}>Chưa có bản ghi nào...</td>
                </tr>
            )
        }
        return (
            <div>
                <SearchBox urlMax4DAddEdit={that.props.urlAddEdit} />
                <table className="table table-hover table-responsive table-bordered">
                    <thead>
                        <tr className="webgrid-header">
                            <th>Kỳ quay</th>
                            <th>Giải nhất</th>
                            <th>Giải nhì</th>
                            <th>Giải Ba</th>
                            <th>Ngày quay</th>
                            <th className="col-10"></th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
                <GridPager Size={this.state.Data.TotalPage} onPageChanged={this.pageChanged} CurrentPage={this.state.Data.CurrentPage} />
            </div>
        )
    }
});

var Max4DGridRow = React.createClass({
    render: function () {
        return (
            <tr>
                <td>{this.props.item.DrawId}</td>
                <td>{this.props.item.FirstPrize}</td>
                <td>{this.props.item.SecondPrize1} - {this.props.item.SecondPrize2}</td>
                <td>{this.props.item.ThirdPrize1} - {this.props.item.ThirdPrize2} - {this.props.item.ThirdPrize3}</td>
                <td>{this.props.item.DrawDate}</td>
                <td>
                    <a href={this.props.urlAdAddEdit+this.props.item.Id} className="btn btn-info">
                        <i className="fa fa-pencil-square-o"></i>
                    </a>
                </td>
            </tr>
        )
    }
});

var SearchBox = React.createClass({
    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-footer">
                    <div className="btn-toolbar">
                        <div className="btn-group">
                            <a href={this.props.urlMax4DAddEdit} className="btn btn-gray" title="Thêm mới">
                                <span className="fa fa-plus-square"></span>
                                <span className="btn-text"> Thêm mới</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});