﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace D2Media_CMS.ModuleConnectors
{
    public interface IModuleConnector
    {
        List<SelectListItem> GetSelectItemList();

        //        string GetContent(HtmlHelper htmlHelper, FrontEndCmsPage model, string id);

        bool IsFileUsed(string filePath);
    }
}