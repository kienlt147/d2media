﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace D2Media_API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
//            var cors = new EnableCorsAttribute("http://localhost:53674, ", "accept,accesstoken,authorization,cache-control,pragma,content-type,origin", "GET,PUT,POST,DELETE,TRACE,HEAD,OPTIONS");
            config.EnableCors();
            // Web API configuration and services
            //            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            //            config.EnableCors(corsAttr);
            //            config.SuppressDefaultHostAuthentication();
            //            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Affiliate",
                routeTemplate: "aff_link/{id}",
                defaults: new { controller = "Affiliate", action = "Index", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "Affiliate2",
                routeTemplate: "aff_link2/{id}",
                defaults: new { controller = "Affiliate2", action = "Index", id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "PostBack",
                routeTemplate: "postback/{id}",
                defaults: new { controller = "PostBack", action = "Index", id = RouteParameter.Optional }
            );
        }
    }
}
