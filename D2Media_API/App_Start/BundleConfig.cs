﻿using System.Web;
using System.Web.Optimization;

namespace D2Media_API
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
