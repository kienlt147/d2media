﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using D2Media_API.Helpers;

namespace D2Media_API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private CultureInfo previousCultureInfo;
        protected void Application_Start()
        {
            AdoHelper.ConnectionString = ConfigurationManager.ConnectionStrings["MainConnectionString"].ConnectionString;
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

//        protected void Session_Start()
//        {
//            Session.Abandon();
//            Response.Redirect(Request.Url.AbsoluteUri);
//        }

//        protected void Application_BeginRequest()
//        {
//            string[] allowedOrigin = new string[]
//                {"http://103.252.252.234:8782", "http://localhost:53674", "http://localhost:51136"};
//            var origin = HttpContext.Current.Request.Headers["Origin"];
//            if (origin != null && allowedOrigin.Contains(origin))
//            {
//                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", origin);
//                HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "GET,POST");
//                //            previousCultureInfo = Thread.CurrentThread.CurrentUICulture; //Thread.CurrentThread.CurrentCulture;
//                //            if (Request.Headers.AllKeys.Contains("Origin") && Request.HttpMethod == "OPTIONS")
//                //            {
//                //                Response.Flush();
//                //            }
//            }
//        }

//        protected void Application_EndRequest()
//        {
//            //Restore the previous CultureInfo
//            Thread.CurrentThread.CurrentUICulture = previousCultureInfo;
//        }
    }
}
