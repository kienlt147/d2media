﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using D2Media_API.Entities;
using DeCaptcha;

namespace D2Media_API.Controllers
{
    public class UploadFilesController : Controller
    {
        int ret;

        uint p_pict_to;
        uint p_pict_type;
        uint major_id;
        uint minor_id;
        uint port;
        uint load;
        string host;
        string name;
        string passw;
        string answer_captcha;
        double balance;
        // GET: UploadImages
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(HttpPostedFileBase file)
        {
            try
            {
                if (file.ContentLength > 0)
                {

                    string fileName = Path.GetFileName(file.FileName);
                    if (fileName != null)
                    {
                        string path = Path.Combine(Server.MapPath(@"~/UploadedFiles"), fileName);
                        file.SaveAs(path);
                        // Decode Images
                        DeCaptcher dc = new DeCaptcher();
                        dc.username = "ngdat110490";
                        dc.password = "123qwerr";
                        string result = await dc.solveCaptcha(path);
                        string captcha = result.Split('|')[5];
                        //host = "api.decaptcher.com";
                        //name = "ngdat110490";
                        //passw = "123qwerr";
                        //port = 3500;
                        //p_pict_to = 0;
                        //p_pict_type = 0;
                        //major_id = 0;
                        //minor_id = 0;
                        //load = 0;
                        //FileStream fs = new FileStream(path, FileMode.Open);
                        //byte[] buffer = new byte[fs.Length];
                        //fs.Read(buffer, 0, buffer.Length);
                        //fs.Close();
                        //ret = DecaptcherLib.Decaptcher.RecognizePicture(host, port, name, passw, buffer, out p_pict_to,
                        //    out p_pict_type, out answer_captcha, out major_id, out minor_id);
                        if (captcha == "error")
                        {
                            ViewBag.Message = "File Decaptcher Error!!";
                            ViewBag.DecaptcherValue = "";
                        }
                        else
                        {
                            ViewBag.DecaptcherValue = captcha;
                        }
                    }
                    else
                    {
                        ViewBag.DecaptcherValue = "";
                    }
                }
                ViewBag.Message = "File Uploaded Successfully!!";
                return View();
            }
            catch(Exception e)
            {
                ViewBag.DecaptcherValue = "";
                ViewBag.Message = "File upload failed!!";
                return View();
            }
        }

        public async Task<JsonResult> ImageUpload(UploadImageEntities model)
        {
            var errorCode = 0;
            string captcha = null;
            try
            {
                if (model.ImageUrl != null)
                {
                    String path = System.Web.HttpContext.Current.Server.MapPath("~/UploadedFiles"); //Path

                    //Check if directory exist
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path); //Create directory if it doesn't exist
                    }

                    string imageName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".jpg";

                    //set the image path
                    string imgPath = Path.Combine(path, imageName);
                    byte[] imageBytes;
                    if (model.ImageUrl.Contains("data:image/"))
                    {
                        string convert = model.ImageUrl.Replace("data:image/jpeg;base64,%20", " ");
                        imageBytes = Convert.FromBase64String(convert);
                    }
                    else
                    {
                        imageBytes = new System.Net.WebClient().DownloadData(model.ImageUrl);
                    }
                    System.IO.File.WriteAllBytes(imgPath, imageBytes);
                    // Decode Images
                    DeCaptcher dc = new DeCaptcher();
                    dc.username = "ngdat110490";
                    dc.password = "123qwerr";
                    string result = await dc.solveCaptcha(imgPath);
                    captcha = result.Split('|')[5];
                }
            }
            catch (Exception e)
            {
                errorCode = -1;
                Console.WriteLine(e);
            }
            ApiResults apiResults = new ApiResults();
            apiResults.ErrCode = errorCode;
            apiResults.ErrMessage = captcha;
            return Json(apiResults, JsonRequestBehavior.AllowGet);
        }
    }
}