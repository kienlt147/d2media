﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using D2Media_API.Attribute;
using D2Media_API.Entities;
using D2Media_API.Helpers;
using D2Media_API.Models;

namespace D2Media_API.Controllers
{
    [RoutePrefix("api/offers")]
    public class OffersController : ApiController
    {
        /// <summary>
        /// Request Approval
        /// </summary>
        /// <remarks>
        /// Return result request approval
        /// </remarks>
        /// <response code="200">Successful Operation</response>
        /// <response code="401">Query database error</response>
        /// <response code="405">Validation Exception</response>
        [TokenAuthorize]
        [HttpGet]
        [Route("requestApproval")]
        public IHttpActionResult RequestApproval(int offerId)
        {
            OfferEntities offerEntities = new OfferEntities();
            var userId = UserHelper.SecurityTokenUser.Claims.First(x => x.Type == "UserId").Value;
            OfferUsers offerUsers = new OfferUsers();
            var offerUser = offerUsers.FindByUserIdOfferId(offerId, userId);
            if (offerUser.IsNull())
            {
                var link = ConfigurationManager.AppSettings["BaseURL"] 
                    + "aff_link?offer_id=" + offerId + "&aff_id=" + userId;
                var rs = offerUsers.RequestApproval(offerId, userId, link);
                switch (rs)
                {
                    case 0:
                        offerEntities.ErrCode = 200;
                        offerEntities.ErrMessage = "Send request approval success.";
                        break;
                    case 2:
                        offerEntities.ErrCode = 405;
                        offerEntities.ErrMessage = "Token or OfferId not valid.";
                        break;
                    default:
                        offerEntities.ErrCode = 401;
                        offerEntities.ErrMessage = "An unexpected error occurred. Please try again later.";
                        break;
                }
            }
            return Ok(offerEntities);
        }

        /// <summary>
        /// Request Approval
        /// </summary>
        /// <remarks>
        /// Return result request approval
        /// </remarks>
        /// <response code="200">Successful Operation</response>
        /// <response code="401">Query database error</response>
        /// <response code="405">Validation Exception</response>
        [TokenAuthorize]
        [HttpGet]
        [Route("acceptPartnerRequest")]
        public IHttpActionResult AcceptPartnerRequest(int id, int offerId, string userRequestId)
        {
            OfferEntities offerEntities = new OfferEntities();
            Users users = new Users();
            var userId = UserHelper.SecurityTokenUser.Claims.First(x => x.Type == "UserId").Value;
            var user = users.FilterById(userId);
            if (user.GroupId != 1)
            {
                offerEntities.ErrCode = 403;
                offerEntities.ErrMessage = "Account is not admin.";
                return Ok(offerEntities);
            }
            OfferUsers offerUsers = new OfferUsers();
            var rs = offerUsers.AcceptPartnerRequest(id, offerId, userRequestId);
            switch (rs)
            {
                case 0:
                    offerEntities.ErrCode = 200;
                    offerEntities.ErrMessage = "Accepted request.";
                    break;
                case 2:
                    offerEntities.ErrCode = 405;
                    offerEntities.ErrMessage = "Object not valid.";
                    break;
                default:
                    offerEntities.ErrCode = 401;
                    offerEntities.ErrMessage = "An unexpected error occurred. Please try again later.";
                    break;
            }
            return Ok(offerEntities);
        }
    }
}