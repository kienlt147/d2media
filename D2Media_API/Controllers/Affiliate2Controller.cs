﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web;
using D2Media_API.Attribute;
using D2Media_API.Entities;
using D2Media_API.Helpers;
using D2Media_API.Models;
using log4net;
using Newtonsoft.Json;

namespace D2Media_API.Controllers
{
    [RoutePrefix("aff_link2")]
    public class Affiliate2Controller : ApiController
    {
        protected ILog Log = LogManager.GetLogger(typeof(Affiliate2Controller));
        [HttpGet]
        public IHttpActionResult Index(int offer_id, string aff_id, string sub1 = "", string sub2 = "", string sub3 = "", string sub4 = "", string sub5 = "")
        {
            var xx = Request;
            OfferUsers offerUsers = new OfferUsers();
            Offers offers = new Offers();
            TransOfferLogs transOfferLogs = new TransOfferLogs();
            ReportConversions reportConversions = new ReportConversions();
            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            var userIp = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var transId = LibHelper.Md5(DateTime.Now.ToString("ddMMyyyyHHmmssffffff") + "d2media_sign");
            string versionOsMain = "";
            var offer = offers.FilterById(offer_id);
            try
            {
                var offerUser = offerUsers.FindByUserIdOfferId(offer_id, aff_id);
                if (offerUser.IsNull())
                {
                    Log.Info("Offer User is not null.");
                    return Ok("Offer User is not null.");
                }
                var reportConversion = reportConversions.FilterTotalByOfferId(offer_id);
                if (offer.IsNull())
                {
                    Log.Info("Offer is not null.");
                    return Ok("Offer is not null.");
                }
                if (reportConversion.IgnoreNulls().Any())
                {
                    if (reportConversion.Count > offer.ConversionLimit)
                    {
                        Log.Info("No more conversion.");
                        return Ok("No more conversion.");
                    }
                }
                string strUserAgent = Request.Headers.UserAgent.ToString().ToLower();
                var arrCountryCode = offer.CountryCode.Split(",");

                /*
                 * Kiểm tra Device Os
                 */
                DeviceOsMobile deviceOsMobile = new DeviceOsMobile();
                var deviceOsInfo = deviceOsMobile.Info(strUserAgent);
                var arrVersionOs = deviceOsInfo.OsVersion.Split("_");
                if (arrVersionOs.Length == 0)
                {
                    transOfferLogs.Add(offer_id, aff_id, currentUrl, "Check Os Error.", null, 
                        userIp, TransOfferLogs.TypeClick, transId, offer.CategoryId);
                    Log.Info("Check Os error.");
                    return Ok("Check Os error.");
                }
                versionOsMain = arrVersionOs[0];
                
                /*
                 * Kiểm tra IP và Country
                 */
                if (string.IsNullOrEmpty(userIp))
                {
                    userIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                //http://lumtest.com/myip.json
                string url = "http://freegeoip.net/json/" + userIp.ToString();
                WebClient client = new WebClient();
                string jsonstring = client.DownloadString(url);
                var dynObj = JsonConvert.DeserializeObject<ApiCountry>(jsonstring);

                Log.Info("====> Device Info = " + deviceOsInfo.OsVersion + " | Ip = " + userIp + " | Version Os Main = " + versionOsMain + " | Country Code = " + dynObj.country_code);
                int isCountryCode = arrCountryCode.Count(x => x == dynObj.country_code);
                if (int.Parse(versionOsMain) >= int.Parse(offer.DeviceOsVersion) && isCountryCode > 0)
                {
                    ReportClicks reportClicks = new ReportClicks();
                    reportClicks.ReportClickAddEdit(offer_id, aff_id, userIp, offer.DeviceOs, versionOsMain,
                        dynObj.country_code, dynObj.country_name, transId, offer.CategoryId, Request.RequestUri.Query, strUserAgent);
                    /*
                     * Tạo Link HasOffer từ Keyword Categories
                     */
                    Categories categories = new Categories();
                    var category = categories.FilterById(offer.CategoryId);
                    var hasOfferLink = offer.HasOfferLink + 
                        "&" + category.Sub1 + "=" + transId + 
                        "&" + category.Sub2 + "=" + aff_id;
                    Log.Info("Insert Report Successfuly. | ==> HasOfferLink : " + hasOfferLink);
                    return Redirect(hasOfferLink);
                }
                else
                {
                    Log.Info("Verion Os or Country Code not valid.");
                    transOfferLogs.Add(offer_id, aff_id, currentUrl, "Verion Os or Country Code not valid.", 
                        versionOsMain, userIp, TransOfferLogs.TypeClick, transId, offer.CategoryId);
                }
            }
            catch (Exception e)
            {
                Log.Info("Affiliate Exception : " + e.Message);
                transOfferLogs.Add(offer_id, aff_id, currentUrl, e.Message, versionOsMain, 
                    userIp, TransOfferLogs.TypeClick, transId, offer.CategoryId);
            }
            return Ok("An unexpected error occurred. Please try again later.");
        }
    }
}