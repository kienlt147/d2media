﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using D2Media_API.Entities;
using D2Media_API.Helpers;
using D2Media_API.Models;
using log4net;
using Newtonsoft.Json;

namespace D2Media_API.Controllers
{
    [RoutePrefix("postback")]
    public class PostBackController : ApiController
    {
        protected ILog Log = LogManager.GetLogger(typeof(PostBackController));
        // GET: PostBack
        [HttpGet]
        public IHttpActionResult Index(string orientaps_transid, string trans_id, 
            string country_code, string address_ip, string sign)
        {
            TransOfferLogs transOfferLogs = new TransOfferLogs();
            ReportConversions reportConversions = new ReportConversions();
            string currentUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            var userIp = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            var md5Sign = LibHelper.Md5("d2media_sign");
            int orientapsOfferId = 0;
            var orientapsUserId = "";
            int categoryId = 0;
            Log.Info("===> orientaps_transid : " + orientaps_transid + " | trans_id : " + trans_id + " | country_code : " + country_code + " | address_ip : " + address_ip);
            try
            {
                /*
                 * Check Sign
                 */
                if (sign != null && !sign.Equals(md5Sign))
                {
                    Log.Info("===> Sign not vaild | ==> TransId : " + orientaps_transid + " | ==> HasOfferTransId : " + trans_id);
                    transOfferLogs.Add(orientapsOfferId, orientapsUserId, currentUrl, "Sign not vaild.", null, 
                        userIp, TransOfferLogs.TypeConversion, trans_id, categoryId);
                    return Ok("Sign not vaild.");
                }
                else
                {
                    /*
                     * Lấy thông tin OfferId và UserId từ Orientaps TransId
                     */
                    if (orientaps_transid.IsNotEmptyOrWhiteSpace())
                    {
                        ReportClicks reportClicks = new ReportClicks();
                        var rp = reportClicks.FilterByTransId(orientaps_transid);
                        if (rp.IsNotNull())
                        {
                            orientapsOfferId = rp.OfferId;
                            orientapsUserId = rp.UserId.ToString();
                            categoryId = rp.CategoryId;
                        }
                        else
                        {
                            Log.Info("===> Get TransId in Report is null. | ==> TransId : " + orientaps_transid);
                        }
                    }
                    var rs = reportConversions.ReportConversionAddEdit(
                        orientapsOfferId, 
                        orientapsUserId, 
                        null, 
                        trans_id,
                        country_code,
                        address_ip,
                        categoryId
                    );
                    switch (rs)
                    {
                        case 0:
                            Log.Info("Insert Conversion Successfuly.");
                            break;
                        default:
                            Log.Info("Code : " + rs + " | Add Report Conversion error => " + trans_id);
                            transOfferLogs.Add(orientapsOfferId, orientapsUserId, currentUrl, 
                                "Code : " + rs + " | Add Report Conversion error.", null, 
                                userIp, TransOfferLogs.TypeConversion, trans_id, categoryId);
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Info("PostBack Exception : " + e.Message + " | TransId : " + trans_id);
                transOfferLogs.Add(
                    orientapsOfferId, 
                    orientapsUserId, 
                    currentUrl, 
                    e.Message, 
                    null, 
                    userIp, 
                    TransOfferLogs.TypeConversion, 
                    trans_id,
                    categoryId
                );
            }
            return Ok("Success");
        }
    }
}