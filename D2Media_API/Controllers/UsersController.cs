﻿using D2Media_API.Entities;
using D2Media_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace D2Media_API.Controllers
{
    [RoutePrefix("api/offers")]
    public class UsersController : ApiController
    {
        [HttpPost]
        [Route("Register")]
        public IHttpActionResult Register(UserEntities model)
        {
            ApiResults apiResults = new ApiResults();
            try
            {
                var msg = string.Empty;
                if (!ModelState.IsValid)
                {
                    msg = ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0)
                        .Aggregate(msg, (current, key) => (string.Format("{0}", ModelState[key].Errors[0].ErrorMessage)));
                    apiResults.ErrCode = 404;
                    apiResults.ErrMessage = msg;
                    return Json(apiResults);
                }
                var userName = model.Email;
                Users users = new Users();
                var user = users.Add(userName, model.FullName, model.Email, model.Description, model.Phone);
                switch (user)
                {
                    case 0:
                        apiResults.ErrCode = 200;
                        apiResults.ErrMessage = "Register success.";
                        return Json(apiResults);
                    default:
                        apiResults.ErrCode = 404;
                        apiResults.ErrMessage = "Email already exists.";
                        return Json(apiResults);
                }
            }
            catch (Exception e)
            {
                apiResults.ErrCode = 500;
                apiResults.ErrMessage = e.Message;
                return Json(apiResults);
            }
        }

        [HttpPost]
        [Route("RegisterCms")]
        public IHttpActionResult RegisterCms(UserCmsEntities model)
        {
            ApiResults apiResults = new ApiResults();
            try
            {
                var msg = string.Empty;
                if (!ModelState.IsValid)
                {
                    msg = ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0)
                        .Aggregate(msg, (current, key) => (string.Format("{0}", ModelState[key].Errors[0].ErrorMessage)));
                    apiResults.ErrCode = 404;
                    apiResults.ErrMessage = msg;
                    return Json(apiResults);
                }
                var userName = model.Email;
                Users users = new Users();
                var user = users.Add(userName, model.Password, model.FullName, model.Email, 
                    Users.UserGuest, null, model.NumberPhone, model.Company, model.Skype, null);
                switch (user)
                {
                    case 0:
                        apiResults.ErrCode = 200;
                        apiResults.ErrMessage = "Register success.";
                        return Json(apiResults);
                    default:
                        apiResults.ErrCode = 404;
                        apiResults.ErrMessage = "Email already exists.";
                        return Json(apiResults);
                }
            }
            catch (Exception e)
            {
                apiResults.ErrCode = 500;
                apiResults.ErrMessage = e.Message;
                return Json(apiResults);
            }
        }
    }
}