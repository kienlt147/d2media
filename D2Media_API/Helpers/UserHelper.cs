﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace D2Media_API.Helpers
{
    public class UserHelper
    {
        public static JwtSecurityToken SecurityTokenUser;
//        private HttpRequestMessage HttpRequestMessage => (HttpRequestMessage)HttpContext.Current.Items["MS_HttpRequestMessage"];
        private NameValueCollection HttpRequestMessage => HttpContext.Current.Request.QueryString;

        private string GetBearerToken()
        {
            string authToken = null;
//            if (HttpRequestMessage.Headers.Authorization == null) return null;
//            if (HttpRequestMessage.Headers.Authorization.Scheme == "Bearer")
//            {
//                authToken = HttpRequestMessage.Headers.Authorization.Parameter;
//            }
            if (HttpRequestMessage.Get("authToken") == null) return null;
            authToken = HttpRequestMessage.Get("authToken");
            return authToken;
        }

        public bool ValidateToken()
        {
            string authToken = GetBearerToken();
            var handler = new JwtSecurityTokenHandler();
            SecurityTokenUser = handler.ReadJwtToken(authToken);
            var userId = SecurityTokenUser.Claims.First(x => x.Type == "UserId").Value;
            var userNamae = SecurityTokenUser.Claims.First(x => x.Type == "UserName").Value;
            var expiration = SecurityTokenUser.Claims.First(x => x.Type == "Expiration").Value;
            var currentDate = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            if (Int64.Parse(currentDate) > Int64.Parse(expiration))
            {
                return false;
            }
            return true;
        }
    }
}