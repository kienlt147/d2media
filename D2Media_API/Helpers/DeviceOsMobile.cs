﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_API.Helpers
{
    public class DeviceOsInfo
    {
        public string Brand { get; set; }
        public string Os { get; set; }
        public string Model { get; set; }
        public string OsVersion { get; set; }
    }
    public class DeviceOsMobile
    {
        public DeviceOsInfo Info(string userAgent)
        {
            if (!userAgent.Contains("iphone"))
            {
                return new DeviceOsInfo
                {
                    Brand = null,
                    Os = null,
                    Model = null,
                    OsVersion = null
                };
            }
            var checkOs = userAgent.IndexOf("os", StringComparison.Ordinal);
            var strOs = userAgent.Substring(checkOs, 9);
            var arrOs = strOs.Split(" ");
            var osVersion = "";
            if (arrOs.Length > 1)
            {
                var _osVersion = arrOs[1];
                osVersion = _osVersion;
            }
            return new DeviceOsInfo
            {
                Brand = "apple",
                Os = "ios",
                Model = "iphone",
                OsVersion = osVersion
            };
        }
    }
}