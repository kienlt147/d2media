﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_API.Entities
{
    public class AffiliateEntities
    {
        public int offer_id { get; set; }
        public string aff_id { get; set; }
        public string sub1 { get; set; }
        public string sub2 { get; set; }
        public string sub3 { get; set; }
        public string sub4 { get; set; }
        public string sub5 { get; set; }
    }
}