﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace D2Media_API.Entities
{
    public class PostBackEntities
    {
        public int OfferId { get; set; }
        public string UserId { get; set; }
        public string CreatedDate { get; set; }
    }
}