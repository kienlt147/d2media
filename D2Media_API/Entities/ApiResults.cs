﻿namespace D2Media_API.Entities
{
    public class ApiResults
    {
        public string ErrMessage { get; set; }
        public int ErrCode { get; set; }
    }
}