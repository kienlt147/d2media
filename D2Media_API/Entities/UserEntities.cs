﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace D2Media_API.Entities
{
    public class UserEntities
    {
        [Required]
        [BindRequired]
        public string FullName { get; set; }

        [Required]
        [BindRequired]
        public string Phone { get; set; }

        [Required]
        [BindRequired]
        public string Email { get; set; }

        public string Description { get; set; }
    }

    public class UserCmsEntities
    {
        [Required]
        [BindRequired]
        public string FullName { get; set; }

        [Required]
        [BindRequired]
        public string Password { get; set; }

        [Required]
        [BindRequired]
        public string Email { get; set; }

        [Required]
        [BindRequired]
        public string Company { get; set; }

        [Required]
        [BindRequired]
        public string NumberPhone { get; set; }

        [Required]
        [BindRequired]
        public string Skype { get; set; }
    }
}