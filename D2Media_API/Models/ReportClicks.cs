﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_API.Helpers;

namespace D2Media_API.Models
{
    public class ReportClick
    {
        public int Id { get; set; }
        public int Click { get; set; }
        public int UniqueClick { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ReportClickDetail
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public string AddressIp { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceOsVersion { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string TransId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class ReportClicks
    {
        public ReportClickDetail FilterByTransId(string transId)
        {
            var rs = AdoHelper.ExecCachedListProc<ReportClickDetail>("sp_cms_report_click_detail_filter_by_trans_id_select", true, false, null, null,
                "@TransId", transId);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }
        public int? ReportClickAddEdit(int offerId, string userId, string addressIp, string deviceOs, 
            string deviceOsVersion, string countryCode, string countryName, string transId, int categoryId, string dynamicParam, string userAgent)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_report_click_insert_update",
                    "@OfferId", offerId,
                    "@UserId", userId,
                    "@AddressIp", addressIp,
                    "@DeviceOs", deviceOs,
                    "@DeviceOsVersion", deviceOsVersion,
                    "@CoutryCode", countryCode,
                    "@CountryName", countryName,
                    "@TransId", transId,
                    "@CategoryId", categoryId,
                    "@DynamicParam", dynamicParam,
                    "@UserAgent", userAgent,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}