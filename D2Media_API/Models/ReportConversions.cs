﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_API.Helpers;

namespace D2Media_API.Models
{
    public class ReportConversion
    {
        public int Id { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }

        public int HasOfferId { get; set; }
        public string HasOfferTransId { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class ReportConversions
    {
        public int? ReportConversionAddEdit(int offerId, string userId, string hasOfferId, 
            string hasOfferTransId, string countryCode, string addressIp, int categoryId)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_report_conversion_insert_update",
                    "@OfferId", offerId,
                    "@UserId", userId,
                    "@HasOfferId", hasOfferId,
                    "@HasOfferTransId", hasOfferTransId,
                    "@CountryCode", countryCode,
                    "@AddressIp", addressIp,
                    "@CategoryId", categoryId,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public List<ReportConversion> FilterTotalByOfferId(int offerId)
        {
            return AdoHelper.ExecCachedListProc<ReportConversion>("sp_cms_report_total_conversion_select", true, false, null, null,
                "@OfferId", offerId);
        }
    }
}