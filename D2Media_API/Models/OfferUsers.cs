﻿using D2Media_API.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_API.Models
{
    public class OfferUser
    {
        public int UserId { get; set; }
        public int OfferId { get; set; }
        public string Link { get; set; }
        public int UniqueClick { get; set; }
        public int Click { get; set; }
        public int Status { get; set; }
    }

    public class OfferUsers
    {
        public static int TotalCount { get; set; }

        public OfferUser FindByUserIdOfferId(int? offerId, string userId)
        {
            var rs = AdoHelper.ExecCachedListProc<OfferUser>("sp_cms_offer_users_select", true, false, null, null,
                "@OfferId", offerId, "@UserId", userId);
            return rs.IgnoreNulls().Any() ? rs.First() : null;
        }

        public int? RequestApproval(int offerId, string userId, string link)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_offer_user_request_approval",
                    "@UserId", userId,
                    "@OfferId", offerId,
                    "@Link", link,
                    "@Status", 1,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? AcceptPartnerRequest(int id, int offerId, string userId)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_offer_user_accept_partner_request",
                    "@Id", id,
                    "@OfferId", offerId,
                    "@UserId", userId,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}