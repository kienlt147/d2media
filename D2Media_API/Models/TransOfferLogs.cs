﻿using D2Media_API.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_API.Models
{
    public class TransOfferLog
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int OfferId { get; set; }
        public int UserId { get; set; }
        public string Message { get; set; }
        public string DeviceOsVersion { get; set; }
        public string AddressIp { get; set; }
        public string Type { get; set; }
    }

    public class TransOfferLogs
    {
        public static string TypeConversion = "conversion";
        public static string TypeClick = "click";
        public int? Add(int offerId, string userId, string url, 
            string message, string deviceVersion, string addressIp, 
            string type, string hasOfferTransId, int categoryId)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_trans_offer_log_insert",
                    "@UserId", userId,
                    "@OfferId", offerId,
                    "@Url", url,
                    "@Message", message,
                    "@DeviceOsVersion", deviceVersion,
                    "@AddressIp", addressIp,
                    "@Type", type,
                    "@HasOfferTransId", hasOfferTransId,
                    "@CategoryId", categoryId,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}