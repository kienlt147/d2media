﻿using D2Media_API.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace D2Media_API.Models
{
    public class Offer
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string HasOfferLink { get; set; }
        public double HasOfferRevenue { get; set; }
        public int ConversionLimit { get; set; }
        public string PreviewLink { get; set; }
        public string Description { get; set; }
        public string DeviceBrand { get; set; }
        public string DeviceModel { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceOsVersion { get; set; }
        public int Status { get; set; }
        public string Images { get; set; }
        public double Revenue { get; set; }
        public double Payout { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
    }

    public class Offers
    {
        public static int TotalCount { get; set; }
        
        public Offer FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Offer>("sp_cms_offer_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }
    }
}