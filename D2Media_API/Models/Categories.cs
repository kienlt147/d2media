﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using D2Media_API.Helpers;

namespace D2Media_API.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public string TransId { get; set; }
        public string Sub1 { get; set; }
        public string Sub2 { get; set; }
        public string Sub3 { get; set; }
        public string Sub4 { get; set; }
        public string Sub5 { get; set; }
        public string CountryCode { get; set; }
        public string AddressIp { get; set; }
        public string DeviceOs { get; set; }
        public string DeviceOsVersion { get; set; }
        public int Status { get; set; }
    }

    public class Categories
    {
        public List<Category> FilterAll(int currentPage, int pageSize, string searchText = "")
        {
            return AdoHelper.ExecCachedListProc<Category>("sp_cms_categories_select", true, false, null, null,
                "@PageNumber", currentPage, "@PageSize", pageSize, "@SearchText", searchText);
        }

        public Category FilterById(int? id)
        {
            var rs = AdoHelper.ExecCachedListProc<Category>("sp_cms_category_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }
    }
}