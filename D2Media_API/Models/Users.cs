﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using D2Media_API.Helpers;

namespace D2Media_API.Models
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int? GroupId { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
    }
    public class Users
    {
        public static int UserGuest = 5;
        public User FilterById(string id)
        {
            var rs = AdoHelper.ExecCachedListProc<User>("sp_api_user_filter_by_id_select", true, false, null, null,
                "@Id", id);
            return rs.IgnoreNulls().Any() ? rs.FirstOrDefault() : null;
        }

        public int? Add(string username, string fullName, string email,
            string description, string numberPhone)
        {
            int? result;

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_api_users_insert",
                    "@UserName", username,
                    "@FullName", fullName,
                    "@Email", email,
                    "@Description", description,
                    "@NumberPhone", numberPhone,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }

        public int? Add(string username, string password, string fullName, string email,
            int? groupId, string description, string numberPhone, string company, string skype, string postbackLink)
        {
            int? result;

            string salt;
            string encryptedPassword = SecurityHelper.EncryptPassword(password, out salt);

            using (AdoHelper db = new AdoHelper())
            {
                var returnValue = db.CreateParamReturnValue("returnValue");
                db.ExecNonQueryProc("sp_admin_users_insert",
                    "@UserName", username,
                    "@Password", encryptedPassword,
                    "@Salt", salt,
                    "@FullName", fullName,
                    "@Email", email,
                    "@GroupId", groupId,
                    "@Description", description,
                    "@NumberPhone", numberPhone,
                    "@Company", company,
                    "@Skype", skype,
                    "@PostBackLink", postbackLink,
                    returnValue);
                result = db.GetParamReturnValue(returnValue);
                db.Dispose();
            }

            return result;
        }
    }
}