﻿using D2Media_Frontend.Helpers;
using D2Media_Frontend.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace D2Media_Frontend.Controllers
{
    public class MailController : Controller
    {
        // GET: Mail
        public ActionResult Send(SendMail sendMail)
        {
            try
            {
                var msg = string.Empty;
                if (!ModelState.IsValid)
                {
                    msg = ModelState.Keys.Where(key => ModelState[key].Errors.Count > 0)
                        .Aggregate(msg, (current, key) => (string.Format("{0}", ModelState[key].Errors[0].ErrorMessage)));
                    return Json(new {
                        ErrorCode = 404,
                        ErrorMessage = msg
                    });
                }
                EmailHelper emailHelper = new EmailHelper();
                emailHelper.ToEmail = sendMail.Email;
                emailHelper.Subject = "Redictu.com";
                emailHelper.Message = $"Fullname: {sendMail.FullName} <br /> " +
                    $"NumberPhone: {sendMail.NumberPhone} <br /> " +
                    $"Message: {sendMail.Message}";
                emailHelper.SendEmail(emailHelper);
                return Json(new
                {
                    ErrorCode = 200,
                    ErrorMessage = "We sent the mail to your mailbox."
                });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    ErrorCode = 404,
                    ErrorMessage = e.Message
                });
            }
        }
    }
}