﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;

namespace D2Media_Frontend.ViewModel
{
    public class SendMail
    {
        [Required]
        [BindRequired]
        public string FullName { get; set; }

        [Required]
        [BindRequired]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        public string NumberPhone { get; set; }

        [Required]
        [BindRequired]
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string Email { get; set; }

        public string Message { get; set; }
    }
}