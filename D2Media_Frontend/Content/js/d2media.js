﻿(function ($) {
    $(".send_email").on("click", function () {
        var verificationToken = $('input[name="__RequestVerificationToken"]').val();
        var fullName = $("#FullName").val();
        var numberPhone = $("#NumberPhone").val();
        var email = $("#Email").val();
        var message = $("#Message").val();
        $.ajax({
            url: "/mail/send",
            type: "POST",
            data: {
                FullName: fullName,
                NumberPhone: numberPhone,
                Email: email,
                Message: message,
                __RequestVerificationToken: verificationToken
            },
            dataType: "json",
            success: function (data) {
                alert(data.ErrorMessage);
            }
        });
    });
})(jQuery);