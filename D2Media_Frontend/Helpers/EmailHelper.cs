﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace D2Media_Frontend.Helpers
{
    public class EmailHelper
    {
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }

        public void SendEmail(EmailHelper model)
        {
            var message = new MailMessage();
            message.To.Add(new MailAddress(model.ToEmail));
            message.From = new MailAddress(ConfigurationManager.AppSettings["Email_Support"]);
            message.Subject = model.Subject;
            message.Body = string.Format(model.Message);
            message.IsBodyHtml = true;

            using (var smtp = new SmtpClient())
            {
                var credential = new NetworkCredential
                {
                    UserName = ConfigurationManager.AppSettings["Email_Support"],  // replace with valid value
                    Password = ConfigurationManager.AppSettings["Email_Pass"]  // replace with valid value
                };
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = credential;
                smtp.Host = "mail.redictu.com";
                smtp.Port = 587;
                try
                {
                    smtp.Send(message);
                    //                    await smtp.SendMailAsync(message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
    }
}